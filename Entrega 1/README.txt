PROYECTO 'SYSTEMA DE FICHEROS'

ASIGNATURA: SISTEMAS OPERATIVOS II
AUTORES:    JUAN CARLOS BOO CRUGEIRAS
            HECTOR MARIO MEDINA CABANELAS
FECHA:      5 DE ABRIL DE 2019

1. REPOSITORIO GITLAB:

Para mayor agilidad a lahora de crear el proyecto se ha implementado el uso de un repositorio en 'GITLAB', una herramienta online que permite almacenamiento de código fuente con control de versiones.

El repositorio se encuentra en la siguiente URL: 

https://gitlab.com/grado_informatica_uib/sistemas-opertivos-ii/snapchat/entrega-1

2. SINTAXIS.

2.1. MAKEFILE

Se trata del fichero encargado de la compilación y generación de ficheros ejecutables.

Tiene dos comandos: 'make' y 'make clean'

- make : Tras ejecutar este comando se compilarán y se generarán los ficheros ejecutables.
- make clean : Tras ejecutar este comando se eliminarán los ficheros ejecutables generados previamente.

2.2. MI_MKFS.C

Este programa es el encargado de generar un disco con el número de bloques que se indiquen.

Sólo tiene un comando: 'mi_mkfs <Nombre del fichero a crear>'

2.3. ESCRIBIR.C

Escribirá en uno o varios inodos el contenido que le indiquemos.

escribir <Nombre de fichero-disco> <Contenido a escribir> <Uno [0] o Varios [1] Inodos>

2.4. LEER.C

Le pasamos por línea de comandos un número de inodo obtenido con el programa anterior (Además del nombre del dispositivo. Su funcionamiento tiene que ser similar a la funcion 'cat' de linux, explorando todo el fichero.

leer <nombre_dispositivo> <numero de inodo>

2.5. TRUNCAR.C

Libera un inodo a partir de un número de byte dado por parámetro.

truncar <nombre_dispositivo> <ninodo> <nbytes>

2.6. PERMITIR.C

Cambia los permisos de un inodo.

permitir <nombre_dispositivo> <ninodo> <permisos>

2.7. LEER_SF.C

Mostramos información genérica del sistema de ficheros.

leer_sf <Nombre del disco a leer>


