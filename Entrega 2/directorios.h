/******************************************************************************
*                                DIRECTORIOS.H 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 14 DE MAYO DE 2019.
******************************************************************************/
#include "ficheros.h"

#define     MAX_TAMANO  60
struct UltimaEntrada{
char camino [512];
int p_inodo;
};


// ESTRUCTURA DIRECTORIO
struct entrada{
    char nombre[60];            // En el SF ext32la longitud del nombre es 255
    unsigned int ninodo;
};


/******************************************************************************
* Método: extraer_camino()
* Descripción:  Disecciona un camino para saber si una ruta es un directorio 
*               o un fichero.
* Parámetros:   camino  :   Camino completo recibido.
*               inicial :   Puntero donde se guardará el inicial.
*               final   :   Puntero donde se guardará el final.
*               tipo    :   Puntero donde se guardará el tipo.
* Devuelve:    -1: Si ha habido algún problema.
*               1: Si se ha ejecutado correctamente.
******************************************************************************/
int extraer_camino(const char *camino, char *inicial, char *final, char *tipo);


/******************************************************************************
* Método: buscar_entrada()
* Descripción: Devuelve el número de inodo del camino parcial. 
* Parámetros:  reservas : (1:crea) | (0:no crea)
* Devuelve:   -1 : Si ha habido algún error.
*              0 : Si todo ha ido bien.
******************************************************************************/
int buscar_entrada(const char *camino_parcial, unsigned int *p_inodo_dir, 
unsigned int *p_inodo, unsigned int *p_entrada, char reservar, unsigned char permisos);

/******************************************************************************
* Método: 
* Descripción: 
* Parámetros: 
* Devuelve:   
******************************************************************************/
void errorControl(int errorCode, char *message);

/******************************************************************************
* Método: 
* Descripción: 
* Parámetros: 
* Devuelve:   
******************************************************************************/
int mi_creat(const char *camino, unsigned char permisos);

/******************************************************************************
* Método: 
* Descripción: 
* Parámetros: 
* Devuelve:   
******************************************************************************/
int mi_touch(const char *camino, unsigned char permisos);

/******************************************************************************
* Método: 
* Descripción: 
* Parámetros: 
* Devuelve:   
******************************************************************************/
 int mi_dir(const char *camino, char *buffer);

/******************************************************************************
* Método: 
* Descripción: 
* Parámetros: 
* Devuelve:   
******************************************************************************/
int mi_chmod(const char *camino, unsigned char permisos);

/******************************************************************************
* Método: 
* Descripción: 
* Parámetros: 
* Devuelve:   
******************************************************************************/
int mi_stat(const char *camino, struct STAT *p_stat);

/******************************************************************************
* Método: 
* Descripción: 
* Parámetros: 
* Devuelve:   
******************************************************************************/
int mi_read(const char *camino, void *buf, unsigned int offset, unsigned int nbytes);

/******************************************************************************
* Método: 
* Descripción: 
* Parámetros: 
* Devuelve:   
******************************************************************************/
int mi_write(const char *camino, const void *buf, unsigned int offset, unsigned int nbytes);

/******************************************************************************
* Método: 
* Descripción: 
* Parámetros: 
* Devuelve:   
******************************************************************************/
int mi_link(const char *camino1, const char *camino2);

/******************************************************************************
* Método: 
* Descripción: 
* Parámetros: 
* Devuelve:   
******************************************************************************/
 int mi_unlink(const char *camino);