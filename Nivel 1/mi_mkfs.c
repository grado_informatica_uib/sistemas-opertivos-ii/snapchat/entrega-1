#include "bloques.h"

int main (int argc, char *argv[]){
    /**************************************************************************
     *                          MONTAMOS EL DISCO
     *************************************************************************/
    bmount(argv[1]);

    /**************************************************************************
     *                      ASIGNAMOS TAMAÑO AL DISCO
     *************************************************************************/    
    int nbloques = atoi(argv[2]);
    unsigned char init_bloque [BLOCKSIZE];
    memset(init_bloque,0,BLOCKSIZE);
    for(int i = 0; i < nbloques; i++)
    {
        bwrite(i,init_bloque);
    }

    return 0;
}