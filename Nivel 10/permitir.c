/******************************************************************************
*                                PERMITIR.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERACTIVOS II - GRADO ING. INFORMATICA - UIB
*
* DESCRIPCIÓN: CAMBIA LOS PERMISOS DE UN INODO.
*
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
*
* FECHA: 04 DE ABRIL DE 2019.
******************************************************************************/

#include<stdio.h>
#include "ficheros.h"

int main(int argc, char **argv){
    /**************************************************************************
     *                 COMPROBAMOS SI LA SINTAXIS ES CORRECTA
     *************************************************************************/
    if(argc != 4){
        printf("Sintaxis: permitir <nombre_dispositivo> <ninodo> <permisos>\n");
        exit(EXIT_FAILURE);
    }
    /**************************************************************************
     *                    MONTAMOS EL DISPOSITIVO VIRTUAL
     *************************************************************************/
    int r = bmount(argv[1]);
    if(r == -1){
        fprintf(stderr,"Error al montar el disco.\n");
        exit(EXIT_FAILURE);
    }
    /**************************************************************************
     *                    PERMITIR
     *************************************************************************/
    int permisos = atoi(argv[3]);
    int ninodo = atoi(argv[2]);
    mi_chmod_f(ninodo, permisos);
    /**************************************************************************
     *                   DESMONTAMOS EL DISPOSITIVO VIRTUAL
     *************************************************************************/
    if(bumount() == -1){
        fprintf(stderr,"Error al montar el disco.\n");
        exit(EXIT_FAILURE);
    }
}