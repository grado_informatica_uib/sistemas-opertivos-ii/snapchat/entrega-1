/******************************************************************************
*                               MI_LS.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÓN: FUNCIONES BÁSICA DE ENTRADA Y SALIDA DE BLOQUES.
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 14 DE MAYO DE 2019.
******************************************************************************/
#include "directorios.h"

int main(int argc, char *argv[]){
    // Comprobamos que el numero de argumentos es el correcto
    if (argc != 4) {
      fprintf(stderr, "Sintaxis: ./mi_chmod <disco> <permisos> </ruta>.\n");
      exit(EXIT_FAILURE);
    } 
    // Comprobamos que el rango de permisos es el adecuado
    //if(atoi(argv[2]) < 0 || atoi(argv[2]) > 7){
    //    fprintf(stderr, "Error: permisos debe estar en el rango 0-7");
    //    exit(EXIT_FAILURE);
    //}
    // Montamos eldisco
    if (bmount(argv[1]) == -1) {
        fprintf(stderr, "Error: error de apertura de fichero.\n");
        exit(-1);
    }
    mi_chmod(argv[3],atoi(argv[2]));
    if (bumount() == -1) {
        fprintf(stderr, "Error: error al cerrar el fichero.\n");
        exit(EXIT_FAILURE);
    }
    return 0;
}