/******************************************************************************
*                               DIRECTORIOS.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÃN: FUNCIONES BÃSICA DE ENTRADA Y SALIDA DE BLOQUES.
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÃCTOR MARIO MEDINA CABANELAS
* FECHA: 14 DE MAYO DE 2019.
******************************************************************************/


/******************************************************************************
*                           ARCHIVOS AUXILIARES
******************************************************************************/
#include "directorios.h"
#include <string.h>
struct UltimaEntrada UltimaEntradaLectura; 
/******************************************************************************
* MÃ©todo: extraer_camino()
* DescripciÃ³n:  Disecciona un camino para saber si una ruta es un directorio 
*               o un fichero.
* ParÃ¡metros:   camino  :   Camino completo recibido.
*               inicial :   Puntero donde se guardarÃ¡ el inicial.
*               final   :   Puntero donde se guardarÃ¡ el final.
*               tipo    :   Puntero donde se guardarÃ¡ el tipo.
* Devuelve:    -1: Si ha habido algÃºn problema.
*               1: Si se ha ejecutado correctamente.
******************************************************************************/
int extraer_camino(const char *camino, char *inicial, char *final, char *tipo){
    const char ch = '/';
    int estado = 1;
    // Tratamos el inicial.
    int j = 0;                  // Indice de inicial.
    int i=1;                    // Indice de camino.

    // Si el camino comianza por '/'
    if(camino[0] == ch){
        for(i=1;camino[i] != ch && camino[i] != '\0' ;i++){
            inicial[j] = camino[i]; // Asignamos camino a inicial por caracteres
            j++;                    // Aumentamos el indice de inicial.
        }
        inicial[j] = 0;             // Final de String en inicial.
        // Averiguamos si es directorio o fichero.
        if(camino[i] == ch){
            camino++;
            camino = strchr(camino, ch);  // Avanzamos al segundo '/' y actualizamos final
            strcpy(final, camino);
            strcpy(tipo, "d");          // Informamos que se trata de un directorio
        }
        else{
            strcpy(tipo, "f");          // Informamos de que se trata de un directorio.
        }
    }
    // Si el camino no comienza por '/'
    else{
        // Estado error
        estado = -1;
        // Imprimimos por la salida estandar de errores, el error.
        //fprintf(stderr, " Error: [extraer_camino()] --> camino invÃ¡lido.\n");
    }
    // Devolvemos el estado de la ejecuciÃ³n de la funciÃ³n.
    return estado;
}


/******************************************************************************
* MÃ©todo: buscar_entrada()
* DescripciÃ³n: Devuelve el nÃºmero de inodo del camino parcial. 
* ParÃ¡metros:  reservas : (1:crea) | (0:no crea)
* Devuelve:   -1 : Si ha habido algÃºn error.
*              0 : Si todo ha ido bien.
******************************************************************************/
int buscar_entrada(const char *camino_parcial, unsigned int *p_inodo_dir, unsigned int *p_inodo, unsigned int *p_entrada, char reservar, unsigned char permisos){
    /*************************************************************************
     *                  ATRIBUTOS Y DECLARACIONES AUXILIARES.
     ************************************************************************/
    char inicial[MAX_TAMANO];           // Buffer inicial
    char final[MAX_TAMANO];             // Buffer final
    memset(inicial, 0, MAX_TAMANO);     // InicializaiÃ³n buffer inicial
    memset(final, 0, MAX_TAMANO);       // InicializaciÃ³n buffer final
    char tipo;                          // Tipo fichero/directorio
    // Array de entradas.
    struct entrada entradas[BLOCKSIZE / sizeof(struct entrada)];    
    int numentradas = 0;                // NÃºmero de entradas.
    memset(entradas, 0, sizeof(entradas)); // Inicializamos entradas a 0.


    /*                  Si se trata del directorio raÃ­z.                   */
    if(strcmp(camino_parcial, "/") == 0){
        p_inodo = 0;        // Inicializamos p_inodo a 0
        p_entrada = 0;      // Inicializamos p_entrada a 0
        return 0;           // Devolvemos 0.
    }
    /*             Si extraer_camino() se ha ejecuta con errores           */
    if(extraer_camino(camino_parcial, inicial, final, &tipo) == -1){
        //fprintf(stderr, "[buscar_entrada]--> Error al extraer camino.\n");
        return -1;
    }
    //printf("[buscar_entrada()â inicial: %s, final: %s, reservar: %d]\n", inicial, final, reservar);

    struct inodo inodo_dir; // Inodo para guardar datos del dir.
    /*              Si leer_inodo() se ha ejecutado con errores            */
    if(leer_inodo(*p_inodo_dir, &inodo_dir) == -1){
       // fprintf(stderr, "[buscar_entrada]--> Error de lectura del inodo");
        return -2;
    }

    /*                    Si no tiene permisos de lectura                  */
    if((inodo_dir.permisos & 4) != 4){
        fprintf(stderr, "[buscar_entrada()â El inodo %d no tiene permisos de lectura]\n", *p_inodo_dir);
        return -3;
    }
    // Calculamos el nÃºmero de entradas que tenemos.
    numentradas = inodo_dir.tamEnBytesLog/sizeof(struct entrada);
    int nentrada = 0;           // NÃºmero de entrada actual 0
    int offset = 0;             // Offset actual 0
    int index = 0;              // index actual 0
    /*                    Si nÃºmero de entradas es mayor a 0               */
    if(numentradas > 0){
        mi_read_f(*p_inodo_dir, entradas, offset, BLOCKSIZE);  // Leemos el inodo.
        /*                  Mientras el nentrada < numentradas 
                            y no lleguemos al final de inicial              */
        while((nentrada < numentradas) && ((strcmp(inicial, entradas[index].nombre) != 0))){
            nentrada++;                                         // Avanzamos en nentrada
            index++;                                            // Avanzamos en index
            offset += sizeof(struct entrada);                   // Actualizamos offset
            /*  Si hemos llegado al nÃºmero mÃ¡ximo de entradas por blocksize  */
            if(index == BLOCKSIZE / sizeof(struct entrada)){
                index = 0;              // Inicializamos el Ã­ndice a 0.
                // Leemos el siguiente blocksize del inoco.
                mi_read_f(*p_inodo_dir, entradas, offset, BLOCKSIZE);
            }
        }
    }
    /*                       Si nentrada == numentradas                    */
    if(nentrada == numentradas){
        // Switch en reservar.
        switch(reservar){
            case 0:    // Si no ha de reservar, no se puede consultar algo que no existe.
                //fprintf(stderr, "[buscar_entrada]--> Error: No existe entrada consulta.\n");
                return -4;
            case 1:    // Si se ha de reservar, hay que mirar si es d o f
                /*                  Si se trata de un fichero             */
                if(inodo_dir.tipo == 'f'){
                    // No se puede crear una entrada dentro de un fichero
                    //fprintf(stderr, "[buscar_entrada]--> Error: No se puede crear una entrada en un fichero.\n");
                    return -5;
                }
                /*                  Si no tenemos permiso de escritura             */
                if((inodo_dir.permisos & 2) != 2){
                    fprintf(stderr, "[buscar_entrada]--> Error: Fallo en permiso escritura.\n");
                    return -6;
                } else {
                    // AquÃ­ tenemos permiso de escritura.
                    strcpy(entradas[index].nombre, inicial);
                    if(tipo == 'd'){
                        if(strcmp(final, "/") == 0){
                            int ninodo = reservar_inodo(tipo, permisos);
                            entradas[index].ninodo = ninodo;
                        } else {
                            fprintf(stderr,"[buscar_entrada()â entrada.nombre: %s,]\n", entradas[index].nombre);    
                            return -7;
                        }
                    } else {
                        int ninodo = reservar_inodo('f', permisos);
                        entradas[index].ninodo = ninodo;
                    }
                    if(mi_write_f(*p_inodo_dir, &entradas[index], offset, sizeof(struct entrada)) == -1){
                        if(entradas[index].ninodo != -1){
                            liberar_inodo(entradas[index].ninodo);
                        }
                        //fprintf(stderr, "[buscar_entrada]--> Error: Exit failure.\n");
                        return -8;
                    }
                }
            }
    } 
    if(strcmp(final, "/") == 0 || strcmp(final, "")==0){
        if((nentrada < numentradas) && (reservar == 1)){
            //fprintf(stderr, "[buscar_entrada]--> Error: Entrada ya existe.\n");
            return -9;
        }
        *p_inodo = entradas[index].ninodo;
        *p_entrada = nentrada;
    } else {
        *p_inodo_dir = entradas[index].ninodo;
        return buscar_entrada(final, p_inodo_dir, p_inodo, p_entrada, reservar, permisos);
    }
    if(reservar == 1){
        struct inodo inodo;
        leer_inodo(entradas[index].ninodo, &inodo);
        //printf("[buscar_entrada()â entrada.nombre: %s, entrada.ninodo: %d]\n",entradas[index].nombre, entradas[index].ninodo);
       // printf("[buscar_entrada()â reservado inodo %d tipo %c con permisos %d]\n", entradas[index].ninodo, inodo.tipo,permisos);
    }
    return 0;
}


/******************************************************************************
* MÃ©todo: 
* DescripciÃ³n: 
* ParÃ¡metros: 
* Devuelve:   
******************************************************************************/
void errorControl(int errorCode, char *message){
    switch(errorCode){
        case -1: strcpy(message, "Error: Camino incorrecto.");break;
        case -2: strcpy(message, "Error: Error de lectura del inodo.");break;
        case -3: strcpy(message, "Error: Permiso denegado de lectura.");break;
        case -4: strcpy(message, "Error: No existe el archivo o el directorio.");break;
        case -5: strcpy(message, "Error: No es un directorio.");break;
        case -6: strcpy(message, "Error  Permiso denegado de escritura.");break;
        case -7: strcpy(message, "Error: No existe algÃºn directorio intermedio.");break;
        case -8: strcpy(message, "Error: Fallo en el Exit.");break;
        case -9: strcpy(message, "Error: El archivo ya existe.");break;
    }
}

/******************************************************************************
* MÃ©todo: mi_creat()
* DescripciÃ³n: FunciÃ³n de la capa de directorios que crea un fichero/directorio
*              y su entrada de directorio.
* ParÃ¡metros:  const char *camino :  
*              unsigne char permisos : 
* Devuelve:  -1 : Si ha habido algÃºn error.
*             0 : Si todo hacido bien. 
******************************************************************************/
int mi_creat(const char *camino, unsigned char permisos){
    unsigned int p_inodo_dir;
    unsigned int p_inodo;
    unsigned int entrada;
    // Inicializamos reservar a 1 y el inodo padre a 0 (inodo raiz)
    unsigned int reservar = 1;
    char message[1000];
    memset(message, 0, 1000);
    p_inodo_dir = 0;
    p_inodo = 0;
    entrada=0;
    int error;
    mi_waitSem();           // Semaforo wait.
    // Si la direccion pertenece a un fichero debe usarse la funciÃ³n mi_touch
    if((camino[strlen(camino) - 1] != '/')&& strlen(camino) > 1){
        fprintf(stderr, "Error: No es un directorio.\n");
        mi_signalSem();     // Semaforo sigue.
        exit(EXIT_FAILURE);
    }
    // Buscar entrada creara un nuevo directorio si en parÃ¡metro 'reservar' recibe un 1
    error = buscar_entrada(camino, &p_inodo_dir, &p_inodo, &entrada, reservar, permisos);
    if( error < 0){
        errorControl(error, message);
        fprintf(stderr, "%s\n", message);
        mi_signalSem();     // Semaforo sigue.
        exit(EXIT_FAILURE);
    }
    mi_signalSem();         // Semaforo sigue.
    return 0;
}

/******************************************************************************
* MÃ©todo: mi_touch()
* DescripciÃ³n: FunciÃ³n de la capa de directorios que crea un fichero
*              y su entrada de directorio.
* ParÃ¡metros:  const char *camino :  
*              unsigne char permisos : 
* Devuelve:  -1 : Si ha habido algÃºn error.
*             0 : Si todo hacido bien. 
******************************************************************************/
int mi_touch(const char *camino, unsigned char permisos){
    unsigned int p_inodo_dir;
    unsigned int p_inodo;
    unsigned int entrada;
    // Inicializamos reservar a 1 y el inodo padre a 0 (inodo raiz)
    unsigned int reservar = 1;

    p_inodo_dir = 0;
    p_inodo = 0;
    entrada=0;
        char message[1000];
    memset(message, 0, 1000);
    int error;
    // Si la direccion pertenece a un fichero debe usarse la funciÃ³n mi_touch
    if(camino[strlen(camino) - 1] == '/'){
        fprintf(stderr, "Error: para crear un directorio se debe usar la funciÃ³n mi_creat\n");
        exit(EXIT_FAILURE);
    }
    error = buscar_entrada(camino, &p_inodo_dir, &p_inodo, &entrada, reservar, permisos);
    // Buscar entrada creara un nuevo directorio si en parÃ¡metro 'reservar' recibe un 1
    if(error < 0 ){
        errorControl(error, message);
        fprintf(stderr, "%s\n", message);
        exit(EXIT_FAILURE);
    }
    return 0;
}


/******************************************************************************
* MÃ©todo: 
* DescripciÃ³n: 
* ParÃ¡metros: 
* Devuelve:   
******************************************************************************/
 int mi_dir(const char *camino, char *buffer){
    int entr, nEntradas;
    struct inodo inodoAux;
    struct entrada entrada;
	unsigned int p_inodo_dir = 0, p_inodo = 0, p_entrada = 0;
    struct tm *tm; //ver info: struct tm
    char tmp[100];
    memset(tmp, 0, 100*sizeof(char));
    char message[1000];
    memset(message, 0, 1000*sizeof(char));

    entr = buscar_entrada(camino, &p_inodo_dir, &p_inodo, &p_entrada, 0, 0);
    if(entr < 0){
        errorControl(entr, message);
        fprintf(stderr, "%s\n", message);
        return -1;
    }
    leer_inodo(p_inodo, &inodoAux);
    nEntradas = inodoAux.tamEnBytesLog/sizeof(struct entrada); // calculamos el nÃºmero de entradas
    memset(buffer, 0, strlen(buffer)*sizeof(char));

    char str[100];
    memset(str, 0, 100*sizeof(char));
    if (leer_inodo(p_inodo, &inodoAux) == -1) return -1;
if(inodoAux.tipo == 'd'){
    strcpy(buffer, "Total: ");
sprintf(str, "%d",nEntradas);
    strcat(buffer, str);
    strcat(buffer, "\n");
}
    
    strcat(buffer,"Tipo\t Permisos\t mTime\t\t\t\t TamaÃ±o\t\t Nombre");
    strcat(buffer, "\n---------------------------------------------------------------------------------");
if(inodoAux.tipo == 'f'){
        strcat(buffer, "\n");
        str[0] = inodoAux.tipo;
        str[1] = '\0';
        strcat(buffer, str);
        strcat(buffer, "\t");

        if(inodoAux.permisos & 4){
            strcat(buffer, "r");
        }
        else{
            strcat(buffer, "-");
        }
        if(inodoAux.permisos & 2){
            strcat(buffer, "w");
        }
        else{
            strcat(buffer, "-");
        }
        if(inodoAux.permisos & 1){
            strcat(buffer, "x");
        }
        else{
            strcat(buffer, "-");
        }
        strcat(buffer, "\t\t");
        tm = localtime(&inodoAux.mtime);
        sprintf(tmp,"%d-%02d-%02d %02d:%02d:%02d\t",tm->tm_year+1900,
        tm->tm_mon+1,tm->tm_mday,tm->tm_hour,tm->tm_min,tm->tm_sec);
        strcat(buffer,tmp);
        strcat(buffer, "\t");
        sprintf(str, "%d", inodoAux.tamEnBytesLog);
        strcat(buffer, str);
        strcat(buffer,"\t\t");
    	strcat(buffer,camino);
        strcat(buffer, "\n");
        return 0;
    }
    
    
    
    for (int i = 0; i < nEntradas; i++) {
    if (mi_read_f(p_inodo, &entrada, (i * sizeof(struct entrada)), sizeof(struct entrada)) == -1) return -1;
    if (leer_inodo(entrada.ninodo, &inodoAux) == -1) return -1;
        strcat(buffer, "\n");
        str[0] = inodoAux.tipo;
        str[1] = '\0';
        strcat(buffer, str);
        strcat(buffer, "\t");

        if(inodoAux.permisos & 4){
            strcat(buffer, "r");
        }
        else{
            strcat(buffer, "-");
        }
        if(inodoAux.permisos & 2){
            strcat(buffer, "w");
        }
        else{
            strcat(buffer, "-");
        }
        if(inodoAux.permisos & 1){
            strcat(buffer, "x");
        }
        else{
            strcat(buffer, "-");
        }
        strcat(buffer, "\t\t");
        tm = localtime(&inodoAux.mtime);
        sprintf(tmp,"%d-%02d-%02d %02d:%02d:%02d\t",tm->tm_year+1900,
        tm->tm_mon+1,tm->tm_mday,tm->tm_hour,tm->tm_min,tm->tm_sec);
        strcat(buffer,tmp);
        strcat(buffer, "\t");
        sprintf(str, "%d", inodoAux.tamEnBytesLog);
        strcat(buffer, str);
        strcat(buffer,"\t\t");
    	strcat(buffer,entrada.nombre);
    }
    strcat(buffer, "\n");
    return entr;
 }


 /******************************************************************************
* MÃ©todo: 
* DescripciÃ³n: 
* ParÃ¡metros: 
* Devuelve:   
******************************************************************************/
int mi_chmod(const char *camino, unsigned char permisos){
    unsigned int p_inodo_dir, p_inodo, p_entrada;
    buscar_entrada(camino,&p_inodo_dir,&p_inodo, &p_entrada,0,0);
    mi_chmod_f(p_inodo, permisos);
    return 0;
 }

 /******************************************************************************
* MÃ©todo: mi_stat()
* DescripciÃ³n: Muestra la informaciÃ³n de estado de un camino.
* ParÃ¡metros: char * camino       : puntero a la ruta.
*             struct STAT *p_stat : puntero al objeto STAT.
* Devuelve:   0 : Si todo ha ido bien.
*            -1 : Si ha habido algÃºn error.
******************************************************************************/
int mi_stat(const char *camino, struct STAT *p_stat){
    /*************************************************************************
     *                        ATRIBUTOS Y DECLARACIONES
    *************************************************************************/
    unsigned int p_inodo_dir = 0;           // inodo raÃ­z.
    unsigned int p_inodo, p_entrada;        // inodo objetivo y p_entrada.
    // Almacenamos el resultado de la ejecuciÃ³n en error.
    int error = buscar_entrada(camino,&p_inodo_dir,&p_inodo, &p_entrada,0,7);
    // Si error es menor que 0, ha habido algÃºn error.
    char message[1000];
    memset(message, 0, 1000*sizeof(char));
    if(error < 0 ){
        // Imprimimos error.
        errorControl(error, message);
        fprintf(stderr, "%s\n", message);
        return -1;
    }
    // Si no hay errores, procedemos a hacer la llamada a mi_stat_f()
    mi_stat_f(p_inodo, p_stat);
    printf("NÂº de inodo: %d\n", p_inodo);
    return 0;
}


/******************************************************************************
* MÃ©todo: 
* DescripciÃ³n: 
* ParÃ¡metros: 
* Devuelve:   
******************************************************************************/
int mi_read(const char *camino, void *buf, unsigned int offset, unsigned int nbytes){
    unsigned int p_inodo_dir= 0, p_inodo = 0, p_entrada = 0;
    int bytesLeidos;
    if(strcmp (camino, UltimaEntradaLectura.camino) == 0){
        p_inodo = UltimaEntradaLectura.p_inodo;
        printf("\n\x1b[31m[mi_read() â Utilizamos la cachÃ© de lectura en vez de llamar a buscar_entrada()]\x1b[0m\n");

    }
    else{
        if(buscar_entrada(camino, &p_inodo_dir, &p_inodo, &p_entrada, 0, 0) == -1){
            return -1;
        }
        UltimaEntradaLectura.p_inodo = p_inodo;
        strcpy(UltimaEntradaLectura.camino, camino);
        printf("\n\x1b[31m[mi_read() â Actualizamos la cachÃ© de lectura]\x1b[0m\n");
    }

    if ((bytesLeidos = mi_read_f(p_inodo, buf, offset, nbytes)) == -1) return -1;
    return bytesLeidos;
}

/******************************************************************************
* MÃ©todo: 
* DescripciÃ³n: 
* ParÃ¡metros: 
* Devuelve:   
******************************************************************************/
int mi_write(const char *camino, const void *buf, unsigned int offset, unsigned int nbytes){
    unsigned int p_inodo_dir= 0, p_inodo = 0, p_entrada = 0;
    int bytesEscritos;
    if(strcmp (camino, UltimaEntradaLectura.camino) == 0){
        p_inodo = UltimaEntradaLectura.p_inodo;
        printf("\n\x1b[31m[mi_write() â Utilizamos la cachÃ© de escritura en vez de llamar a buscar_entrada()]\x1b[0m\n");

    }
    else{
        if (buscar_entrada(camino, &p_inodo_dir, &p_inodo, &p_entrada, 0, 6) == -1) {
            return -1;
        }
        UltimaEntradaLectura.p_inodo = p_inodo;
        strcpy(UltimaEntradaLectura.camino, camino);
        //printf("\n\x1b[31m[mi_write() â Actualizamos la cachÃ© de escritura]\x1b[0m\n");
    }


    if ((bytesEscritos = mi_write_f(p_inodo, buf, offset, nbytes)) == -1) return -1;

    return bytesEscritos;
  }


/******************************************************************************
* MÃ©todo: mi_link()
* DescripciÃ³n: Crea el enlace de una entrada de directorio camino2 al inodo 
*              especificado por otra entrada de directorio camino1 .
* ParÃ¡metros: 
* Devuelve:   
******************************************************************************/
int mi_link(const char *camino1, const char *camino2){
    unsigned int p_inodo_dir1=0, p_inodo1=0, p_entrada1=0;
    unsigned int p_inodo_dir2=0, p_inodo2=0, p_entrada2= 0;
    struct inodo inodo1;
    struct entrada entrada1;
    int r;
    mi_waitSem();     // Semaforo wait.
    if(camino1[strlen(camino1)-1] == '/'){
        fprintf(stderr,"Error: Camino1 es un directorio\n.");
                mi_signalSem();     // Semaforo sigue.
        return -1;
    }
    r = buscar_entrada(camino1, &p_inodo_dir1, &p_inodo1, &p_entrada1, 0, 0);
    if(r < 0){
        printf("Error: No existe el archivo o el directorio.\n");
                mi_signalSem();     // Semaforo sigue.
        return -1;
    }
    if (leer_inodo(p_inodo1, &inodo1) == -1)      {  
        mi_signalSem();     // Semaforo sigue.
        return -1;}
      if (inodo1.tipo != 'f') {        
        mi_signalSem();     // Semaforo sigue.
        return -1;
        } //camino1 ha de referirse a un fichero
    r = buscar_entrada(camino2, &p_inodo_dir2, &p_inodo2, &p_entrada2, 1, 6);
    if(r < 0){
        printf("Error: El archivo ya existe.\n");
                mi_signalSem();     // Semaforo sigue.
        return -1;
    }
      if (mi_read_f(p_inodo_dir2, &entrada1, p_entrada2*sizeof(struct entrada), sizeof(struct entrada)) == -1) {
    perror("Error: Entrada camino destino no leida correctamente.\n");
        mi_signalSem();     // Semaforo sigue.
    return -1;
  }
      entrada1.ninodo = p_inodo1; //Creamos enlace (modificamos entrada)

  //Escribimos entrada modificada
  if (mi_write_f(p_inodo_dir2, &entrada1, p_entrada2*sizeof(struct entrada), sizeof(struct entrada)) == -1) {
    perror("Error: Entrada camino no escrita correctamente.\n");
            mi_signalSem();     // Semaforo sigue.
    return -1;
  }


  if (liberar_inodo(p_inodo2) == -1) { //Liberamos inodo asociado a la entrada creada
    perror("Error: Inodo no liberado.\n");
            mi_signalSem();     // Semaforo sigue.
    return -1;
  }

  inodo1.nlinks++;
  inodo1.ctime = time(NULL);

  if (escribir_inodo(p_inodo1, inodo1) == -1) {
       mi_signalSem();     // Semaforo sigue.
       return -1;
    } //Salvamos inodo actualizado
    mi_signalSem();     // Semaforo sigue.

    return 0;
}

/******************************************************************************
* MÃ©todo: unlink()
* DescripciÃ³n: Borra la entrada de directorio especificada (no hay que olvidar
*              actualizar la cantidad de enlaces en el inodo) y, en caso de que 
*              fuera el Ãºltimo enlace existente, borrar el propio 
*              fichero/directorio.
*
*              Es decir que esta funciÃ³n nos servirÃ¡ tanto para borrar un 
*              enlace a un fichero como para eliminar un fichero o directorio 
*              que no contenga enlaces.
* ParÃ¡metros: 
* Devuelve:   
******************************************************************************/
 int mi_unlink(const char *camino){
         unsigned int p_inodo_dir=0, p_inodo=0, p_entrada=0;
    struct inodo inodo;
        struct entrada entrada;
    int nEntradas;
    mi_waitSem();
     int    r = buscar_entrada(camino, &p_inodo_dir, &p_inodo, &p_entrada, 0, 0);
    if(r < 0){
        printf("Error: No existe el archivo o el directorio.\n");
        mi_signalSem();
        return -1;
    }
        if (leer_inodo(p_inodo, &inodo) == -1) {
            mi_signalSem();
            return -1;}

    if(inodo.tipo == 'd' && inodo.tamEnBytesLog > 0){
        printf("No se puede eliminar el directorio porque no estÃ¡ vacÃ­a.\n");
        mi_signalSem();
        return 0;
    }
      if (leer_inodo(p_inodo_dir, &inodo) == -1) {
          mi_signalSem();
          return -1;}

        nEntradas = inodo.tamEnBytesLog/sizeof(struct entrada);

      if (p_entrada != nEntradas-1) { //Si la entrada a eliminar no es la ultima
    //Leemos ultima entrada
    if (mi_read_f(p_inodo_dir, &entrada, (nEntradas-1)*sizeof(struct entrada), sizeof(struct entrada)) == -1) {
      perror("Error: Ultima entrada no leida correctamente.\n");
      mi_signalSem();
      return -1;
    }
    //Colocamos ultima entrada en la posicion de la entrada que queremos eliminar
    if (mi_write_f(p_inodo_dir, &entrada, p_entrada*sizeof(struct entrada), sizeof(struct entrada)) == -1) {
      perror("Error: Ultima entrada no escrita correctamente.\n");
      mi_signalSem();
      return -1;
    }
  }

  if (mi_truncar_f(p_inodo_dir, (nEntradas-1)*sizeof(struct entrada)) == -1) {
    perror("Error: No se ha podido truncar el inodo correctamente.\n");
    mi_signalSem();
    return -1;
  }


if (leer_inodo(p_inodo, &inodo) == -1) {
    mi_signalSem();
    return -1;}

  if (inodo.nlinks == 1) {
    if (liberar_inodo(p_inodo) == -1) {
      perror("Error: Inodo no liberado\n");
      mi_signalSem();
      return -1;
    }
  } else {
    inodo.nlinks--;
    inodo.ctime = time(NULL);
    if (escribir_inodo(p_inodo, inodo) == -1) {
        mi_signalSem();
        return -1;} //Salvamos inodo actualizado
  }
    mi_signalSem();
     return 0;
 }