/******************************************************************************
*                                 FICHEROS.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÃN:
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÃCTOR MARIO MEDINA CABANELAS
* FECHA: 30 DE MARZO DE 2019.
******************************************************************************/


/******************************************************************************
*                           ARCHIVOS AUXILIARES
******************************************************************************/
#include "ficheros.h"                    // LibrerÃ­a personalizada.

/******************************************************************************
* MÃ©todo: mi_write_f()
* DescripciÃ³n:  Escribe el contenido de un buffer de memoria, âbuf_originalâ, 
*               en un fichero/directorio
* ParÃ¡metros:   ninodo        : Identificador de fichero/directorio (inodo)
*               *buf_original : Buffer donde se referencia el contenido
*               offset        : PosiciÃ³n inicial de escritura en inodo, bytes 
*                               lÃ³gicos.
*               nbytes        : NÃºmero de bytes que hay que escribir.
* Devuelve:     Cantidad de bytes escritos.
*               -1 si ha habido algÃºn problema.
******************************************************************************/
int mi_write_f(unsigned int ninodo, const void *buf_original, unsigned int offset, unsigned int nbytes){
    struct inodo inodo;                     // Estructura inodo
    int r = leer_inodo(ninodo, &inodo);     // Leemos el inodo ninodo
    // Si ha habido algÃºn error.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error my_writef(): Lectura inodo incorrecta.\n");
        return -1; 
    }
    // Si no tenemos los permisos de escritura.
    if((inodo.permisos & 2) != 2){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error my_writef(): No tiene permisos necesarios.\n");
        return -1;       
    }
    int bytesEscritos = 0;                          // NÃºmero de bytes escritos
    int firstblogico = offset/BLOCKSIZE;            // Primer bloque lÃ³gico
    int lastblogico = (offset+nbytes-1)/BLOCKSIZE;  // Ãltimo bloque lÃ³gico
    unsigned char buffer[BLOCKSIZE];                // Buffer auxiliar
    int bfisico;                                    // Bloque fÃ­sico
    /**************************************************************************
     *                    SI SÃLO SE VA A ESCRIBIR UN BLOQUE 
     *************************************************************************/
    if(firstblogico == lastblogico){
        int desp1 = offset % BLOCKSIZE;             // Desplazamiento en bytes.
        mi_waitSem();
        // Calculamos bloque fÃ­sico en el que trabajaremos.
        bfisico = traducir_bloque_inodo(ninodo,firstblogico,1);
        mi_signalSem();
        // Si ha habido algÃºn error.
        if(bfisico == -1){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            fprintf(stderr, "Error my_writef(): Error al traducir bloque inodo.\n");
            return -1;
        }
        // Leemos el bloque fÃ­sico
        r = bread(bfisico, buffer);
        // Si ha habido algÃºn error.
        if(r == -1){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            fprintf(stderr, "Error my_writef(): Error al leer el bloque.\n");
            return -1;
        }
        // Compiamos los datos en el bloque con offset
        memcpy(buffer + desp1, buf_original, nbytes);
        // Escribimos el buffer en el bloque fÃ­sico correspondiente.
        r = bwrite(bfisico, buffer);
        if(r == -1){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            fprintf(stderr, "Error my_writef(): Error al escribir el bloque.\n");
            return -1;
        }
        // Actualizamos la variable que controla el nÃºmero de bytes escritos.
        bytesEscritos = nbytes;
    }
    /**************************************************************************
     *                SI SÃLO SE VA A ESCRIBIR MÃS DE UN BLOQUE 
     *************************************************************************/
    if(firstblogico != lastblogico){
        mi_waitSem();
        // Calculamos el primer bloque fÃ­sico.
        int primerBF = traducir_bloque_inodo(ninodo, firstblogico, 1);
        mi_signalSem();
        /**********************************************************************
         *                     PRIMER BLOQUE A ESCRIBIR
         *********************************************************************/
        r = bread(primerBF, buffer);
        // Si ha habido algÃºn error.
        if(r == -1){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            fprintf(stderr, "Error my_writef(): Error al leer el bloque.\n");
            return -1;
        }
        int desp1 = offset % BLOCKSIZE;              // Desplazamiento en bytes
        // Copiamos la primera parte de los datos en el primer bloque con offset
        memcpy(buffer + desp1, buf_original, BLOCKSIZE-desp1);  
        // Escribimos el bloque 
        r = bwrite(primerBF, buffer);
        // Si ha habido algÃºn error.
        if(r == -1){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            fprintf(stderr, "Error my_writef(): Error al escribir el bloque.\n");
            return -1;
        }
        // Actualizamos los bytes escritos.
        bytesEscritos = BLOCKSIZE - desp1;
        /**********************************************************************
         *                  BLOQUES INTERMEDIOS A ESCRIBIR
         *********************************************************************/
        // Recorremos los bloques lÃ³gicos.
        for(int i = firstblogico+1; i < lastblogico; i++){
            mi_waitSem();
            // Calculamos el bloque fÃ­sico.
            bfisico = traducir_bloque_inodo(ninodo,i,1);
            mi_signalSem();
            // Escribimos todos los datos, ya que no debemos preservar nada.
            r = bwrite(bfisico, buf_original + (BLOCKSIZE - desp1) + (i - firstblogico - 1)*BLOCKSIZE);
            if(r == -1){
                // Imprimimos el error por la salida estÃ¡ndar de errores.
                fprintf(stderr, "Error my_writef(): Error al escribir el bloque.\n");
                return -1;
            }
            // Actualizamos los bytes leÃ­dos.
            bytesEscritos += BLOCKSIZE;
        }
        /**********************************************************************
         *                       ÃLTIMO BLOQUE A ESCRIBIR
         *********************************************************************/
        mi_waitSem();
        // Calculamos Ãºltimo bloque fÃ­sico.
        int ultimoBF = traducir_bloque_inodo(ninodo,lastblogico,1);
        mi_signalSem();
        r = bread(ultimoBF, buffer);
        if(r == -1){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            fprintf(stderr, "Error my_writef(): Error al leer el bloque.\n");
            return -1;
        }
        // Calculamos los bytes que hemos escrito.
        int bytesUltimos = (offset + nbytes -1)%BLOCKSIZE;
        // Actualizamos el buffer con los datos a escribir.
        memcpy(buffer, 
               buf_original+(BLOCKSIZE - desp1)+(lastblogico-firstblogico-1)*BLOCKSIZE,
               bytesUltimos+1);
        // Escribimos el bloque en disco.
        r = bwrite(ultimoBF, buffer);
        if(r == -1){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            fprintf(stderr, "Error my_writef(): Error al escribir el bloque.\n");
            return -1;
        }
        // Actualizamos los bloques escritos.
        bytesEscritos += bytesUltimos+1;
    }
    mi_waitSem();
    // leemos el inodo
    r = leer_inodo(ninodo, &inodo);
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error my_writef(): Error al leer el inodo.\n");
        mi_signalSem();
        return -1;
    }
    // Si hemos aÃ±adido bytes al fichero
    if(offset + nbytes > inodo.tamEnBytesLog){
        // Actualizamos el valor en el inodo
        inodo.tamEnBytesLog = offset + nbytes;
        inodo.ctime = time(NULL);
    }
    // Actualizamos fecha de modificaciÃ³n.
    inodo.mtime = time(NULL);
    // Actualizamos fecha de creaciÃ³n.
	//inodo.ctime = time(NULL);
    // Escribimos el inodo.
    r = escribir_inodo(ninodo, inodo);
    mi_signalSem();
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error my_writef(): Error al escribir el inodo.\n");
        return -1;
    }
    // Devolvemos los bytes escritos.
    return bytesEscritos;
}

/******************************************************************************
* MÃ©todo: mi_read_f()
* DescripciÃ³n:  Lee informaciÃ³n de un fichero/directorio correspondiente al 
*               nÃºmero de inodo pasado como argumento y lo almacena en el buffer
*               de memoria, buf_original.
* ParÃ¡metros:   ninodo        : NÃºmero de inodo correspondiente.
*               *buf_original : Buffer donde se almacenarÃ¡n los datos.
*               offset        : PosiciÃ³n de lectura inicial.
*               nbytes        : NÃºmero de bytes que se deben leer.
* Devuelve:     NÃºmero de bytes leÃ­dos.
*              -1 si ha habido algÃºn problema.
******************************************************************************/
int mi_read_f(unsigned int ninodo, void *buf_original, unsigned int offset, unsigned int nbytes){
    int bytesLeidos = 0;                // NÃºmero de bytes leÃ­dos
    struct inodo inodo;                 // Estructura inodo
    int r = leer_inodo(ninodo, &inodo); // Leer inodo
    int bytesALeer = nbytes;            // NÃºmero de bytes a leer.
    // SI ha habido algÃºn error.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error my_read_f(): Error al leer el inodo.\n");
        return -1;
    }
    // Si no tenemos los permisos de lectura
    if((inodo.permisos & 4) != 4){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error my_read_f(): No tiene permisos de lectura.\n");
        return -1;
    }
    // Si el offset es mayor al tamaÃ±o del fichero leemos 0 bytes.
    if(offset >= inodo.tamEnBytesLog){
        bytesLeidos = 0;
        return bytesLeidos;
    }
    // Si nos indican leer mÃ¡s de los bytes del fichero, sÃ³lo leeremos hasta EOF
    if(offset + nbytes >= inodo.tamEnBytesLog){
        bytesALeer = inodo.tamEnBytesLog - offset;
    } else {
        bytesALeer = nbytes;
    }
    unsigned char buffer[BLOCKSIZE];                // Buffer auxiliar
    memset(buffer, 0, BLOCKSIZE);
    int bfisico;                                    // Bloque fÃ­sico
    int firstblogico = offset/BLOCKSIZE;            // Primer bloque lÃ³gico
    int lastblogico = (offset+bytesALeer-1)/BLOCKSIZE;  // Ãltimo bloque lÃ³gico
    /**************************************************************************
     *                    SI SÃLO SE VA A LEER UN BLOQUE 
     *************************************************************************/
    if(firstblogico == lastblogico){
        int desp1 = offset % BLOCKSIZE;             // Desplazamiento en bytes.
        // Calculamos bloque fÃ­sico en el que trabajaremos.
        bfisico = traducir_bloque_inodo(ninodo,firstblogico,0);
        // Si ha habido algÃºn error.
        if(bfisico == -1){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            //fprintf(stderr, "Error my_read_f(): Error al traducir bloque inodo.\n");
            //return -1;
            bytesLeidos = bytesALeer;
            return bytesLeidos;
        }
        // Leemos el bloque fÃ­sico
        r = bread(bfisico, buffer);
        // Si ha habido algÃºn error.
        if(r == -1){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            fprintf(stderr, "Error my_read_f(): Error al leer el bloque.\n");
            return -1;
        }
        // Compiamos los datos en el bloque con offset
        memcpy(buf_original,buffer + desp1, bytesALeer);
        // Actualizamos la variable que controla el nÃºmero de bytes escritos.
        bytesLeidos = bytesALeer;
    }
    /**************************************************************************
     *                SI SÃLO SE VA A LEER MÃS DE UN BLOQUE 
     *************************************************************************/
    if(firstblogico != lastblogico){
        // Calculamos el primer bloque fÃ­sico.
        int primerBF = traducir_bloque_inodo(ninodo, firstblogico, 0);
        /**********************************************************************
         *                      PRIMER BLOQUE A LEER
         *********************************************************************/
        int desp1 = offset % BLOCKSIZE;              // Desplazamiento en bytes
        if(primerBF != -1){
            r = bread(primerBF, buffer);
            // Si ha habido algÃºn error.
            if(r == -1){
                // Imprimimos el error por la salida estÃ¡ndar de errores.
                fprintf(stderr, "Error my_read_f(): Error al leer el bloque.\n");
                return -1;
            }
            // Copiamos la primera parte de los datos en el primer bloque con offset
            memcpy(buf_original,buffer + desp1, BLOCKSIZE-desp1);  
            // Actualizamos los bytes leÃ­dos.
        }
        bytesLeidos = BLOCKSIZE - desp1;
        /**********************************************************************
         *                    BLOQUES INTERMEDIOS A LEER
         *********************************************************************/
        // Recorremos los bloques lÃ³gicos.
        for(int i = firstblogico+1; i < lastblogico; i++){
            // Calculamos el bloque fÃ­sico.
            bfisico = traducir_bloque_inodo(ninodo,i,0);
            if(bfisico != -1){
                // Leemos todos los datos, ya que no debemos preservar nada.
                r = bread(bfisico, buf_original + (BLOCKSIZE - desp1) + (i - firstblogico - 1)*BLOCKSIZE);
                if(r == -1){
                    // Imprimimos el error por la salida estÃ¡ndar de errores.
                    fprintf(stderr, "Error my_read_f(): Error al leer el bloque.\n");
                    return -1;
                }
            } 
            // Actualizamos los bytes leÃ­dos.
            bytesLeidos += BLOCKSIZE;
        }
        /**********************************************************************
         *                       ÃLTIMO BLOQUE A LEER
         *********************************************************************/
        // Calculamos Ãºltimo bloque fÃ­sico.
        int ultimoBF = traducir_bloque_inodo(ninodo,lastblogico,0);
        r = bread(ultimoBF, buffer);
        if(r == -1){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            fprintf(stderr, "Error my_read_f(): Error al leer el bloque.\n");
            return -1;
        }
        // Calculamos los bytes que hemos escrito.
        int bytesUltimos = (offset + bytesALeer -1)%BLOCKSIZE;
        // Actualizamos el buffer con los datos a leer.
        memcpy( buf_original+(BLOCKSIZE - desp1)+(lastblogico-firstblogico-1)*BLOCKSIZE,
                buffer,
                bytesUltimos+1);
        // Actualizamos los bloques escritos.
        bytesLeidos += bytesUltimos+1;
    }
    mi_waitSem();
    r = leer_inodo(ninodo, &inodo); // Leer inodo
    // Actualizamos fecha de acceso.
    inodo.atime = time(NULL);
    // Escribimos el inodo.
    r = escribir_inodo(ninodo, inodo);
    mi_signalSem();
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error my_writef(): Error al escribir el inodo.\n");
        return -1;
    }
    // Devolvemos nÃºmero de bytes leÃ­dos.
    return bytesLeidos;
}

/******************************************************************************
* MÃ©todo: mi_stat_f()
* DescripciÃ³n:  Devuelve la metainformaciÃ³m de un fichero/directorio 
*               correspondiente al nÃºmero de inodo pasado como argumento.
* ParÃ¡metros:   ninodo  : NÃºmero de inodo
*               *p_stat : Puntero a struct STAT
* Devuelve:      0 : Ha funcionado bien
*               -1 : Ha habido algÃºn error.
******************************************************************************/
int mi_stat_f(unsigned int ninodo, struct STAT *p_stat){
	struct inodo inodo;                     // Estructura inodo auxiliar
    int r = leer_inodo(ninodo, &inodo);     // Leemos el inodo.
    // Si ha habido algÃºn error.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error my_stat_f(): Error al leer el inodo.\n");
        return -1;
    }
    // Devolvemos meta informaciÃ³n por referencia. 
    p_stat->tipo = inodo.tipo;
    p_stat->permisos = inodo.permisos;
    p_stat->nlinks = inodo.nlinks;
    p_stat->tamEnBytesLog = inodo.tamEnBytesLog;
    p_stat->atime = inodo.atime;
    p_stat->mtime = inodo.mtime;
    p_stat->ctime = inodo.ctime;
    p_stat->numBloquesOcupados = inodo.numBloquesOcupados;
    // Todo ha ido bien.
    return 0;
}

/******************************************************************************
* MÃ©todo:  mi_chmod_f()
* DescripciÃ³n:  Cambia los permisos de un fichero/directorio correspondiente al 
*               nÃºmero de inodo pasado como argumento segÃºn indique el argumento.
* ParÃ¡metros:   ninodo   : NÃºmero de inodo
*               permisos : Permisos
* Devuelve:      0 : Ha funcionado bien
*               -1 : Ha habido algÃºn error.
******************************************************************************/
int mi_chmod_f(unsigned int ninodo, unsigned char permisos){
	struct inodo inodo;                     // Estructura inodo auxiliar.
    mi_waitSem();
    int r = leer_inodo(ninodo, &inodo);     // Leemos el inodo.
    // Si ha habido algÃºn error.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error mi_chmod_f(): Error al leer el inodo.\n");
        return -1;
    }
    inodo.permisos = permisos;              // Cambiamos permisos
    inodo.ctime = time(NULL);               // Cambiamos ctime.
    r = escribir_inodo(ninodo, inodo);
    mi_signalSem();
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error mi_chmod_f(): Error al escribir el inodo.\n");
        return -1;
    }
    return 0;
}

/******************************************************************************
* MÃ©todo: mi_truncar_f()
* DescripciÃ³n:  Trunca un fichero/directorio a los bytes indicados, liberando 
*               los bloques necesarios.
* ParÃ¡metros:   ninodo : NÃºmero de inodo a truncar.
*               nbytes : NÃºmero de bytes a truncar.
* Devuelve:     NÃºmero de bloques liberados.
*              -1 : Ha ocurrido algÃºn error.
******************************************************************************/
int mi_truncar_f(unsigned int ninodo, unsigned int nbytes){
    struct inodo inodo;                         // Estructura inodo auxiliar.
    int blogico = 0;                            // Bloque lÃ³gico
    int r = leer_inodo(ninodo, &inodo);         // Leemos el inodo es disco
    int bloques_liberados = 0;                  // bloques liberados
    // Si ha habido un error al escribir el inodo.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error mi_truncarf(): Error al leer el inodo.\n");
        return -1;
    }
    // Si no tiene permisos de escritura.
    if((inodo.permisos & 2) != 2){
        fprintf(stderr, "Error mi_truncarf(): No tiene permisos adecuados.\n");
        return -1;
    }
    // Si queremos truncar una zona que excede del fichero.
    if(nbytes > inodo.tamEnBytesLog){
        fprintf(stderr, "Error mi_truncarf(): No se puede truncar, recurso demasiado corto.\n");
        return -1;
    }
    // Calculamos los bloques lÃ³gicos.
    if ((nbytes % BLOCKSIZE) == 0) {
        blogico = (nbytes / BLOCKSIZE);
    } else {
        blogico = (nbytes / BLOCKSIZE) + 1;
    }
    bloques_liberados = liberar_bloques_inodo(ninodo, blogico);
    if(bloques_liberados == -1){
        fprintf(stderr, "Error mi_truncarf(): No se ha podido liberar los bloques inodo.\n");
        return -1;
    }
    inodo.mtime = time(NULL);                       // Actualizamos hora-dia sustituciÃ³n. 
    inodo.ctime = time(NULL);                       // Actualizamos la hora de creaciÃ³n.
    inodo.tamEnBytesLog = nbytes;                   // TamaÃ±o en bytes.
    inodo.numBloquesOcupados -= bloques_liberados;
    r = escribir_inodo(ninodo, inodo);
    if(r == -1){
        fprintf(stderr, "Error mi_truncarf(): No se haposico escribir el inodo.\n");
        return -1;
    }
    // Si llegamos aquÃ­ todo ha ido bien.
    return bloques_liberados;
}