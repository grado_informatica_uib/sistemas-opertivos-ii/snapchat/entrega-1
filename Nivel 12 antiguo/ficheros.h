/******************************************************************************
*                               FICHEROS.H 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÃN:
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÃCTOR MARIO MEDINA CABANELAS
* FECHA: 30 DE MARZO DE 2019.
******************************************************************************/
#include "ficheros_basico.h"
#include <time.h>

struct STAT {     // comprobar que ocupa 128 bytes haciendo un sizeof(inodo)!!!
   char tipo;     // Tipo ('l':libre, 'd':directorio o 'f':fichero)
   char permisos; // Permisos (lectura y/o escritura y/o ejecuciÃ³n)
   time_t atime; // Fecha y hora del Ãºltimo acceso a datos: atime
   time_t mtime; // Fecha y hora de la Ãºltima modificaciÃ³n de datos: mtime
   time_t ctime; // Fecha y hora de la Ãºltima modificaciÃ³n del inodo: ctime

   /* comprobar el tamaÃ±o del tipo time_t para vuestra plataforma/compilador:
   printf ("sizeof time_t is: %d\n", sizeof(time_t)); */

   unsigned int nlinks;             // Cantidad de enlaces de entradas en directorio
   unsigned int tamEnBytesLog;      // TamaÃ±o en bytes lÃ³gicos. Se actualizarÃ¡ al escribir si crece
   unsigned int numBloquesOcupados; // Cantidad de bloques ocupados zona de datos
};

/******************************************************************************
* MÃ©todo: mi_write_f()
* DescripciÃ³n:  Escribe el contenido de un buffer de memoria, âbuf_originalâ, 
*               en un fichero/directorio
* ParÃ¡metros:   ninodo        : Identificador de fichero/directorio (inodo)
*               *buf_original : Buffer donde se referencia el contenido
*               offset        : PosiciÃ³n inicial de escritura en inodo, bytes 
*                               lÃ³gicos.
*               nbytes        : NÃºmero de bytes que hay que escribir.
* Devuelve:     Cantidad de bytes escritos.
*               -1 si ha habido algÃºn problema.
******************************************************************************/
int mi_write_f(unsigned int ninodo, const void *buf_original, unsigned int offset, unsigned int nbytes); 

/******************************************************************************
* MÃ©todo: mi_read_f()
* DescripciÃ³n:  Lee informaciÃ³n de un fichero/directorio correspondiente al 
*               nÃºmero de inodo pasado como argumento y lo almacena en el buffer
*               de memoria, buf_original.
* ParÃ¡metros:   ninodo        : NÃºmero de inodo correspondiente.
*               *buf_original : Buffer donde se almacenarÃ¡n los datos.
*               offset        : PosiciÃ³n de lectura inicial.
*               nbytes        : NÃºmero de bytes que se deben leer.
* Devuelve:     NÃºmero de bytes leÃ­dos.
*              -1 si ha habido algÃºn problema.
******************************************************************************/
int mi_read_f(unsigned int ninodo, void *but_original, unsigned int offset, unsigned int nbytes);

/******************************************************************************
* MÃ©todo: mi_stat_f()
* DescripciÃ³n:  Devuelve la metainformaciÃ³m de un fichero/directorio 
*               correspondiente al nÃºmero de inodo pasado como argumento.
* ParÃ¡metros:   ninodo  : NÃºmero de inodo
*               *p_stat : Puntero a struct STAT
* Devuelve:      0 : Ha funcionado bien
*               -1 : Ha habido algÃºn error.
******************************************************************************/
int mi_stat_f(unsigned int ninodo, struct STAT *p_stat);

/******************************************************************************
* MÃ©todo:  mi_chmod_f()
* DescripciÃ³n:  Cambia los permisos de un fichero/directorio correspondiente al 
*               nÃºmero de inodo pasado como argumento segÃºn indique el argumento.
* ParÃ¡metros:   ninodo   : NÃºmero de inodo
*               permisos : Permisos
* Devuelve:      0 : Ha funcionado bien
*               -1 : Ha habido algÃºn error.
******************************************************************************/
int mi_chmod_f(unsigned int ninodo, unsigned char permisos);

/******************************************************************************
* MÃ©todo: mi_truncar_f()
* DescripciÃ³n:  Trunca un fichero/directorio a los bytes indicados, liberando 
*               los bloques necesarios.
* ParÃ¡metros:   ninodo : NÃºmero de inodo a truncar.
*               nbytes : NÃºmero de bytes a truncar.
* Devuelve:     NÃºmero de bloques liberados.
*              -1 : Ha ocurrido algÃºn error.
******************************************************************************/
int mi_truncar_f(unsigned int ninodo, unsigned int nbytes);