/******************************************************************************
*                            FICHEROS_BASICOS.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERACTIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÃN:
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÃCTOR MARIO MEDINA CABANELAS
* FECHA: 22 DE MARZO DE 2019.
******************************************************************************/


/******************************************************************************
*                           ARCHIVOS AUXILIARES
******************************************************************************/
#include "ficheros_basico.h"                    // LibrerÃ­a personalizada.

/******************************************************************************
* MÃ©todo: tamMb()
* DescripciÃ³n:  Calcula el nÃºmero de bloques necesarios para alojar el 
*               mapa de bits.
* ParÃ¡metros:   nbloques : numero de bloques del disco.
* Devuelve:     tamMB    : nÃºmero de bloques del MB.
******************************************************************************/
int tamMB(unsigned int nbloques){
    int tamMB = (nbloques/8)/BLOCKSIZE; // Calcula tamaÃ±o MB (parte entera)
    // Si necesita 1 bloque mÃ¡s para la parte decimal
    if((nbloques/8)%BLOCKSIZE > 0){     
        tamMB++;                        // AÃ±adimos un bloque (parte decimal)
    }
    // Devolvemos el tamaÃ±o del Mapa de Bits
    return tamMB;
}

/******************************************************************************
* MÃ©todo: tamAI()
* DescripciÃ³n:  Calcula el numero de bloques necesarios para alojar el Array 
*               de inodos. 
* ParÃ¡metros:   ninodos : NÃºmero de inodos del sistema.
* Devuelve:     tamAI   : NÃºmero de bloques para alojar el Array de inodos. 
******************************************************************************/
int tamAI(unsigned int ninodos){
    int tamAI = ninodos*INODOSIZE/BLOCKSIZE;// Calcula tamaÃ±o AI (p. entera)
    // Si necesita 1 bloque mÃ¡s para la parte decimal
    if((ninodos*INODOSIZE)%BLOCKSIZE > 0){
        tamAI++;                            // AÃ±adimos un bloque (p. decimal)
    }
    // Devolvemos el tamaÃ±o del Array de Inodos.
    return tamAI;
}

/******************************************************************************
* MÃ©todo: initSB()
* DescripciÃ³n:  Inicializa el superbloque con los datos apropiados.
* ParÃ¡metros:   nbloques : NÃºmero de bloques del disco.
*               inodos   : NÃºmero de inodos del disco.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initSB(unsigned int nbloques, unsigned int ninodos){
    // DeclaraciÃ³n de nuestro superbloque
    struct superbloque SB;              
    // NÃºmero del primer bloque del Mapa de Bits.
    SB.posPrimerBloqueMB = posSB + tamSB;
    // NÃºmero del Ãºltimo bloque del Mapa de Bits.
    int var_tamMB = tamMB(nbloques)-1;
    SB.posUltimoBloqueMB = SB.posPrimerBloqueMB + var_tamMB;
    // NÃºmero del primer boqque del Array de Inodos.
    SB.posPrimerBloqueAI = SB.posUltimoBloqueMB + 1;
    // NÃºmero del Ãºltimo bloque del Array de Inodos.
    int var_tamAI = tamAI(ninodos)-1;
    SB.posUltimoBloqueAI = SB.posPrimerBloqueAI + var_tamAI;
    // Numero del primer bloque de datos.
    SB.posPrimerBloqueDatos = SB.posUltimoBloqueAI + 1;
    // NÃºmero del Ãºltimo bloque de datos.
    SB.posUltimoBloqueDatos = nbloques - 1;
    // PosiciÃ³n inodo raÃ­z.
    SB.posInodoRaiz = 0;
    // PosiciÃ³n del primer inodo libre.
    SB.posPrimerInodoLibre = 0; // ESTO PARECE QUE SE HA DE CAMBIAR AL FINAL.
    // NÃºmero de bloques libres
    SB.cantBloquesLibres = nbloques;
    // NÃºmero de inodos libres
    SB.cantInodosLibres = ninodos;
    // Cantidad total de bloques
    SB.totBloques = nbloques;
    // Cantidad total de inodos
    SB.totInodos = ninodos;
    // Escribimos los datos en la memoria superbloque.
    int r = bwrite(posSB, &SB);
    // Comprobamos si ha habido algÃºn error.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Escritura SuperBloque incorrecta.\n");
        return r;
    }
    return 0;
}

/******************************************************************************
* MÃ©todo: initMB()
* DescripciÃ³n:  Inicializa los valores del Mapa de Bits a 0. 
* ParÃ¡metros:   Ninguno.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initMB(){
    // Valor que vamos a escribir en el MB al inicializarlo.
    unsigned char initMBValue [BLOCKSIZE];
    memset(initMBValue,0,BLOCKSIZE);
    // DeclaraciÃ³n Super Bloque.
    struct superbloque SB;

    // Leemos el superbloque y lo guardamos en SB
    int r = bread(posSB, &SB);
    // Si se ha producido un error.cd
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Lectura Mapa de Bits incorrecta.\n");
        return r;
    }

    // Recorremos el Mapa de Bits para inicializar sus valores.
    for(int i = SB.posPrimerBloqueMB; i <= SB.posUltimoBloqueMB; i++)
    {
        // Escribimos el initMBValue en el MB.
        int r = bwrite(i,initMBValue);
        // Si se ha producido un error
        if(r == -1){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            fprintf(stderr, "Error: Escritura Mapa de Bits incorrecta.\n");
            return r;
        }
    }
    // Actualizamos el MB con los datos ya ocupados (SB, )
    for(int i = posSB; i <= SB.posUltimoBloqueAI; i++){
        escribir_bit(i,1);          // bloque ocupado
        SB.cantBloquesLibres--;     // Restamos uno a los bloques libres.
    }
    r = bwrite(posSB, &SB);
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Escritura actualizacion Superbloque incorrecta.\n");
        return r;
    }
    // Si hemos llegado hasta aquÃ­ todo ha ido bien: Devolvemos 0;
    return 0;
}

/******************************************************************************
* MÃ©todo: initAI()
* DescripciÃ³n:  Inicializa el Array de Inodos. Al principio todos los inodos 
*               estÃ¡n libres y por tanto enlazados.
* ParÃ¡metros:   Ninguno.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initAI(){
    // DeclaraciÃ³n de array de inodos par luego escribirlos.
    struct inodo inodos[BLOCKSIZE/INODOSIZE];
    // DeclaraciÃ³n del superbloque para coger datos.
    struct superbloque SB;
    // Leemos el superbloque y lo almacenamos en SB
    int r = bread(posSB, &SB);
    // Si ha habido algÃºn error.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Lectura SuperBloque incorrecta.\n");
        return r;
    }
    // Contador de Inodo para hacer el enlace entre Inodos.
    int contInodos = SB.posPrimerInodoLibre + 1;
    // Recorremos los bloques que contienen el Array de Inodos.
    for(int i = SB.posPrimerBloqueAI; i <= SB.posUltimoBloqueAI;i++)
    {        
        // Recorremos cada Inodo dentro del bloque
        for(int j = 0; j < BLOCKSIZE / INODOSIZE ; j++)
        {
            // Asignamos el tipo 'libre'
           inodos[j].tipo = 'l'; // libre
           // Si no hemos llegado al Ãºltimo inodo
           if(contInodos < SB.totInodos){
                // Lo enlazamos con el siguiente.
			    inodos[j].punterosDirectos[0] = contInodos;
				contInodos++;
           }
           else {
                // El Ãºltimo elemento no se puede enlazar con nada.
				inodos[j].punterosDirectos[0] = UINT_MAX;
                j = BLOCKSIZE/INODOSIZE;
           }
        }
        // Escribimos el grupo de inodos en el Array de Inodos,
        r = bwrite(i, inodos);
        // SI ha habido algun error.
        if (r == -1) {
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            fprintf(stderr, "Error: Escritura Inodos del bloque incorrecta.\n");
            return r;     
        }  
    }
    // Si hemos llegado hasta aquÃ­ es que ha ido todo correctamente.
    return 0;
}


/******************************************************************************
* MÃ©todo: escribir_bit()
* DescripciÃ³n:  Escribe el valor indicado por elparÃ¡metro bit (0 Ã³ 1) en un 
*               determinado bit del MP que representa el bloque nbloque.
* ParÃ¡metros:   unsigned int nbloque :  NÃºmero de bloque.  
*               unsigned int bit     :  Bit a escribir.
* Devuelve:     0   : Ha funcionado correctamente.
*              -1   : Ha habido algÃºn problema.
******************************************************************************/
int escribir_bit(unsigned int nbloque, unsigned int bit){
    if ((bit!=0) && (bit!=1)) {
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: el bit a escribir debe ser 0 Ã³ 1.\n");
        return -1; 
    }
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algÃºn problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    int posbyte = nbloque / 8;              // Calculamos el posbyte
    int posbit = nbloque % 8;               // Calculamos el posbit
    int nbloqueMB = posbyte / BLOCKSIZE;    // Calculamos el bloqueMB
    int nbloqueabs = nbloqueMB + SB.posPrimerBloqueMB;  // Bloque absoluto
    unsigned char bufferMB[BLOCKSIZE];      // Buffer de MB
    r = bread(nbloqueabs, &bufferMB);       // Leemos el buffer del disco.
    // Si ha habido algÃºn problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Lectura del bloque MB incorrecta.\n");
        return r;    
    }
    posbyte = posbyte % BLOCKSIZE; // Calculo byte implicado en el bloqueMB.
    unsigned char mascara = 128;   // MÃ¡scara para setear el valor del bit.
    mascara >>= posbit;            // Desplazamos el bit conforme posbit.
    // Si el bit es 0 (libre)
    if(bit == 0){
        bufferMB[posbyte] &= ~mascara;  // Actualizamos bufferMB[posbyte]
    }
    // Si el bit es 1 (ocupado)
    else{
        bufferMB[posbyte] |= mascara;   // Actualizamos bufferMB[posbyte]
    }
    r = bwrite(nbloqueabs, bufferMB);   // Escribimos el bloque en disco.
    // Si ha habido algÃºn problema en la escritura.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Escrita del bloque MB incorrecta.\n");
        return r;    
    }
    // Si hemos llegado hasta aquÃ­ significa que todo ha ido bien y return 0.
    return 0;
}

/******************************************************************************
* MÃ©todo: leer_bit()
* DescripciÃ³n:  Lee un determinado bit del MB y devuelve el valor del bit 
*               leÃ­do.
* ParÃ¡metros:   nbloque  : NÃºmero de bloque que queremos leer.
* Devuelve:     El valor del bit leÃ­do.
*               -1 si ha habido algun problema.
******************************************************************************/
unsigned char leer_bit(unsigned int nbloque){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algÃºn problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    int posbyte = nbloque / 8;              // Calculamos el posbyte
    int posbit = nbloque % 8;               // Calculamos el posbit
    int nbloqueMB = posbyte / BLOCKSIZE;    // Calculamos el bloqueMB
    int nbloqueabs = nbloqueMB + SB.posPrimerBloqueMB;  // Bloque absoluto
    unsigned char bufferMB[BLOCKSIZE];      // Buffer de MB
    r = bread(nbloqueabs, &bufferMB);       // Leemos el buffer del disco.
    // Si ha habido algÃºn problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Lectura del bloque MB incorrecta.\n");
        return r;    
    }
    posbyte = posbyte % BLOCKSIZE; // Calculo byte implicado en el bloqueMB.
    unsigned char mascara = 128;   // MÃ¡scara para setear el valor del bit.
    mascara >>= posbit;            // Desplazamos el bit conforme posbit.
    mascara &= bufferMB[posbyte];  // Obtenemos en la mascara el valor del byte
    mascara >>= (7-posbit);        // Desplezamos a la derecha
    printf("[leer_bit()â nbloque: %d, posbyte:%d, posbit:%d, nbloqueMB:%d, nbloqueabs:%d)]\n", nbloque, posbyte, posbit, nbloqueMB, nbloqueabs );
    return mascara;
}

/******************************************************************************
* MÃ©todo: reservar_bloque()
* DescripciÃ³n:  Encuentra el primer bloque libre, consultando el MB, lo ocupa 
*               y devuelve su posiciÃ³n.
* ParÃ¡metros:   Ninguno
* Devuelve:     PosiciÃ³n del primer bloque libre.
*              -1 : si ha habido algÃºn problema.
******************************************************************************/
int reservar_bloque(){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algÃºn problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Si hay bloques libres.
    if(SB.cantBloquesLibres == 0){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: No hay bloques libres disponibles.\n");
        return -1;       
    }
    /**********************************************************
     *  Encontramos un bloque MB que tiene un 0.
     **********************************************************/
    unsigned char bufferMB[BLOCKSIZE];      // Buffer de MB
    unsigned char bufferAux[BLOCKSIZE];     // Buffer auxiliar (comparar)
    memset(bufferAux, 255, BLOCKSIZE);      // Set buffer 1111 1111
    int posBloqueMB = SB.posPrimerBloqueMB;          // Posicion bloque MB con 0
    int encontrado = 0;       // Control de bloque encontrado
    // Recorrido de MB hasta encontrar bloque que tenga algÃºn 0.
    while(posBloqueMB <= SB.posUltimoBloqueMB && encontrado != 1){
            bread(posBloqueMB, bufferMB);
            if(memcmp(bufferAux, bufferMB, BLOCKSIZE) > 0){
                encontrado = 1;        // Lo encontramos
            }
            else {
                posBloqueMB++;
            }
    }
    /**********************************************************
     *  Encontramos el byte MB que tiene un 0.
     **********************************************************/
    int byteEncontrado = 0;    // Control de byte encontrado
    int posbyte = 0;               // PosiciÃ³n byte encontrado.
    // Recorremos el bloque en busca de byte con algÃºn 0.
    while(posbyte <= BLOCKSIZE && byteEncontrado != 1){
        // Si encontramos un byte con 0.
        int i = 255 - bufferMB[posbyte];
        if(i> 0) byteEncontrado = 1;         // Lo encontramos
        else posbyte++;
    }
    /**********************************************************
     *  Encontramos el bit MB que vale 0.
     **********************************************************/
    unsigned char mascara = 128;        // 1000 0000
    int posbit = 0;                     // PosiciÃ³n del bit 0.
    // Mientras no encontremos el bit con 0.
    while((bufferMB[posbyte] & mascara) != 0){
        posbit++;
        bufferMB[posbyte] <<= 1; //desplazamiento izquierda.
    }
    // Calculamos el nÃºmero de bloque resultando.
    int nbloque = ((posBloqueMB - SB.posPrimerBloqueMB)*BLOCKSIZE + posbyte)*8 + posbit;
    // Escribimos en el MB que el bloque nbloque estarÃ¡ ocupado.
    r = escribir_bit(nbloque, 1);
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Fallo en la reserva del bloque.\n");
        return -1;  
    }
    SB.cantBloquesLibres--;        // Actualizamos los bloques libres en SB.
    memset(bufferAux, 0, BLOCKSIZE);      // Set buffer 0000 0000
    r = bwrite(nbloque, &bufferAux);    // Reseteamos los valores del bloque a 0
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: No se pudo resetear valores bloque.\n");
        return -1;  
    }
    r = bwrite(posSB, &SB);
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: No se pudo actualizar superbloque.\n");
        return -1;  
    }
    // Si hemos llegado aquÃ­ todo ha ido bien, devolvemos nÃºmero de bloque.
    return nbloque;
}

/******************************************************************************
* MÃ©todo: liberar_bloque()
* DescripciÃ³n:  Libera un bloque determinado.
* ParÃ¡metros:   nbloque : NÃºmero del bloque de se desea liberar.
* Devuelve:     nbloque : Si ha funcionado correctamente.
*              -1       : Si ha habido algÃºn error.
******************************************************************************/
int liberar_bloque(unsigned int nbloque){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algÃºn problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Actualizamos el valor del bloque en MB
    r = escribir_bit(nbloque, 0);
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: No se pudo liberar el bloque.\n");
        return -1;  
    }
    // Aumentos el nÃºmero de bloques libres.
    SB.cantBloquesLibres++;
    // Actualizamos el superbloque en el disco virtual
    r = bwrite(posSB, &SB);
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: No se ha podido actualizarel superbloque.\n");
        return r; 
    }
    // Devolvemos el nÃºmero de bloque liberado.
    return nbloque;
}

/******************************************************************************
* MÃ©todo: escribir_inodo()
* DescripciÃ³n:  Escribe el contenido de una variable de tipo struct inodo en 
*               un determinado inodo del array de inodos.
* ParÃ¡metros:   ninodo  : NÃºmero del inodo donde se quiere escribir.
*               inodo   : Inodo a escribir.
* Devuelve:     0  : Si ha funcionado correctamente.
*              -1  : Si ha habido algÃºn error.
******************************************************************************/
int escribir_inodo(unsigned int ninodo, struct inodo inodo){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algÃºn problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Calculamos el bloque del inodo que queremos modificar.
    int posbloqueinodo = (ninodo/(BLOCKSIZE/INODOSIZE)) + SB.posPrimerBloqueAI;
    // Buffer que utilizaremos para modificar el array de inodos. 
    struct inodo inodos[BLOCKSIZE/INODOSIZE];
    // Leemos el bloque del disco
    r = bread(posbloqueinodo, &inodos);
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Lectura del Array de Inodos incorrecta.\n");
        return r;    
    }
    // Calculamos la posiciÃ³n del inodo dentro del bloque 
    int posInodo = ninodo%(BLOCKSIZE/INODOSIZE);
    // Modificamos el inodo dentro del buffer.
    inodos[posInodo] = inodo;
    // Escribimos el buffer en el disco.
    r = bwrite(posbloqueinodo, &inodos);
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Escritura del Array de Inodos incorrecta.\n");
        return r;    
    }
    // Si hemos llegado hasta aquÃ­ significa que todo ha ido bien.
    return 0;
}

/******************************************************************************
* MÃ©todo: leer_inodo()
* DescripciÃ³n:  Lee un determinado inodo del array de inodos para volcarlo en 
*               una variable tipo struct inodo pasada por referencia.
* ParÃ¡metros:   ninodo : NÃºmero de inodo que se quiere leer.
*               *inodo : Puntero a un inodo donde se guardarÃ¡n los datos leÃ­dos
* Devuelve:     0  : Si ha funcionado correctamente.
*              -1  : Si ha habido algÃºn error.
******************************************************************************/
int leer_inodo(unsigned int ninodo, struct inodo *inodo){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algÃºn problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Calculamos el bloque del inodo que queremos modificar.
    int posbloqueinodo = (ninodo/(BLOCKSIZE/INODOSIZE)) + SB.posPrimerBloqueAI;
    // Buffer que utilizaremos para modificar el array de inodos. 
    struct inodo inodos[BLOCKSIZE/INODOSIZE];
    // Leemos el bloque del disco
    r = bread(posbloqueinodo, &inodos);
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Lectura del Array de Inodos incorrecta.\n");
        return r;    
    }
    // Calculamos la posiciÃ³n del inodo dentro del bloque 
    int posInodo = ninodo%(BLOCKSIZE/INODOSIZE);
    // Devolvemos el inodo modificando el parÃ¡metro*inodo
    *inodo =  inodos[posInodo];
    // Si hemos llegado hasta aquÃ­ es que todo ha ido bien.
    return 0;
}

/******************************************************************************
* MÃ©todo: reservar_inodo()
* DescripciÃ³n:  Encuentra el primer inodo libre, lo reserva, devuelve su 
*               nÃºmero y actualiza la lista enlazada de inodos libres.
* ParÃ¡metros:   tipo     : (directorio | fichero | libre)
*               permisos : Permisos que se le atribuyen (0-7)
* Devuelve:     NÃºmero de inodo reservado.
*               -1 : Si ha habido algÃºn problema.
******************************************************************************/
int reservar_inodo(unsigned char tipo, unsigned char permisos){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algÃºn problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Si hay bloques libres.
    if(SB.cantInodosLibres == 0){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: No hay inodos libres disponibles.\n");
        return -1;       
    }
    // PosiciÃ³n del inodo que vamos a reservar.
    int posInodoReservado = SB.posPrimerInodoLibre;
    // Declaramos una estructura inodo.
    struct inodo inodo;
    // Leermos el inodo
    leer_inodo(posInodoReservado, &inodo);
    // Hacemos apuntar al superbloque al siguiente inodo libre.
    SB.posPrimerInodoLibre = inodo.punterosDirectos[0];
    // Inicialiamos el inodo.
    inodo.tipo = tipo;
    inodo.permisos = permisos;
    inodo.nlinks = 1;
    inodo.tamEnBytesLog = 0;
    inodo.atime = time(NULL);
    inodo.mtime = time(NULL);
    inodo.ctime = time(NULL);
    inodo.numBloquesOcupados = 0;
    for(int i = 0; i < (int) (sizeof(inodo.punterosDirectos)/sizeof(int)); i++){
       inodo.punterosDirectos[i] = 0;
    }
    for(int i = 0; i < (int) (sizeof(inodo.punterosIndirectos)/sizeof(int)); i++){
       inodo.punterosIndirectos[i] = 0;
    }
    // Escribimos el inodo
    r = escribir_inodo(posInodoReservado, inodo);
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Escritura del inodo incorrecta.\n");
        return r;    
    }
    // Actualizamos la cantidad inodos libres.
    SB.cantInodosLibres--;
    // Escribimos el SB en el disco.
    r = bwrite(posSB, &SB);
    // Si ha habido algÃºn error.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Escritura en superbloque incorrecta.\n");
        return r;    
    }
    // Si hemos llegado aquÃ­ todo ha ido bien.
    return posInodoReservado;
}

/******************************************************************************
* MÃ©todo: obtener_nrangoBL()
* DescripciÃ³n: Obtiene el rando de punteros en el que se sitÃºa el bloque lÃ³gico 
*              que buscamos y la direcciÃ³n almaenada en el puntero 
*              correspondiente del inodo.
* ParÃ¡metros:  inodo    : inodo en el cual buscaremos en el bloque.
*              nblogico : nÃºmero de bloque lÃ³gico.
*              *ptr     : puntero hacia atributo inodo correspondiente.
* Devuelve:    0    : Si pertenece a bloque directo.
*              1    : Si pertenece a bloque indirecto 1
*              2    : Si pertenece a bloque indirecto 2
*              3    : Si pertenece a bloque indirecto 3
*             -1    : Si ha habido algÃºn error.
******************************************************************************/
int obtener_nrangoBL(struct inodo inodo, unsigned int nblogico, unsigned int  *ptr){
    // Comprobamos si el bloque lÃ³gico estÃ¡ fuera de rango
    if(nblogico < 0 || nblogico >= INDIRECTOS2){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: nÃºmero de bloque lÃ³gico fuera de rango.\n");
        return -1; 
    }
    // Si pertenece a los punteros directos
    if(nblogico < DIRECTOS){
        *ptr=inodo.punterosDirectos[nblogico];    
        return 0;
    }
    // Si pertenece a los punteros indirectos 1
    if(nblogico < INDIRECTOS0){
        *ptr=inodo.punterosIndirectos[0];     
        return 1;
    }
    // Si pertenecen a los punteros indirectos 2
    if(nblogico < INDIRECTOS1){
        *ptr=inodo.punterosIndirectos[1];     
        return 2;
    }
    // Si pertenecen a los punteros indirectos 3
    if(nblogico < INDIRECTOS2){
        *ptr=inodo.punterosIndirectos[2];     
        return 3;
    }
    // Si por lo que se ha habido un error.
    fprintf(stderr, "Error: error inesperado en obtener_nrangoBL().\n");
    return -1; 
}

/******************************************************************************
* MÃ©todo: obtener_indice()
* DescripciÃ³n: A partir de un numero de bloque lÃ³gico y un nivel de punteros, 
*              obtiene el el Ã­ndice que almacena el bloque fÃ­sico de los datos. 
* ParÃ¡metros:  nblogico       : nÃºmero de bloque lÃ³gico.
*              nivel_punteros : Determina quÃ© nivel hemos solicitado.
* Devuelve:    Ã¯ndice solicitado
*              -1 si ha habido algÃºn error. 
******************************************************************************/
int obtener_indice(int nblogico, int nivel_punteros){
    // Comprobamos si el bloque lÃ³gico estÃ¡ fuera de rango
    if(nblogico < 0 || nblogico >= INDIRECTOS2){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: nÃºmero de bloque lÃ³gico fuera de rango.\n");
        return -1; 
    }
    int indice;
    // Si pertenece a los punteros directos
    if(nblogico < DIRECTOS){
        indice = nblogico;    
        return indice;  
    }
    else {
        // Si pertenece a los punteros indirectos0
        if(nblogico < INDIRECTOS0){
            indice = nblogico - DIRECTOS;
            return indice;
        }
        else{
            // Si pertenece a los punterior indirectos1
            if(nblogico < INDIRECTOS1){
                // Nivel 2
                if(nivel_punteros == 2){
                    indice = (nblogico - INDIRECTOS0) / NPUNTEROS;
                    return indice;
                }
                // Nivel 1
                if(nivel_punteros == 1){
                    indice = (nblogico - INDIRECTOS0) % NPUNTEROS;
                    return indice;
                }
            }
            else {
                // Si pertenece a los punteros indirectos2
                if(nblogico < INDIRECTOS2){
                    // Nivel 3
                    if(nivel_punteros == 3){
                        indice = (nblogico - INDIRECTOS1)/(NPUNTEROS*NPUNTEROS);
                        return indice;
                    }
                    // Nivel 2
                    if(nivel_punteros == 2){
                        indice = ((nblogico-INDIRECTOS1)%(NPUNTEROS*NPUNTEROS))/NPUNTEROS;
                        return indice;
                    }
                    // Nivel 1
                    if(nivel_punteros == 1){
                        indice = ((nblogico-INDIRECTOS1)%(NPUNTEROS*NPUNTEROS))%NPUNTEROS;
                        return indice;
                    }
                }
            }
        }
    }
    // Si llegamos hasta aquÃ­ ha habido un error
    return -1;
}

/******************************************************************************
* MÃ©todo: traducir_bloque_inodo()
* DescripciÃ³n:  Se encarga de obtener el nÃºmero de bloque fÃ­sico correspondiente 
*               a un bloque lÃ³gico determinado del inodo indicado.
* ParÃ¡metros:   ninodo    : NÃºmero de inodo que queremos traducir.
*               nblogico  : NÃºmero de bloque lÃ³gico.
*               reservvar : 0 (consulta) | 1 (escritura)
* Devuelve:     NÃºmero de bloque fÃ­sico.
******************************************************************************/
int traducir_bloque_inodo(unsigned int ninodo, unsigned int nblogico, char reservar){
    struct inodo inodo;     // Estructura auxiliar inodo
    unsigned int ptr, ptr_ant, salvar_inodo, nRangoBL, nivel_punteros, indice;
    int buffer[NPUNTEROS];
    // Leemos el inodo correspondiente y lo guardamos en inodo.
    int r = leer_inodo (ninodo, &inodo);
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Lectura de inodo incorrecta.\n");
        return r;    
    }
    // Inicializamos variables.
    ptr = 0;
    ptr_ant = 0;
    salvar_inodo = 0;
    indice = 0;
    nRangoBL = obtener_nrangoBL(inodo, nblogico, &ptr);
    if(nRangoBL == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error: Error al obtener el rangoBL.\n");
        return r;    
    }
    nivel_punteros = nRangoBL;
    // Nos pasamos por todos los niveles posibles.
    for(int i = nivel_punteros; i > 0; i--){
        if(ptr == 0){
            if(reservar == 0){
                // Imprimimos el error por la salida estÃ¡ndar de errores.
                //fprintf(stderr, "Error: lectura bloque inexistente.\n");
                return -1; 
            }
            else {
                salvar_inodo = 1;
                ptr = reservar_bloque();
                if(ptr == -1){
                    // Imprimimos el error por la salida estÃ¡ndar de errores.
                    fprintf(stderr, "Error: No se ha podido reservar el bloque..\n");
                    return -1;    
                }
                inodo.numBloquesOcupados++;
                inodo.ctime = time(NULL);
                if(nivel_punteros == nRangoBL){
                    inodo.punterosIndirectos[nRangoBL-1] = ptr;
                   // printf("[traducir_bloque_inodo()â inodo.punterosIndirectos[%d] = %d (reservado BF %d para punteros_nivel%d)]\n", nRangoBL-1, ptr, ptr, nivel_punteros);
                }
                else {
                    buffer[indice] = ptr; // (imprimirlo)
                  //  printf("[traducir_bloque_inodo()â punteros_nivel%d [%d] = %d (reservado BF %d para punteros_nivel%d)]\n",nivel_punteros+1, indice, ptr, ptr, nivel_punteros );
                    r = bwrite(ptr_ant, buffer);
                    if(r == -1){
                         // Imprimimos el error por la salida estÃ¡ndar de errores.
                        fprintf(stderr, "Error: No se ha podido escrbir el bloque.\n");
                        return -1; 
                    }
                }
            }
        }
        r = bread(ptr, buffer);
        if(r == -1){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            fprintf(stderr, "Error: No se ha podido leer el bloque.\n");
            return -1; 
        }
        indice = obtener_indice(nblogico, nivel_punteros);
        if(indice == -1){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            fprintf(stderr, "Error: Ha habido algÃºn problema al obetener el indice.\n");
            return -1; 
        }
        ptr_ant = ptr; //guardamos el puntero
        ptr = buffer[indice]; // y lo desplazamos al siguiente nivel
        nivel_punteros--;
    }
    
    if(ptr == 0){
        if(reservar == 0){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            //fprintf(stderr, "Error: lectura bloque inexistente.\n");
            return -1; 
        }
        else {
            salvar_inodo = 1;
            ptr = reservar_bloque();
            if(ptr == -1){
                // Imprimimos el error por la salida estÃ¡ndar de errores.
                fprintf(stderr, "Error: No se ha podido reservar el bloque..\n");
                return -1;    
            }
            inodo.numBloquesOcupados++;
            inodo.ctime = time(NULL);
            if(nRangoBL == 0){
                inodo.punterosDirectos[nblogico] = ptr;
                //printf("[traducir_bloque_inodo()â inodo.punterosDirectos[%d] = %d (reservado BF %d para BL %d)]\n", nblogico, ptr, ptr, nblogico);
            }
            else {
                buffer[indice] = ptr;
                r = bwrite(ptr_ant, buffer);
                if(r == -1){
                    // Imprimimos el error por la salida estÃ¡ndar de errores.
                    fprintf(stderr, "Error: No se ha podido escrbir el bloque.\n");
                    return -1; 
                }
              // printf("[traducir_bloque_inodo()â punteros_nivel%d [%d] = %d (reservado BF %d para BL %d)]\n", nivel_punteros+1, indice, ptr , ptr, nblogico);
            }
        }
    }
    if(salvar_inodo == 1){
        r = escribir_inodo(ninodo, inodo);
        if(r == -1){
            // Imprimimos el error por la salida estÃ¡ndar de errores.
            fprintf(stderr, "Error: No se ha podido escribir el inodo.\n");
            return -1; 
        }
    }
    // Devolvemos el Ã­ndice.
    return ptr;
}

/******************************************************************************
* MÃ©todo: liberar_inodo()
* DescripciÃ³n:  Libera el inodo especificado por parÃ¡metro.
* ParÃ¡metros:   ninodo : NÃºmero de inodo a liberar.
* Devuelve:     NÃºmero de inodo liberado.
*               -1 si ha habido algÃºn error.
******************************************************************************/
int liberar_inodo(unsigned int ninodo){
    struct inodo inodo;                                 // Estructura Inodo
    int liberados = liberar_bloques_inodo(ninodo, 0);   // Liberamos inodo.
    int r = leer_inodo(ninodo, &inodo);                 // Leemos inodo.
    // Si ha habido algÃºn error.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error (liberar_inodo()): No se ha podido leer el inodo.\n");
        return -1; 
    }
    inodo.numBloquesOcupados =- liberados;    // Actualizamos num bloques ocupados.
    inodo.tipo = 'l';                         // Tipo libre.
    struct superbloque SB;                    // Estructura superbloque.
    r = bread(posSB, &SB);                        // Leemos superbloque
    // Si ha habido algÃºn error.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error (liberar_inodo()): No se ha podido leer el Superbloque.\n");
        return -1; 
    }
    inodo.punterosDirectos[0] = SB.posPrimerInodoLibre; //Inodo apunta al Ãºltimo libre.  
    SB.posPrimerInodoLibre = ninodo;                    //SB apunta al inodo.
    SB.cantInodosLibres++;                              //Aumentamos num inodos libres.
    r = bwrite(posSB, &SB);                             // Escribimos SB en disco
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error (liberar_inodo()): No se ha podido escribir el Superbloque.\n");
        return -1; 
    }
    r = escribir_inodo(ninodo, inodo);
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error (liberar_inodo()): No se ha podido escribir el inodo.\n");
        return -1; 
    }
    // Devolvemos el nÃºmero de inodo liberado.
    return ninodo;
}

/******************************************************************************
* MÃ©todo: liberar_bloques_inodo()
* DescripciÃ³n:  Libera todos los bloques ocupados a partir del bloque lÃ³gico 
*               indicado por el argumento nblogico (inclusive)
* ParÃ¡metros:   ninodo   : NÃºmero de inodo afectado.
*               nblogico : NÃºmero de bloque lÃ³gico al partir del cual se 
*                          comenzarÃ¡ a liberar.
* Devuelve:     Cantidad de bloques liberados.
******************************************************************************/
int liberar_bloques_inodo(unsigned int ninodo, unsigned int nblogico){
    struct inodo inodo;                             // Estructura inodo.
    int r = leer_inodo(ninodo, &inodo);             // Leemos inodo.
    if(r == -1){
        // Imprimimos el error por la salida estÃ¡ndar de errores.
        fprintf(stderr, "Error (liberar_bloques_inodo()): No se ha podido leer el inodo.\n");
        return -1; 
    }
    int nblogico_ultimo;                            // Ultimo num bloque lÃ³gico.
    int nbloquesLiberados = 0;                      // Num bloques liberados.
    int nRangoBL;                                   // Rango de Bloque lÃ³gico
    int nivel_punteros;                             // Nivel de punteros indirectos
    unsigned int bloques_punteros[3][NPUNTEROS];    // Array auxiliar bloques punteros
    unsigned char bufferAux[BLOCKSIZE];             // Buffer auxiliar blocksize
    int indice;                                     // Indice 
    int ptr_nivel[3];                               // Puntero nivel
    int indices[3];                                 // indices
    int nblog;                                      // num bloque logico 
    int salvar_inodo;                               // salvar inodo.
    unsigned int ptr = 0;                           // Puntero
    // Si el fichero no tiene bloques asignados, estÃ¡ vacÃ­o y ya hemos terminado.
    if(inodo.tamEnBytesLog == 0){
        return 0; // fichero vacÃ­o.
    }
    memset(bufferAux,0,BLOCKSIZE);                  // Asignamos valor a bufferAux
    // Obtener Ãºltino nÃºmero de bloque lÃ³gico ocupado.
    if((inodo.tamEnBytesLog % BLOCKSIZE) == 0){
        nblogico_ultimo = inodo.tamEnBytesLog / BLOCKSIZE -1;}
    else {
        nblogico_ultimo = inodo.tamEnBytesLog / BLOCKSIZE;}
    //nblogico_ultimo = 16843019;
    //printf("[liberar_bloques_inodo()â primerBL: %d ultimoBL %d]\n", nblogico, nblogico_ultimo);
    // Recorremos los bloques lÃ³gicos para liberarlos.
    for(nblog = nblogico; nblog <= nblogico_ultimo; nblog++){
        nRangoBL = obtener_nrangoBL(inodo, nblog, &ptr);
        if(nRangoBL < 0){
            fprintf(stderr, "Error (liberar_bloques_inodo()): No se ha podido obtener el rangoBL.\n");
            return -1;
        }
        nivel_punteros = nRangoBL;
        while(ptr > 0 && nivel_punteros > 0){        
            r = bread(ptr, bloques_punteros[nivel_punteros-1]);
            if(r == -1){
                // Imprimimos el error por la salida estÃ¡ndar de errores.
                fprintf(stderr, "Error (liberar_bloques_inodo()): No se ha podido leer blocksize.\n");
                return -1; 
            }
            indice = obtener_indice(nblog, nivel_punteros);
            if(r == -1){
                // Imprimimos el error por la salida estÃ¡ndar de errores.
                fprintf(stderr, "Error (liberar_bloques_inodo()): No se ha podido obtener indice.\n");
                return -1; 
            }
            ptr_nivel[nivel_punteros-1] = ptr;
            indices[nivel_punteros-1] = indice;
            ptr = bloques_punteros[nivel_punteros-1][indice];
            nivel_punteros--;    
        }
        if(ptr > 0){
            int bloque_liberado = liberar_bloque(ptr);
            if(bloque_liberado == -1){
                // Imprimimos el error por la salida estÃ¡ndar de errores.
                fprintf(stderr, "Error (liberar_bloques_inodo()): No se ha podido liberar el bloque.\n");
                return -1; 
            }
            nbloquesLiberados++;
           // printf("[liberar_bloques_inodo()â liberado BF %d  de datos correspondiente al BL %d]\n", bloque_liberado,nblog);
            if(nRangoBL == 0){
                inodo.punterosDirectos[nblog] = 0;
                salvar_inodo = 1;
            }
            else {
                while(nivel_punteros < nRangoBL){
                    indice = indices[nivel_punteros];
                    bloques_punteros[nivel_punteros][indice] = 0;
                    ptr = ptr_nivel[nivel_punteros];
                    if(memcmp(bloques_punteros[nivel_punteros],bufferAux,BLOCKSIZE)==0){
                        r = liberar_bloque(ptr);
                        if(r == -1){
                            // Imprimimos el error por la salida estÃ¡ndar de errores.
                            fprintf(stderr, "Error (liberar_bloques_inodo()): No se ha podido liberar el bloque.\n");
                            return -1; 
                        }
                        nbloquesLiberados++;
                       // printf("[liberar_bloques_inodo()â liberado BF %d  de punteros de nivel %d correspondiente al BL %d]\n", ptr, nivel_punteros+1, nblog);
                        nivel_punteros++;
                        if(nivel_punteros == nRangoBL){
                            inodo.punterosIndirectos[nRangoBL-1] = 0;
                            salvar_inodo = 1;
                        }
                    }
                    else{
                        r = bwrite(ptr,bloques_punteros[nivel_punteros]);
                        if(r == -1){
                            // Imprimimos el error por la salida estÃ¡ndar de errores.
                            fprintf(stderr, "Error (liberar_bloques_inodo()): No se ha podido escribir bloques_punteros.\n");
                            return -1; 
                        }
                        nivel_punteros = nRangoBL;
                    }
                }
            }
        }

    }
    if(salvar_inodo == 1){
        escribir_inodo(ninodo, inodo);
    }
    // Devolvemos nÃºmero de bloques liberados.
   // printf("[liberar_bloques_inodo()â total bloques liberados: %d]\n", nbloquesLiberados);
    return nbloquesLiberados;
}