/******************************************************************************
*                          FICHEROS_BASICOS.H 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÃCTOR MARIO MEDINA CABANELAS
* FECHA: 22 DE MARZO DE 2019.
******************************************************************************/

/* HAY QUE PONER ESTO BONITO */

#include <stdio.h>  //printf(), fprintf(), stderr, stdout, stdin
#include <fcntl.h> //O_WRONLY, O_CREAT, O_TRUNC
#include <sys/stat.h> //S_IRUSR, S_IWUSR
#include <stdlib.h>  //exit(), EXIT_SUCCESS, EXIT_FAILURE, atoi()
#include <unistd.h> // SEEK_SET, read(), write(), open(), close(), lseek()
#include <errno.h>  //errno
#include <string.h> // strerror()
#include <limits.h>  // UINT_MAX
#include <time.h>   // time

#include "bloques.h"

#define  INODOSIZE 128              // TamaÃ±o de los inodos 128 bytes.

#define  posSB  0
#define  tamSB  1

#define NPUNTEROS (BLOCKSIZE/sizeof(unsigned int)) //256
#define DIRECTOS 12   
#define INDIRECTOS0 (NPUNTEROS + DIRECTOS)	//268   
#define INDIRECTOS1 (NPUNTEROS * NPUNTEROS + INDIRECTOS0)   //65.804   
#define INDIRECTOS2 (NPUNTEROS * NPUNTEROS * NPUNTEROS + INDIRECTOS1) //16.843.020 

struct superbloque {
   unsigned int posPrimerBloqueMB;                       // PosiciÃ³n del primer bloque del mapa de bits en el SF
   unsigned int posUltimoBloqueMB;                       // PosiciÃ³n del Ãºltimo bloque del mapa de bits en el SF
   unsigned int posPrimerBloqueAI;                       // PosiciÃ³n del primer bloque del array de inodos en el SF
   unsigned int posUltimoBloqueAI;                       // PosiciÃ³n del Ãºltimo bloque del array de inodos en el SF
   unsigned int posPrimerBloqueDatos;                    // PosiciÃ³n del primer bloque de datos en el SF
   unsigned int posUltimoBloqueDatos;                    // PosiciÃ³n del Ãºltimo bloque de datos en el SF
   unsigned int posInodoRaiz;                            // PosiciÃ³n del inodo del directorio raÃ­z en el AI 
   unsigned int posPrimerInodoLibre;                     // PosiciÃ³n del primer inodo libre en el AI
   unsigned int cantBloquesLibres;                       // Cantidad de bloques libres del SF
   unsigned int cantInodosLibres;                        // Cantidad de inodos libres del SF
   unsigned int totBloques;                              // Cantidad total de bloques del SF
   unsigned int totInodos;                               // Cantidad total de inodos del SF
   char padding[BLOCKSIZE - 12 * sizeof(unsigned int)];  // Relleno para que ocupe 1 bloque
};



struct inodo {     // comprobar que ocupa 128 bytes haciendo un sizeof(inodo)!!!
   char tipo;     // Tipo ('l':libre, 'd':directorio o 'f':fichero)
   char permisos; // Permisos (lectura y/o escritura y/o ejecuciÃ³n)
   /* Por cuestiones internas de alineaciÃ³n de estructuras, si se estÃ¡ utilizando
    un tamaÃ±o de palabra de 4 bytes (microprocesadores de 32 bits):
   unsigned char reservado_alineacion1 [2];
   en caso de que la palabra utilizada sea del tamaÃ±o de 8 bytes
   (microprocesadores de 64 bits): unsigned char reservado_alineacion1 [6]; */
   char reservado_alineacion1[6];
   time_t atime; // Fecha y hora del Ãºltimo acceso a datos: atime
   time_t mtime; // Fecha y hora de la Ãºltima modificaciÃ³n de datos: mtime
   time_t ctime; // Fecha y hora de la Ãºltima modificaciÃ³n del inodo: ctime

   /* comprobar el tamaÃ±o del tipo time_t para vuestra plataforma/compilador:
   printf ("sizeof time_t is: %d\n", sizeof(time_t)); */

   unsigned int nlinks;             // Cantidad de enlaces de entradas en directorio
   unsigned int tamEnBytesLog;      // TamaÃ±o en bytes lÃ³gicos. Se actualizarÃ¡ al escribir si crece
   unsigned int numBloquesOcupados; // Cantidad de bloques ocupados zona de datos

   unsigned int punterosDirectos[12];  // 12 punteros a bloques directos
   unsigned int punterosIndirectos[3]; /* 3 punteros a bloques indirectos:
   1 indirecto simple, 1 indirecto doble, 1 indirecto triple */

   /* Utilizar una variable de alineaciÃ³n si es necesario para vuestra plataforma/compilador */
   char
       padding[INODOSIZE - 2 * sizeof(unsigned char) - 3 * sizeof(time_t) - 18 * sizeof(unsigned int) - 6 * sizeof(unsigned char)];
   // Hay que restar tambiÃ©n lo que ocupen las variables de alineaciÃ³n utilizadas!!!
};

/******************************************************************************
* MÃ©todo: tamMb()
* DescripciÃ³n:  Calcula el nÃºmero de bloques necesarios para alojar el 
*               mapa de bits.
* ParÃ¡metros:   nbloques : numero de bloques del disco.
* Devuelve:     tamMB    : nÃºmero de bloques del MB.
******************************************************************************/
int tamMB(unsigned int nbloques);

/******************************************************************************
* MÃ©todo: tamAI()
* DescripciÃ³n:  Calcula el numero de bloques necesarios para alojar el Array 
*               de inodos. 
* ParÃ¡metros:   ninodos : NÃºmero de inodos del sistema.
* Devuelve:     tamAI   : NÃºmero de bloques para alojar el Array de inodos. 
******************************************************************************/
int tamAI(unsigned int ninodos);

/******************************************************************************
* MÃ©todo: initSB()
* DescripciÃ³n:  Inicializa el superbloque con los datos apropiados.
* ParÃ¡metros:   nbloques : NÃºmero de bloques del disco.
*               inodos   : NÃºmero de inodos del disco.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initSB(unsigned int nbloques, unsigned int ninodos);

/******************************************************************************
* MÃ©todo: initMB()
* DescripciÃ³n:  Inicializa los valores del Mapa de Bits a 0. 
* ParÃ¡metros:   Ninguno.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initMB();

/******************************************************************************
* MÃ©todo: initAI()
* DescripciÃ³n:  Inicializa el Array de Inodos. Al principio todos los inodos 
*               estÃ¡n libres y por tanto enlazados.
* ParÃ¡metros:   Ninguno.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initAI();

/******************************************************************************
* MÃ©todo: escribir_bit()
* DescripciÃ³n:  Escribe el valor indicado por elparÃ¡metro bit (0 Ã³ 1) en un 
*               determinado bit del MP que representa el bloque nbloque.
* ParÃ¡metros:   unsigned int nbloque :  NÃºmero de bloque.  
*               unsigned int bit     :  Bit a escribir.
* Devuelve:     0   : Ha funcionado correctamente.
*              -1   : Ha habido algÃºn problema.
******************************************************************************/
int escribir_bit(unsigned int nbloque, unsigned int bit);

/******************************************************************************
* MÃ©todo: leer_bit()
* DescripciÃ³n:  Lee un determinado bit del MB y devuelve el valor del bit 
*               leÃ­do.
* ParÃ¡metros:   nbloque  : NÃºmero de bloque que queremos leer.
* Devuelve:     El valor del bit leÃ­do.
*               -1 si ha habido algun problema.
******************************************************************************/
unsigned char leer_bit(unsigned int nbloque);

/******************************************************************************
* MÃ©todo: reservar_bloque()
* DescripciÃ³n:  Encuentra el primer bloque libre, consultando el MB, lo ocupa 
*               y devuelve su posiciÃ³n.
* ParÃ¡metros:   Ninguno
* Devuelve:     PosiciÃ³n del primer bloque libre.
*              -1 : si ha habido algÃºn problema.
******************************************************************************/
int reservar_bloque();

/******************************************************************************
* MÃ©todo: liberar_bloque()
* DescripciÃ³n:  Libera un bloque determinado.
* ParÃ¡metros:   nbloque : NÃºmero del bloque de se desea liberar.
* Devuelve:     nbloque : Si ha funcionado correctamente.
*              -1       : Si ha habido algÃºn error.
******************************************************************************/
int liberar_bloque(unsigned int nbloque);

/******************************************************************************
* MÃ©todo: escribir_inodo()
* DescripciÃ³n:  Escribe el contenido de una variable de tipo struct inodo en 
*               un determinado inodo del array de inodos.
* ParÃ¡metros:   ninodo  : NÃºmero del inodo donde se quiere escribir.
*               inodo   : Inodo a escribir.
* Devuelve:     0  : Si ha funcionado correctamente.
*              -1  : Si ha habido algÃºn error.
******************************************************************************/
int escribir_inodo(unsigned int ninodo, struct inodo inodo);

/******************************************************************************
* MÃ©todo: leer_inodo()
* DescripciÃ³n:  Lee un determinado inodo del array de inodos para volcarlo en 
*               una variable tipo struct inodo pasada por referencia.
* ParÃ¡metros:   ninodo : NÃºmero de inodo que se quiere leer.
*               *inodo : Puntero a un inodo donde se guardarÃ¡n los datos leÃ­dos
* Devuelve:     0  : Si ha funcionado correctamente.
*              -1  : Si ha habido algÃºn error.
******************************************************************************/
int leer_inodo(unsigned int ninodo, struct inodo *inodo);

/******************************************************************************
* MÃ©todo: reservar_inodo()
* DescripciÃ³n:  Encuentra el primer inodo libre, lo reserva, devuelve su 
*               nÃºmero y actualiza la lista enlazada de inodos libres.
* ParÃ¡metros:   tipo     : (directorio | fichero | libre)
*               permisos : Permisos que se le atribuyen (0-7)
* Devuelve:     NÃºmero de inodo reservado.
*               -1 : Si ha habido algÃºn problema.
******************************************************************************/
int reservar_inodo(unsigned char tipo, unsigned char permisos);

/******************************************************************************
* MÃ©todo: obtener_nrangoBL()
* DescripciÃ³n: Obtiene el rando de punteros en el que se sitÃºa el bloque lÃ³gico 
*              que buscamos y la direcciÃ³n almaenada en el puntero 
*              correspondiente del inodo.
* ParÃ¡metros:  inodo    : inodo en el cual buscaremos en el bloque.
*              nblogico : nÃºmero de bloque lÃ³gico.
*              *ptr     : puntero hacia atributo inodo correspondiente.
* Devuelve:    0    : Si pertenece a bloque directo.
*              1    : Si pertenece a bloque indirecto 1
*              2    : Si pertenece a bloque indirecto 2
*              3    : Si pertenece a bloque indirecto 3
*             -1    : Si ha habido algÃºn error.
******************************************************************************/
int obtener_nrangoBL(struct inodo inodo, unsigned int nblogico, unsigned int  *ptr);

/******************************************************************************
* MÃ©todo: obtener_indice()
* DescripciÃ³n: A partir de un numero de bloque lÃ³gico y un nivel de punteros, 
*              obtiene el el Ã­ndice que almacena el bloque fÃ­sico de los datos. 
* ParÃ¡metros:  nblogico       : nÃºmero de bloque lÃ³gico.
*              nivel_punteros : Determina quÃ© nivel hemos solicitado.
* Devuelve:    Ã¯ndice solicitado
*              -1 si ha habido algÃºn error. 
******************************************************************************/
int obtener_indice(int nblogico, int nivel_punteros);

/******************************************************************************
* MÃ©todo: traducir_bloque_inodo()
* DescripciÃ³n:  Se encarga de obtener el nÃºmero de bloque fÃ­sico correspondiente 
*               a un bloque lÃ³gico determinado del inodo indicado.
* ParÃ¡metros:   ninodo    : NÃºmero de inodo que queremos traducir.
*               nblogico  : NÃºmero de bloque lÃ³gico.
*               reservvar : 0 (consulta) | 1 (escritura)
* Devuelve:     NÃºmero de bloque fÃ­sico.
******************************************************************************/
int traducir_bloque_inodo(unsigned int ninodo, unsigned int nblogico, char reservar);

/******************************************************************************
* MÃ©todo: liberar_inodo()
* DescripciÃ³n:  Libera el inodo especificado por parÃ¡metro.
* ParÃ¡metros:   ninodo : NÃºmero de inodo a liberar.
* Devuelve:     NÃºmero de inodo liberado.
*               -1 si ha habido algÃºn error.
******************************************************************************/
int liberar_inodo(unsigned int ninodo);

/******************************************************************************
* MÃ©todo: liberar_bloques_inodo()
* DescripciÃ³n:  Libera todos los bloques ocupados a partir del bloque lÃ³gico 
*               indicado por el argumento nblogico (inclusive)
* ParÃ¡metros:   ninodo   : NÃºmero de inodo afectado.
*               nblogico : NÃºmero de bloque lÃ³gico al partir del cual se 
*                          comenzarÃ¡ a liberar.
* Devuelve:     Cantidad de bloques liberados.
******************************************************************************/
int liberar_bloques_inodo(unsigned int ninodo, unsigned int nblogico);