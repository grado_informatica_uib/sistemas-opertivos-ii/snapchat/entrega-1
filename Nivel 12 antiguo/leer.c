
/******************************************************************************
*                                 LEER.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERACTIVOS II - GRADO ING. INFORMATICA - UIB
*
* DESCRIPCIÃN: LE PASAREMOS POR LÃNEA DE COMANDOS UN NÃMERO DE INODO OBTENIDO 
*              CON EL PROGRAMA ANTERIOR (ADEMÃS DEL NOMBRE DEL DISPOSITIVO). 
*   
*              SU FUNCIONAMIENTO TIENE QUE SER SIMILAR A LA FUNCIÃN 'CAT' DE 
*              LÃNUX, EXPLORANDO TODO EL FICHERO.
*
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÃCTOR MARIO MEDINA CABANELAS
*
* FECHA: 03 DE ABRIL DE 2019.
******************************************************************************/

#include<stdio.h>
#include "ficheros.h"

#define BLOCKSIZE_READ 1500

int main(int argc, char **argv){
    /**************************************************************************
     *                 COMPROBAMOS SI LA SINTAXIS ES CORRECTA
     *************************************************************************/
    if(argc != 3){
        printf("Sintaxis: leer <nombre_dispositivo> <numero de inodo>\n");
        exit(EXIT_FAILURE);
    }
    /**************************************************************************
     *                    MONTAMOS EL DISPOSITIVO VIRTUAL
     *************************************************************************/
    int r = bmount(argv[1]);
    if(r == -1){
        fprintf(stderr,"Error al montar el disco.\n");
        exit(EXIT_FAILURE);
    }
    // creamos un struct stat...
    struct STAT stat;
    // inforamciÃ³n del inodo leido
    if(mi_stat_f(atoi(argv[2]), &stat) == -1){
        fprintf(stderr,"Error\n");
        exit(EXIT_FAILURE);
    }
    /**************************************************************************
     *                          COMPROBAR PERMISOS
     *************************************************************************/
    if((stat.permisos & 4) != 4){
        fprintf(stderr, "Error: Permiso denegado de lectura\n");
        exit(EXIT_FAILURE);
    }
    /**************************************************************************
     *                   CREAMOS VARIABLES PARA LA LECTURA
     *************************************************************************/
    // Creamos un buffer
    unsigned char buffer[BLOCKSIZE_READ];
    // Creamos una variable offset inicializada a 0
    int offset = 0;
    // Creamos una variable de bytes leidos inicializada a 0
    int totalBytesLeidos = 0;
    // Inicializamos el buffer a 0
    memset(buffer, 0, BLOCKSIZE_READ);
    // Leemos
    int bytesLeidos = mi_read_f(atoi(argv[2]), buffer, offset, BLOCKSIZE_READ);
    if(bytesLeidos == -1){  // Comprobamos error de lectura
        fprintf(stderr,"Error de lectura.\n");
        exit(EXIT_FAILURE);
    }
    /**************************************************************************
     *                         BUCLE DE LECTURA
     *************************************************************************/
    while(bytesLeidos > 0){
        // Escribimos el buffer a 1
        if(write(1, buffer, bytesLeidos) == -1){
            fprintf(stderr,"Error de escritura\n");
            exit(EXIT_FAILURE);
        }
        // Colocamos el buffer a 0
        memset(buffer, 0, BLOCKSIZE_READ);
        // Avanzamos un bloque
        offset += BLOCKSIZE_READ; 
        // Aumentamos el nÃºmero de bytes leidos
        totalBytesLeidos += bytesLeidos;
        // Realizamos la lectura y comprobamos error
        bytesLeidos = mi_read_f(atoi(argv[2]), buffer, offset, BLOCKSIZE_READ);
        if(bytesLeidos == -1){
            fprintf(stderr,"Error de lectura\n");
            exit(EXIT_FAILURE);
        }
    }
    // Imprimimos por pantalla el total leido
    fprintf(stderr,"\ntotal_leido: %d\n", totalBytesLeidos );
    // ... donde guardamos la inforamciÃ³n del inodo leido
    if(mi_stat_f(atoi(argv[2]), &stat) == -1){
        fprintf(stderr,"Error\n");
        exit(EXIT_FAILURE);
    }
    // Imprimimos
    fprintf(stderr,"tamEnBytesLog: %d\n", stat.tamEnBytesLog);
    /**************************************************************************
     *                 DESMONTAMOS EL DISPOSITIVO VIRTUAL
     *************************************************************************/
    if(bumount() == -1){
        fprintf(stderr,"Error al montar el disco.\n");
        exit(EXIT_FAILURE);
    }
    // Finalizamos
    exit(EXIT_SUCCESS);
}