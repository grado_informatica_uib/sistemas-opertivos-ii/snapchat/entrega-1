/******************************************************************************
*                                LEER_SF.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERACTIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÃN:
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÃCTOR MARIO MEDINA CABANELAS
* FECHA: 22 DE MARZO DE 2019.
******************************************************************************/
#include "ficheros_basico.h"
int main (int argc, char *argv[]){
    if (argc != 2) {
      fprintf(stderr, "Error: Arg.1: Nombre fichero.\n");
      exit(EXIT_FAILURE);
    } 
    /**************************************************************************
     *                          MONTAMOS EL DISCO
     *************************************************************************/
    bmount(argv[1]);

    /**************************************************************************
     *                     MOSTRAMOS INFORMACIÃN DEL SB
     *************************************************************************/ 
    struct superbloque SB;

    if (bread(posSB, &SB) == -1) {
      perror("Error: Lectura superbloque incorrecta.\n");
      bumount();
      exit(EXIT_FAILURE);
    } 

    printf("DATOS DEL SUPERBLOQUE:\n");
    printf("posPrimerBloqueMB = %d\n", SB.posPrimerBloqueMB);
    printf("posUltimoBloqueMB = %d\n", SB.posUltimoBloqueMB);
    printf("posPrimerBloqueAI = %d\n", SB.posPrimerBloqueAI);
    printf("posUltimoBloqueAI = %d\n", SB.posUltimoBloqueAI);
    printf("posPrimerBloqueDatos = %d\n", SB.posPrimerBloqueDatos);
    printf("posUltimoBloqueDatos = %d\n", SB.posUltimoBloqueDatos);
    printf("posInodoRaiz = %d\n", SB.posInodoRaiz);
    printf("posPrimerInodoLibre = %d\n", SB.posPrimerInodoLibre);
    printf("cantBloquesLibres = %d\n", SB.cantBloquesLibres);
    printf("canInodosLibres = %d\n", SB.cantInodosLibres);
    printf("totBloques = %d\n", SB.totBloques);
    printf("totInodos = %d\n\n", SB.totInodos);

    /**************************************************************************
     *                         DESMONTAMOS DISCO
     *************************************************************************/
    if (bumount() == -1) exit(EXIT_FAILURE);
    exit(EXIT_SUCCESS);
}