/******************************************************************************
*                               MI_CAT.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÃN: FUNCIONES BÃSICA DE ENTRADA Y SALIDA DE BLOQUES.
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÃCTOR MARIO MEDINA CABANELAS
* FECHA: 14 DE MAYO DE 2019.
******************************************************************************/
#include "directorios.h"

#define TAM_BUFFER 4096


int main(int argc, char *argv[]){
    // Comprobamos que el numero de argumentos es el correcto
      int bytesLeidos, offset, totalBytesLeidos;

      unsigned char buffer[TAM_BUFFER];
    if (argc != 3) {
      fprintf(stderr, "Sintaxis: : ./mi_cat <disco> </ruta_fichero>.\n");
      exit(EXIT_FAILURE);
    } 
    if (argv[2][strlen(argv[2])-1] == '/') {
  	    perror("Error: La ruta no es un fichero.\n");
  	    exit(EXIT_FAILURE);
    }
    if (bmount(argv[1]) == -1) {
        fprintf(stderr, "Error: error de apertura de fichero.\n");
        exit(-1);
    }

    offset = 0;
    totalBytesLeidos = 0;
    memset(buffer, 0, TAM_BUFFER);
    bytesLeidos = mi_read(argv[2], buffer, offset, TAM_BUFFER);
      

    while (bytesLeidos > 0) {
        write(1, buffer, bytesLeidos);
        memset(buffer, 0, TAM_BUFFER);
        offset += TAM_BUFFER; //avanzamos un bloque
        totalBytesLeidos += bytesLeidos;
        bytesLeidos = mi_read(argv[2], buffer, offset, TAM_BUFFER);
    }

    printf("\nBytes leidos: %i\n", totalBytesLeidos);


    if (bumount() == -1) {
        fprintf(stderr, "Error: error al cerrar el fichero.\n");
        exit(EXIT_FAILURE);
    }
    return 0;
}