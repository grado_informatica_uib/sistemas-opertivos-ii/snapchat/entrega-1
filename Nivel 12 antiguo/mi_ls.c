/******************************************************************************
*                               MI_LS.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÃN: FUNCIONES BÃSICA DE ENTRADA Y SALIDA DE BLOQUES.
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÃCTOR MARIO MEDINA CABANELAS
* FECHA: 14 DE MAYO DE 2019.
******************************************************************************/
#include "directorios.h"

int main(int argc, char *argv[]){
    // Comprobamos que el numero de argumentos es el correcto
    if (argc != 3) {
      fprintf(stderr, "Sintaxis: : ./mi_ls <disco> </ruta_directorio>.\n");
      exit(EXIT_FAILURE);
    } 
    // Comprobamos que el rango de permisos es el adecuado
    //if(atoi(argv[2]) < 0 || atoi(argv[2]) > 7){
    //    fprintf(stderr, "Error: permisos debe estar en el rango 0-7");
    //    exit(EXIT_FAILURE);
    //}
    // Montamos eldisco
    if (bmount(argv[1]) == -1) {
        fprintf(stderr, "Error: error de apertura de fichero.\n");
        exit(-1);
    }
    char buffer[1000];
    int r = mi_dir(argv[2], buffer);
    if(r >= 0){
    printf("%s", buffer);
    }

    if (bumount() == -1) {
        fprintf(stderr, "Error: error al cerrar el fichero.\n");
        exit(EXIT_FAILURE);
    }
    return 0;
}