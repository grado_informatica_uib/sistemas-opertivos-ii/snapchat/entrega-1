/******************************************************************************
*                               MI_MKDIR.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÃN: FUNCIONES BÃSICA DE ENTRADA Y SALIDA DE BLOQUES.
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÃCTOR MARIO MEDINA CABANELAS
* FECHA: 14 DE MAYO DE 2019.
******************************************************************************/
#include "directorios.h"

int main(int argc, char *argv[]){
    // Comprobamos que el numero de argumentos es el correcto
    if (argc != 4) {
      fprintf(stderr, "Sintaxis: mi_mkdir <nombre_dispositivo> <permisos> </ruta>.\n");
      exit(EXIT_FAILURE);
    } 
    // Comprobamos que el rango de permisos es el adecuado
    if(atoi(argv[2]) < 0 || atoi(argv[2]) > 7){
        fprintf(stderr, "Error:  modo invÃ¡lido: <<%s>>\n", argv[2]);
        exit(EXIT_FAILURE);
    }
    // Montamos eldisco
    if (bmount(argv[1]) == -1) {
        fprintf(stderr, "Error: error de apertura de fichero.\n");
        exit(-1);
    }
    
    mi_creat(argv[3], atoi(argv[2]));
    if (bumount() == -1) {
        fprintf(stderr, "Error: error al cerrar el fichero.\n");
        exit(EXIT_FAILURE);
    }
    return 0;
}