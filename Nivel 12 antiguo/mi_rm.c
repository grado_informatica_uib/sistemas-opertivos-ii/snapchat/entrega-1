/******************************************************************************
*                               MI_RM.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÃN: FUNCIONES BÃSICA DE ENTRADA Y SALIDA DE BLOQUES.
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÃCTOR MARIO MEDINA CABANELAS
* FECHA: 14 DE MAYO DE 2019.
******************************************************************************/
#include "directorios.h"

int main(int argc, char *argv[]){
    // Comprobamos que el numero de argumentos es el correcto
    if (argc != 3) {
      fprintf(stderr, "Sintaxis: ./mi_rm disco /ruta.\n");
      exit(EXIT_FAILURE);
    } 
    // Montamos eldisco
    if (bmount(argv[1]) == -1) {
        fprintf(stderr, "Error: error de apertura de fichero.\n");
        exit(-1);
    }

    mi_unlink(argv[2]);

    if (bumount() == -1) {
        fprintf(stderr, "Error: error al cerrar el fichero.\n");
        exit(EXIT_FAILURE);
    }
    return 0;
}