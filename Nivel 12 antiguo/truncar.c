/******************************************************************************
*                                TRUNCAR.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERACTIVOS II - GRADO ING. INFORMATICA - UIB
*
* DESCRIPCIÃN: LIBERA UN INODO A PARTIR DE UN NUMERO DE BYTE DADO POR PARÃMETRO
*
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÃCTOR MARIO MEDINA CABANELAS
*
* FECHA: 04 DE ABRIL DE 2019.
******************************************************************************/

#include<stdio.h>
#include "ficheros.h"

int main(int argc, char **argv){
    // Inicializamos una variable que guardara la cantidad de bloques liberados
    int bloquesLiberados = 0;   // inicializada a 0
    /**************************************************************************
     *                 COMPROBAMOS SI LA SINTAXIS ES CORRECTA
     *************************************************************************/
    if(argc != 4){
        printf("Sintaxis: truncar <nombre_dispositivo> <ninodo> <nbytes>\n");
        exit(EXIT_FAILURE);
    }
    /**************************************************************************
     *                    MONTAMOS EL DISPOSITIVO VIRTUAL
     *************************************************************************/
    int r = bmount(argv[1]);
    if(r == -1){    // comprobamos error
        fprintf(stderr,"Error al montar el disco.\n");
        exit(EXIT_FAILURE);
    }
    /**************************************************************************
     *                    TRUNCAMOS
     *************************************************************************/
    // Traducimos los parÃ¡metros de usuario a variables de usuario
    int ninodo = atoi(argv[2]);
    int nbytes = atoi(argv[3]);
    // Creamos un struct stat
    struct STAT stat;
    // Guardamos  en dicho struct el inodo indicado
    if(mi_stat_f(ninodo, &stat)){
        fprintf(stderr, "Error en funciÃ³n my_stat_f()\n");
        exit(EXIT_FAILURE);
    }
    // Si tenemos permiso de escritura, truncamos
    if((stat.permisos & 2) == 2){
        // Si el rango estÃ¡ dentro de lo que permite el fichero, truncamos
        if(stat.tamEnBytesLog > nbytes){
            bloquesLiberados = mi_truncar_f(ninodo, nbytes);
            if(bloquesLiberados == -1){ // comprobamos error al truncar
                fprintf(stderr, "Error en funciÃ³n my_stat_f()\n");
                exit(EXIT_FAILURE);
            }
            if(mi_stat_f(ninodo, &stat) == -1) {
                fprintf(stderr, "Error al \n");
                exit(EXIT_FAILURE);
            }
        } else {    // si no estÃ¡ dentro del rango, error
            fprintf(stderr, "No se puede truncar mÃ¡s alla del EOF: %d", nbytes);
        }
    } else {    // si no tenemos permiso, error
        fprintf(stderr,"Error: Permiso denegado de escritura\n");
    }
    // Guardamos en el struct stat el nodo indicado
    printf("\nDATOS INODO %d\n", ninodo);
    printf("tipo=%c\n", stat.tipo);
    printf("permisos=%d\n",stat.permisos);
    // Creamos un struct tm para todo lo referido al tiempo (<time.h>)
    struct tm *ts;
    char atime[80];
    char mtime[80];
    char ctime[80];
    // Asignamos el tiempo de creaciÃ³n, modificaciÃ³n y acceso.
    ts = localtime(&stat.atime);
    strftime(atime, sizeof(atime), "%a %Y-%m-%d %H:%M:%S", ts);
    ts = localtime(&stat.mtime);
    strftime(mtime, sizeof(mtime), "%a %Y-%m-%d %H:%M:%S", ts);
    ts = localtime(&stat.ctime);
    strftime(ctime, sizeof(ctime), "%a %Y-%m-%d %H:%M:%S", ts);
    // Imprimimos los tiempos por pantalla
    printf("atime: %s\n", atime);
    printf("mtime: %s\n", mtime);
    printf("ctime: %s\n", ctime);
    // Imprimimos el resto de datos
    printf("nlinks=%d\n", stat.nlinks);
    printf("tamEnBytesLog=%d\n", stat.tamEnBytesLog);
    printf("numBloquesOcupados=%d\n", stat.numBloquesOcupados);
    // Final
    exit(EXIT_SUCCESS);
}