/******************************************************************************
*                               MI_ESCRIBIR.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÓN: FUNCIONES BÁSICA DE ENTRADA Y SALIDA DE BLOQUES.
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 14 DE MAYO DE 2019.
******************************************************************************/
#include "directorios.h"

#define TAM_BUFFER 1024


int main(int argc, char *argv[]){
    // Comprobamos que el numero de argumentos es el correcto
     int longitud, offset, bytesEscritos;

    if (argc != 5) {
        printf("%d\n", argc);
      fprintf(stderr, "Sintaxis: ./mi_escribir <disco> </ruta_fichero> <texto> <offset>.\n");
      exit(EXIT_FAILURE);
    } 
    if (argv[2][strlen(argv[2])-1] == '/') {
  	    perror("Error: La ruta no es un fichero.\n");
  	    exit(EXIT_FAILURE);
    }
    if (bmount(argv[1]) == -1) {
        fprintf(stderr, "Error: error de apertura de fichero.\n");
        exit(-1);
    }

      offset = atoi(argv[4]);
  longitud = strlen(argv[3]);
  char buffer[longitud];
  strcpy(buffer, argv[3]);
          printf("longitud texto: %d\n", longitud);

if ((bytesEscritos = mi_write(argv[2], &buffer, offset, longitud)) == -1) {
        printf("Bytes escritos: 0\n");
    exit(EXIT_FAILURE);
  }
  else{
    printf("Bytes escritos: %d\n", bytesEscritos);

  }

    if (bumount() == -1) {
        fprintf(stderr, "Error: error al cerrar el fichero.\n");
        exit(EXIT_FAILURE);
    }
    return 0;
}