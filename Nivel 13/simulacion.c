#include "simulacion.h"


static int acabados = 0;

//Funcion enterrador
void reaper() {
  pid_t ended;
  signal(SIGCHLD, reaper);
  while ((ended = waitpid(-1, NULL, WNOHANG)) > 0) {
    acabados++;
    //fprintf(stderr, "Proceso acabado: %d / Total acabados: %d\n", ended, acabados);
  }
}

int main(int argc, char **argv) {
	if (argc != 2) {
		printf("Error. Uso: ./simulacion <disco>\n");
		//exit(EXIT_FAILURE);
	}
    if (bmount(argv[1]) == -1) { //Proceso padre
        printf("Error: Montaje incorrecto.\n");
        bumount();
        exit(EXIT_FAILURE);
    }

    char camino[128] = "/simul_";
    time_t tiempo;
    time(&tiempo);
    struct tm *tm = localtime(&tiempo);
  sprintf(camino + strlen(camino), "%d%02d%02d%02d%02d%02d/",
    tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour,
    tm->tm_min, tm->tm_sec);

  if (mi_creat(camino, 7) == -1) {
    printf("Error: Directorio no creado. (%s)\n", camino);
    bumount();
    exit(EXIT_FAILURE);
  }

  signal(SIGCHLD, reaper);



  pid_t pid;
  for (int nproceso = 0; nproceso < MAX_PROCESOS; nproceso++) {
struct REGISTRO registro;
    if ((pid = fork()) == -1) {
      bumount();
      exit(EXIT_FAILURE);
    }
      if (pid == 0) { //Proceso hijo
        //Montamos disco
        if (bmount(argv[1]) == -1) {
            printf("Error: Montaje incorrecto.\n");
            bumount();
            exit(EXIT_FAILURE);
        }

          //Creamos directorio del proceso
        char directorioProceso[1024];
        sprintf(directorioProceso, "%sproceso_%d/", camino, getpid());
        if (mi_creat(directorioProceso, 7) == -1) {
            printf("Error: Directorio no creado. (%s)\n", directorioProceso);
            bumount();
            exit(EXIT_FAILURE);
        }

        //Creamos fichero prueba.dat dentro del directorio del proceso
        char ficheroProceso[100];
        sprintf(ficheroProceso, "%s%s", directorioProceso, FICHERO_PRUEBA);
        if (mi_touch(ficheroProceso, 7) == -1) {
            printf("Error: Fichero no creado. (%s)\n", ficheroProceso);
            bumount();
            exit(EXIT_FAILURE);
        }

        srand(time(NULL) + getpid()); //Inicializamos semilla para numeros aleatorios
        for (int i = 0; i < MAX_ESCRITURAS; i++) {
            registro.fecha = time(NULL);
                    registro.pid = getpid();
            registro.nEscritura = i+1;
            registro.nRegistro = rand() % REGMAX;

            mi_write(ficheroProceso, &registro, registro.nRegistro * sizeof(struct REGISTRO), sizeof(struct REGISTRO));
           // printf("[simulación.c → Escritura %d en %s]\n", i+1, ficheroProceso);
            usleep(TIEMPO_ESCRITURA);
        }

        printf("Proceso %d: Completadas %d escrituras en %s\n", nproceso+1, registro.nEscritura, ficheroProceso);
      //Desmontamos disco
      if (bumount() == -1) {
        printf("Error: Cierre incorrecto.\n");
        exit(EXIT_FAILURE);
      }
          exit(0); //Emitimos señal SIGCHLD
    }

    usleep(TIEMPO_PROCESO);
  }
//Esperamos finalizacion de todos los procesos hijos
  while (acabados < MAX_PROCESOS) pause();

  if (bumount() == -1) { //Proceso padre
    printf("Error: Cierre incorrecto.\n");
    exit(EXIT_FAILURE);
  }

  printf("\n*** Simulacion finalizada ***\n");
  exit(EXIT_SUCCESS);
}
