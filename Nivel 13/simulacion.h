//simulación.h
#include "directorios.h"
#include <signal.h>
#include <sys/wait.h>

#define MAX_PROCESOS 100
#define MAX_ESCRITURAS 50
#define REGMAX 500000
#define TIEMPO_ESCRITURA 50000 //0.05 seg
#define TIEMPO_PROCESO 200000 //0.2 seg
#define FICHERO_PRUEBA "prueba.dat"

struct REGISTRO {
    time_t fecha; //fecha de la escritura en formato epoch 
    pid_t pid; //PID del proceso que lo ha creado
    int nEscritura; //Entero con el número de escritura (de 1 a 50)
    int nRegistro; //Entero con el número del registro dentro del fichero (de 0 a REGMAX-1) 
};

void reaper();
