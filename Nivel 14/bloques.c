/******************************************************************************
*                                  BLOQUES.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÓN: FUNCIONES BÁSICA DE ENTRADA Y SALIDA DE BLOQUES.
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 21 DE FEBRERO DE 2019.
******************************************************************************/


/******************************************************************************
*                           ARCHIVOS AUXILIARES
******************************************************************************/
#include "bloques.h"                    // Librería personalizada.
#include "semaforo_mutex_posix.h"
static sem_t *mutex;
static unsigned int inside_sc = 0;
static int descriptor; 
/******************************************************************************
* Método: bmount()
* Descripción: Este método se utilizará para devolver el descriptor del 
*              fichero pasado por parámetro. 
* Parámetros:  char *camino: puntero al 'path' del fichero. 
* Devuelve:    int:          descriptor de fichero.
******************************************************************************/
int bmount(const char *camino) {
    if (descriptor > 0) close(descriptor);

    descriptor = open(camino, O_RDWR|O_CREAT, 0666);
    if(descriptor == -1) {
        printf("Error bmount: Fichero no abierto.\n");
        return -1; //exit(-1)
    }
    if (!mutex) mutex = initSem();
    if (mutex == SEM_FAILED) return -1;
    return descriptor;
}


/******************************************************************************
* Método: bunmount()
* Descripción: Cierra fichero a partir de su descriptor de fichero.
* Parámetros:  No tiene.
* Devuelve:    0: Si salió bien la operación.
*             -1: Si ha habido un error.
******************************************************************************/
int bumount() {
    descriptor = close(descriptor);
    if(descriptor == -1) {
        printf("Error bumount: Fichero no cerrado.\n");
        return -1;
    }
    deleteSem();
    return 0;
}


/******************************************************************************
* Método: bwrite()
* Descripción:  Escribe en un bloque pasado por parámetro el buffer.
* Parámetros:   nbloque : Numero de bloque a escribir.
*               *buf    : Puntero a los datos a escribir.
* Devuelve:     Numero de bytes escritos o -1 si ha habido un error.
******************************************************************************/
 int bwrite(unsigned int nbloque, const void *buf){
    off_t desplazamiento = nbloque * BLOCKSIZE;
    lseek(descriptor, desplazamiento, SEEK_SET);
    size_t r = write(descriptor, buf, BLOCKSIZE);
    if(r == -1){
        fprintf(stderr, "Error %d: %s\n", errno, strerror(errno));
    }
    return r;
 }



/******************************************************************************
* Método: bread()
* Descripción:  Lee el bloque indicado por parametro y copia su contenido en 
*               el buffer.
* Parámetros:   nbloque : Numero de bloque a leer.
*               *buf    : Puntero al especio de memoria a alojar lo leido.
* Devuelve:     Numero de bytes leidos o -1 en el caso de error. 
******************************************************************************/
int bread(unsigned int nbloque, void *buf){
    off_t desplazamiento = nbloque * BLOCKSIZE;
    lseek(descriptor, desplazamiento, SEEK_SET);
    size_t r = read(descriptor, buf, BLOCKSIZE);
    if(r == -1){
        fprintf(stderr, "Error %d: %s\n", errno, strerror(errno));
    }
    return r;
}

/******************************************************************************
* Método: 
* Descripción: 
* Parámetros: 
* Devuelve:    
******************************************************************************/
void mi_waitSem() {
    if (!inside_sc) {
    waitSem(mutex);
    }
    inside_sc++;
}

/******************************************************************************
* Método: 
* Descripción: 
* Parámetros: 
* Devuelve:    
******************************************************************************/
void mi_signalSem() {
    inside_sc--;
    if (!inside_sc) {
    signalSem(mutex);
    }
}
