#include "verificacion.h"
#define TAM_BUFFER 1024
#include <inttypes.h>

int main(int argc, char *argv[]){
    // Verificacion del numero de argumentos
    if (argc != 3) {
		printf("Error sintaxis: verificacion <disco> <directorio_simulacion>\n");
		exit(EXIT_FAILURE);
	}
    // Montamos el disco
    if (bmount(argv[1]) == -1) {
        printf("Error: Montaje incorrecto.\n");
        bumount();
        exit(EXIT_FAILURE);
    } 

    struct STAT stat;
  
    if (mi_stat(argv[2], &stat) < 0) {
        printf("Error: Error mi_stat.\n");
        bumount();
        exit(EXIT_FAILURE);
    }

    int numentradas = stat.tamEnBytesLog / sizeof(struct entrada);
    printf("numentradas: %i / NUMPROCESOS: %i\n", numentradas, MAX_PROCESOS);
    if (numentradas != MAX_PROCESOS) return -1;


    struct entrada entradas[numentradas];

    //Leemos entradas del directorio de simulacion
	if (mi_read(argv[2], entradas, 0, sizeof(entradas)) < 0) {
		printf("Error: Fallo al leyendo entradas del directorio de simulacion\n");
		exit(EXIT_FAILURE);
	}

    //Creamos informe.txt dentro del directorio de simulacion
    char ficheroInforme[128];
    sprintf(ficheroInforme, "%s%s", argv[2], FICHERO_INFORME);
    if (mi_touch(ficheroInforme, 7) == -1) {
        printf("Error: Fichero no creado. (%s)\n", ficheroInforme);
        bumount();
        exit(EXIT_FAILURE);
    }

    int bytesInforme = 0;

    for (int i = 0; i < numentradas; i++) {
        pid_t pid = atoi(strchr(entradas[i].nombre, '_') + 1);
        struct INFORMACION info;
        //printf("Verificando proceso PID: %i\n", pid);

        char ficheroPrueba[128];
        sprintf(ficheroPrueba, "%s%s/%s", argv[2], entradas[i].nombre, FICHERO_PRUEBA);

        int offset = 0;
        int cant_registros_buffer_escrituras = 256;
        struct REGISTRO buffer_escrituras[cant_registros_buffer_escrituras];

        //Inicializamos
        info.PrimeraEscritura.fecha = 0;
        info.PrimeraEscritura.pid = 0;
        info.PrimeraEscritura.nEscritura = 0;
        info.PrimeraEscritura.nRegistro = 0;
        info.UltimaEscritura.fecha = 0;
        info.UltimaEscritura.pid = 0;
        info.UltimaEscritura.nEscritura = 0;
        info.UltimaEscritura.nRegistro = 0;
        info.MenorPosicion.fecha = 0;
        info.MenorPosicion.pid = 0;
        info.MenorPosicion.nEscritura = 0;
        info.MenorPosicion.nRegistro = 0;
        info.MayorPosicion.fecha = 0;
        info.MayorPosicion.pid = 0;
        info.MayorPosicion.nEscritura = 0;
        info.MayorPosicion.nRegistro = 0;
        info.nEscrituras = 0;
        info.pid = pid;

        while (mi_read(ficheroPrueba, buffer_escrituras, offset, sizeof(buffer_escrituras)) > 0) {
            for (int j = 0; j < 256; j++) {
                if (buffer_escrituras[j].pid == pid) {
                    //printf("PID: %d nEscritura: %d\n", buffer_escrituras[j].pid, buffer_escrituras[j].nEscritura);
                    if (info.nEscrituras == 0) {
                        info.PrimeraEscritura = buffer_escrituras[j];
                        info.UltimaEscritura = buffer_escrituras[j];
                        info.MenorPosicion = buffer_escrituras[j];
                        info.MayorPosicion = buffer_escrituras[j];
                    } else {
                        if (difftime( buffer_escrituras[j].fecha, info.PrimeraEscritura.fecha) <= 0) {
                            if (info.PrimeraEscritura.nEscritura > buffer_escrituras[j].nEscritura) {
                                info.PrimeraEscritura = buffer_escrituras[j];
                            } 
                        }
                        if (difftime(buffer_escrituras[j].fecha, info.UltimaEscritura.fecha) >= 0) {
                            if (info.UltimaEscritura.nEscritura < buffer_escrituras[j].nEscritura) {
                                info.UltimaEscritura = buffer_escrituras[j];
                            } 
                        }
                        
                    }
                    info.nEscrituras++; //Incrementamos nº escrituras validadas
                                        info.MayorPosicion = buffer_escrituras[j];


                }
            }

            memset(buffer_escrituras, 0, sizeof(buffer_escrituras));
            offset += sizeof(buffer_escrituras);

        }
        //clock_gettime(CLOCK_MONOTONIC_RAW, &end);
        //int64_t delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
        //printf("Time: %" PRIi64 " microsegundos\n", delta_us);

        //Imprimimos por consola
        //printf("PID: %i\nNumero escrituras: %i\n", pid, info.nEscrituras);
        printf("%i) %i escrituras validadas en %s\n", i+1, info.nEscrituras, ficheroPrueba);

        //Guardamos informacion del struct info al fichero informe.txt por el final
        char buffer[TAM_BUFFER];
        memset(buffer, 0, TAM_BUFFER);

        sprintf(buffer, "%s %i\n", "PID: ", info.pid);

        sprintf(buffer + strlen(buffer), "%s %i\n", "Numero escrituras: ", info.nEscrituras);

        sprintf(buffer + strlen(buffer), "%s %i %i %s", "Primera escritura: ",
            info.PrimeraEscritura.nEscritura, info.PrimeraEscritura.nRegistro,
                asctime(localtime(&info.PrimeraEscritura.fecha)));

            sprintf(buffer + strlen(buffer), "%s %i %i %s", "Ultima escritura: ",
            info.UltimaEscritura.nEscritura, info.UltimaEscritura.nRegistro,
                asctime(localtime(&info.UltimaEscritura.fecha)));

            sprintf(buffer + strlen(buffer), "%s %i %i %s", "Menor posicion: ",
            info.MenorPosicion.nEscritura, info.MenorPosicion.nRegistro,
                asctime(localtime(&info.MenorPosicion.fecha)));

            sprintf(buffer + strlen(buffer), "%s %i %i %s %s", "Mayor posicion: ",
            info.MayorPosicion.nEscritura, info.MayorPosicion.nRegistro,
                asctime(localtime(&info.MayorPosicion.fecha)), "\n");

        if (mi_write(ficheroInforme, buffer, bytesInforme, strlen(buffer)) < 0) {
            printf("Error: Escritura en el informe incorrecta.\n");
            bumount();
            exit(EXIT_FAILURE);
        }
        bytesInforme += strlen(buffer);
    }

        if (bumount() == -1) {
            printf("Error: Cierre incorrecto.\n");
            exit(EXIT_FAILURE);
        }
    
        printf("\n*** Verificacion finalizada ***\n");
        exit(EXIT_SUCCESS);
}