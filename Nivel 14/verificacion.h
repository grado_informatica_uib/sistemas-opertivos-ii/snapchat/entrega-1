//verificacion.h

#include "simulacion.h"
#define FICHERO_INFORME "informe.txt"

struct INFORMACION {
    int pid; 
    unsigned int nEscrituras; // validadas 
    struct REGISTRO PrimeraEscritura; 
    struct REGISTRO UltimaEscritura;
    struct REGISTRO MenorPosicion;
    struct REGISTRO MayorPosicion;
};
