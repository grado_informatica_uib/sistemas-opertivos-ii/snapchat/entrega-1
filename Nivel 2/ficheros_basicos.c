/******************************************************************************
*                            FICHEROS_BASICOS.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERACTIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÓN:
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 22 DE MARZO DE 2019.
******************************************************************************/


/******************************************************************************
*                           ARCHIVOS AUXILIARES
******************************************************************************/
#include "ficheros_basicos.h"                    // Librería personalizada.

/******************************************************************************
* Método: tamMb()
* Descripción:  Calcula el número de bloques necesarios para alojar el 
*               mapa de bits.
* Parámetros:   nbloques : numero de bloques del disco.
* Devuelve:     tamMB    : número de bloques del MB.
******************************************************************************/
int tamMB(unsigned int nbloques){
    int tamMB = (nbloques/8)/BLOCKSIZE; // Calcula tamaño MB (parte entera)
    // Si necesita 1 bloque más para la parte decimal
    if((nbloques/8)%BLOCKSIZE > 0){     
        tamMB++;                        // Añadimos un bloque (parte decimal)
    }
    // Devolvemos el tamaño del Mapa de Bits
    return tamMB;
}

/******************************************************************************
* Método: tamAI()
* Descripción:  Calcula el numero de bloques necesarios para alojar el Array 
*               de inodos. 
* Parámetros:   ninodos : Número de inodos del sistema.
* Devuelve:     tamAI   : Número de bloques para alojar el Array de inodos. 
******************************************************************************/
int tamAI(unsigned int ninodos){
    int tamAI = ninodos*INODOSIZE/BLOCKSIZE;// Calcula tamaño AI (p. entera)
    // Si necesita 1 bloque más para la parte decimal
    if((ninodos*INODOSIZE)%BLOCKSIZE > 0){
        tamAI++;                            // Añadimos un bloque (p. decimal)
    }
    // Devolvemos el tamaño del Array de Inodos.
    return tamAI;
}

/******************************************************************************
* Método: initSB()
* Descripción:  Inicializa el superbloque con los datos apropiados.
* Parámetros:   nbloques : Número de bloques del disco.
*               inodos   : Número de inodos del disco.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initSB(unsigned int nbloques, unsigned int ninodos){
    // Declaración de nuestro superbloque
    struct superbloque SB;              
    // Número del primer bloque del Mapa de Bits.
    SB.posPrimerBloqueMB = posSB + tamSB;
    // Número del último bloque del Mapa de Bits.
    int var_tamMB = tamMB(nbloques)-1;
    SB.posUltimoBloqueMB = SB.posPrimerBloqueMB + var_tamMB;
    // Número del primer boqque del Array de Inodos.
    SB.posPrimerBloqueAI = SB.posUltimoBloqueMB + 1;
    // Número del último bloque del Array de Inodos.
    int var_tamAI = tamAI(ninodos)-1;
    SB.posUltimoBloqueAI = SB.posPrimerBloqueAI + var_tamAI;
    // Numero del primer bloque de datos.
    SB.posPrimerBloqueDatos = SB.posUltimoBloqueAI + 1;
    // Número del último bloque de datos.
    SB.posUltimoBloqueDatos = nbloques - 1;
    // Posición inodo raíz.
    SB.posInodoRaiz = 0;
    // Posición del primer inodo libre.
    SB.posPrimerInodoLibre = 0; // ESTO PARECE QUE SE HA DE CAMBIAR AL FINAL.
    // Número de bloques libres
    SB.cantBloquesLibres = nbloques;
    // Número de inodos libres
    SB.cantInodosLibres = ninodos;
    // Cantidad total de bloques
    SB.totBloques = nbloques;
    // Cantidad total de inodos
    SB.totInodos = ninodos;
    // Escribimos los datos en la memoria superbloque.
    int r = bwrite(posSB, &SB);
    // Comprobamos si ha habido algún error.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Escritura SuperBloque incorrecta.\n");
        return r;
    }
    return 0;
}

/******************************************************************************
* Método: initMB()
* Descripción:  Inicializa los valores del Mapa de Bits a 0. 
* Parámetros:   Ninguno.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initMB(){
    // Valor que vamos a escribir en el MB al inicializarlo.
    unsigned char initMBValue [BLOCKSIZE];
    memset(initMBValue,0,BLOCKSIZE);
    // Declaración Super Bloque.
    struct superbloque SB;

    // Leemos el superbloque y lo guardamos en SB
    int r = bread(posSB, &SB);
    // Si se ha producido un error.cd
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura Mapa de Bits incorrecta.\n");
        return r;
    }

    // Recorremos el Mapa de Bits para inicializar sus valores.
    for(int i = SB.posPrimerBloqueMB; i <= SB.posUltimoBloqueMB; i++)
    {
        // Escribimos el initMBValue en el MB.
        int r = bwrite(i,initMBValue);
        // Si se ha producido un error
        if(r == -1){
            // Imprimimos el error por la salida estándar de errores.
            fprintf(stderr, "Error: Escritura Mapa de Bits incorrecta.\n");
            return r;
        }
    }
    // Si hemos llegado hasta aquí todo ha ido bien: Devolvemos 0;
    return 0;
}

/******************************************************************************
* Método: initAI()
* Descripción:  Inicializa el Array de Inodos. Al principio todos los inodos 
*               están libres y por tanto enlazados.
* Parámetros:   Ninguno.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initAI(){
    // Declaración de array de inodos par luego escribirlos.
    struct inodo inodos[BLOCKSIZE/INODOSIZE];
    // Declaración del superbloque para coger datos.
    struct superbloque SB;
    // Leemos el superbloque y lo almacenamos en SB
    int r = bread(posSB, &SB);
    // Si ha habido algún error.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura SuperBloque incorrecta.\n");
        return r;
    }
    // Contador de Inodo para hacer el enlace entre Inodos.
    int contInodos = SB.posPrimerInodoLibre + 1;
    // Recorremos los bloques que contienen el Array de Inodos.
    for(int i = SB.posPrimerBloqueAI; i <= SB.posUltimoBloqueAI;i++)
    {        
        // Recorremos cada Inodo dentro del bloque
        for(int j = 0; j < BLOCKSIZE / INODOSIZE ; j++)
        {
            // Asignamos el tipo 'libre'
           inodos[j].tipo = 'l'; // libre
           // Si no hemos llegado al último inodo
           if(contInodos < SB.totInodos){
                // Lo enlazamos con el siguiente.
			    inodos[j].punterosDirectos[0] = contInodos;
				contInodos++;
           }
           else {
                // El último elemento no se puede enlazar con nada.
				inodos[j].punterosDirectos[0] = UINT_MAX;
                j = BLOCKSIZE/INODOSIZE;
           }
        }
        // Escribimos el grupo de inodos en el Array de Inodos,
        r = bwrite(i, inodos);
        // SI ha habido algun error.
        if (r == -1) {
            // Imprimimos el error por la salida estándar de errores.
            fprintf(stderr, "Error: Escritura Inodos del bloque incorrecta.\n");
            return r;     
        }  
    }
    // Si hemos llegado hasta aquí es que ha ido todo correctamente.
    return 0;
}
