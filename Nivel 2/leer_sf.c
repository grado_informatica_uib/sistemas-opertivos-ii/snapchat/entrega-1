/******************************************************************************
*                                LEER_SF.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERACTIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÓN:
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 22 DE MARZO DE 2019.
******************************************************************************/
#include "ficheros_basicos.h"
int main (int argc, char *argv[]){
    if (argc != 2) {
      fprintf(stderr, "Error: Arg.1: Nombre fichero.\n");
      exit(EXIT_FAILURE);
    } 
    /**************************************************************************
     *                          MONTAMOS EL DISCO
     *************************************************************************/
    bmount(argv[1]);


    /**************************************************************************
     *                               PRUEBAS
     *************************************************************************/ 
    struct superbloque SB;

  if (bread(posSB, &SB) == -1) {
    perror("Error: Lectura superbloque incorrecta.\n");
    bumount();
    exit(EXIT_FAILURE);
  } else {
    printf("Lectura superbloque correcta.\n");
  }

    printf("DATOS DEL SUPERBLOQUE:\n\n");
    printf("posPrimerBloqueMB = %d\n", SB.posPrimerBloqueMB);
    printf("posUltimoBloqueMB = %d\n", SB.posUltimoBloqueMB);
    printf("posPrimerBloqueAI = %d\n", SB.posPrimerBloqueAI);
    printf("posUltimoBloqueAI = %d\n", SB.posUltimoBloqueAI);
    printf("posPrimerBloqueDatos = %d\n", SB.posPrimerBloqueDatos);
    printf("posUltimoBloqueDatos = %d\n", SB.posUltimoBloqueDatos);
    printf("posInodoRaiz = %d\n", SB.posInodoRaiz);
    printf("poPrimerInodoLibre = %d\n", SB.posPrimerInodoLibre);
    printf("cantBloquesLibres = %d\n", SB.cantBloquesLibres);
    printf("totBloques = %d\n", SB.totBloques);
    printf("totInodos = %d\n\n", SB.totInodos);

    printf("sizeof struct superbloque: %ld\n", sizeof(struct superbloque));
    printf("sizeof struct inodo: %ld\n\n", sizeof(struct inodo));

    struct inodo inodos[BLOCKSIZE/INODOSIZE];

    for(int i = SB.posPrimerBloqueAI; i <= SB.posUltimoBloqueAI; i++) {
        if(bread(i,&inodos) == -1){
            printf("error");
            exit(EXIT_FAILURE);
        };
        for(int j = 0; j < 8; j++){
            printf("%d ", inodos[j].punterosDirectos[0]);
        }
    }
    
    
    /**************************************************************************
     *                         DESMONTAMOS DISCO
     *************************************************************************/
    if (bumount() == -1) exit(EXIT_FAILURE);
    exit(EXIT_SUCCESS);
}