/******************************************************************************
*                            FICHEROS_BASICOS.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERACTIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÓN:
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 22 DE MARZO DE 2019.
******************************************************************************/


/******************************************************************************
*                           ARCHIVOS AUXILIARES
******************************************************************************/
#include "ficheros_basicos.h"                    // Librería personalizada.

/******************************************************************************
* Método: tamMb()
* Descripción:  Calcula el número de bloques necesarios para alojar el 
*               mapa de bits.
* Parámetros:   nbloques : numero de bloques del disco.
* Devuelve:     tamMB    : número de bloques del MB.
******************************************************************************/
int tamMB(unsigned int nbloques){
    int tamMB = (nbloques/8)/BLOCKSIZE; // Calcula tamaño MB (parte entera)
    // Si necesita 1 bloque más para la parte decimal
    if((nbloques/8)%BLOCKSIZE > 0){     
        tamMB++;                        // Añadimos un bloque (parte decimal)
    }
    // Devolvemos el tamaño del Mapa de Bits
    return tamMB;
}

/******************************************************************************
* Método: tamAI()
* Descripción:  Calcula el numero de bloques necesarios para alojar el Array 
*               de inodos. 
* Parámetros:   ninodos : Número de inodos del sistema.
* Devuelve:     tamAI   : Número de bloques para alojar el Array de inodos. 
******************************************************************************/
int tamAI(unsigned int ninodos){
    int tamAI = ninodos*INODOSIZE/BLOCKSIZE;// Calcula tamaño AI (p. entera)
    // Si necesita 1 bloque más para la parte decimal
    if((ninodos*INODOSIZE)%BLOCKSIZE > 0){
        tamAI++;                            // Añadimos un bloque (p. decimal)
    }
    // Devolvemos el tamaño del Array de Inodos.
    return tamAI;
}

/******************************************************************************
* Método: initSB()
* Descripción:  Inicializa el superbloque con los datos apropiados.
* Parámetros:   nbloques : Número de bloques del disco.
*               inodos   : Número de inodos del disco.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initSB(unsigned int nbloques, unsigned int ninodos){
    // Declaración de nuestro superbloque
    struct superbloque SB;              
    // Número del primer bloque del Mapa de Bits.
    SB.posPrimerBloqueMB = posSB + tamSB;
    // Número del último bloque del Mapa de Bits.
    int var_tamMB = tamMB(nbloques)-1;
    SB.posUltimoBloqueMB = SB.posPrimerBloqueMB + var_tamMB;
    // Número del primer boqque del Array de Inodos.
    SB.posPrimerBloqueAI = SB.posUltimoBloqueMB + 1;
    // Número del último bloque del Array de Inodos.
    int var_tamAI = tamAI(ninodos)-1;
    SB.posUltimoBloqueAI = SB.posPrimerBloqueAI + var_tamAI;
    // Numero del primer bloque de datos.
    SB.posPrimerBloqueDatos = SB.posUltimoBloqueAI + 1;
    // Número del último bloque de datos.
    SB.posUltimoBloqueDatos = nbloques - 1;
    // Posición inodo raíz.
    SB.posInodoRaiz = 0;
    // Posición del primer inodo libre.
    SB.posPrimerInodoLibre = 0; // ESTO PARECE QUE SE HA DE CAMBIAR AL FINAL.
    // Número de bloques libres
    SB.cantBloquesLibres = nbloques;
    // Número de inodos libres
    SB.cantInodosLibres = ninodos;
    // Cantidad total de bloques
    SB.totBloques = nbloques;
    // Cantidad total de inodos
    SB.totInodos = ninodos;
    // Escribimos los datos en la memoria superbloque.
    int r = bwrite(posSB, &SB);
    // Comprobamos si ha habido algún error.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Escritura SuperBloque incorrecta.\n");
        return r;
    }
    return 0;
}

/******************************************************************************
* Método: initMB()
* Descripción:  Inicializa los valores del Mapa de Bits a 0. 
* Parámetros:   Ninguno.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initMB(){
    // Valor que vamos a escribir en el MB al inicializarlo.
    unsigned char initMBValue [BLOCKSIZE];
    memset(initMBValue,0,BLOCKSIZE);
    // Declaración Super Bloque.
    struct superbloque SB;

    // Leemos el superbloque y lo guardamos en SB
    int r = bread(posSB, &SB);
    // Si se ha producido un error.cd
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura Mapa de Bits incorrecta.\n");
        return r;
    }

    // Recorremos el Mapa de Bits para inicializar sus valores.
    for(int i = SB.posPrimerBloqueMB; i <= SB.posUltimoBloqueMB; i++)
    {
        // Escribimos el initMBValue en el MB.
        int r = bwrite(i,initMBValue);
        // Si se ha producido un error
        if(r == -1){
            // Imprimimos el error por la salida estándar de errores.
            fprintf(stderr, "Error: Escritura Mapa de Bits incorrecta.\n");
            return r;
        }
    }
    // Actualizamos el MB con los datos ya ocupados (SB, )
    for(int i = posSB; i <= SB.posUltimoBloqueAI; i++){
        escribir_bit(i,1);          // bloque ocupado
        SB.cantBloquesLibres--;     // Restamos uno a los bloques libres.
    }
    r = bwrite(posSB, &SB);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Escritura actualizacion Superbloque incorrecta.\n");
        return r;
    }
    // Si hemos llegado hasta aquí todo ha ido bien: Devolvemos 0;
    return 0;
}

/******************************************************************************
* Método: initAI()
* Descripción:  Inicializa el Array de Inodos. Al principio todos los inodos 
*               están libres y por tanto enlazados.
* Parámetros:   Ninguno.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initAI(){
    // Declaración de array de inodos par luego escribirlos.
    struct inodo inodos[BLOCKSIZE/INODOSIZE];
    // Declaración del superbloque para coger datos.
    struct superbloque SB;
    // Leemos el superbloque y lo almacenamos en SB
    int r = bread(posSB, &SB);
    // Si ha habido algún error.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura SuperBloque incorrecta.\n");
        return r;
    }
    // Contador de Inodo para hacer el enlace entre Inodos.
    int contInodos = SB.posPrimerInodoLibre + 1;
    // Recorremos los bloques que contienen el Array de Inodos.
    for(int i = SB.posPrimerBloqueAI; i <= SB.posUltimoBloqueAI;i++)
    {        
        // Recorremos cada Inodo dentro del bloque
        for(int j = 0; j < BLOCKSIZE / INODOSIZE ; j++)
        {
            // Asignamos el tipo 'libre'
           inodos[j].tipo = 'l'; // libre
           // Si no hemos llegado al último inodo
           if(contInodos < SB.totInodos){
                // Lo enlazamos con el siguiente.
			    inodos[j].punterosDirectos[0] = contInodos;
				contInodos++;
           }
           else {
                // El último elemento no se puede enlazar con nada.
				inodos[j].punterosDirectos[0] = UINT_MAX;
                j = BLOCKSIZE/INODOSIZE;
           }
        }
        // Escribimos el grupo de inodos en el Array de Inodos,
        r = bwrite(i, inodos);
        // SI ha habido algun error.
        if (r == -1) {
            // Imprimimos el error por la salida estándar de errores.
            fprintf(stderr, "Error: Escritura Inodos del bloque incorrecta.\n");
            return r;     
        }  
    }
    // Si hemos llegado hasta aquí es que ha ido todo correctamente.
    return 0;
}


/******************************************************************************
* Método: escribir_bit()
* Descripción:  Escribe el valor indicado por elparámetro bit (0 ó 1) en un 
*               determinado bit del MP que representa el bloque nbloque.
* Parámetros:   unsigned int nbloque :  Número de bloque.  
*               unsigned int bit     :  Bit a escribir.
* Devuelve:     0   : Ha funcionado correctamente.
*              -1   : Ha habido algún problema.
******************************************************************************/
int escribir_bit(unsigned int nbloque, unsigned int bit){
    if ((bit!=0) && (bit!=1)) {
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: el bit a escribir debe ser 0 ó 1.\n");
        return -1; 
    }
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    int posbyte = nbloque / 8;              // Calculamos el posbyte
    int posbit = nbloque % 8;               // Calculamos el posbit
    int nbloqueMB = posbyte / BLOCKSIZE;    // Calculamos el bloqueMB
    int nbloqueabs = nbloqueMB + SB.posPrimerBloqueMB;  // Bloque absoluto
    unsigned char bufferMB[BLOCKSIZE];      // Buffer de MB
    r = bread(nbloqueabs, &bufferMB);       // Leemos el buffer del disco.
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del bloque MB incorrecta.\n");
        return r;    
    }
    posbyte = posbyte % BLOCKSIZE; // Calculo byte implicado en el bloqueMB.
    unsigned char mascara = 128;   // Máscara para setear el valor del bit.
    mascara >>= posbit;            // Desplazamos el bit conforme posbit.
    // Si el bit es 0 (libre)
    if(bit == 0){
        bufferMB[posbyte] &= ~mascara;  // Actualizamos bufferMB[posbyte]
    }
    // Si el bit es 1 (ocupado)
    else{
        bufferMB[posbyte] |= mascara;   // Actualizamos bufferMB[posbyte]
    }
    r = bwrite(nbloqueabs, bufferMB);   // Escribimos el bloque en disco.
    // Si ha habido algún problema en la escritura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Escrita del bloque MB incorrecta.\n");
        return r;    
    }
    // Si hemos llegado hasta aquí significa que todo ha ido bien y return 0.
    return 0;
}

/******************************************************************************
* Método: leer_bit()
* Descripción:  Lee un determinado bit del MB y devuelve el valor del bit 
*               leído.
* Parámetros:   nbloque  : Número de bloque que queremos leer.
* Devuelve:     El valor del bit leído.
*               -1 si ha habido algun problema.
******************************************************************************/
unsigned char leer_bit(unsigned int nbloque){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    int posbyte = nbloque / 8;              // Calculamos el posbyte
    int posbit = nbloque % 8;               // Calculamos el posbit
    int nbloqueMB = posbyte / BLOCKSIZE;    // Calculamos el bloqueMB
    int nbloqueabs = nbloqueMB + SB.posPrimerBloqueMB;  // Bloque absoluto
    unsigned char bufferMB[BLOCKSIZE];      // Buffer de MB
    r = bread(nbloqueabs, &bufferMB);       // Leemos el buffer del disco.
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del bloque MB incorrecta.\n");
        return r;    
    }
    posbyte = posbyte % BLOCKSIZE; // Calculo byte implicado en el bloqueMB.
    unsigned char mascara = 128;   // Máscara para setear el valor del bit.
    mascara >>= posbit;            // Desplazamos el bit conforme posbit.
    mascara &= bufferMB[posbyte];  // Obtenemos en la mascara el valor del byte
    mascara >>= (7-posbit);        // Desplezamos a la derecha
    printf("[leer_bit()→ nbloque: %d, posbyte:%d, posbit:%d, nbloqueMB:%d, nbloqueabs:%d)]\n", nbloque, posbyte, posbit, nbloqueMB, nbloqueabs );
    return mascara;
}

/******************************************************************************
* Método: reservar_bloque()
* Descripción:  Encuentra el primer bloque libre, consultando el MB, lo ocupa 
*               y devuelve su posición.
* Parámetros:   Ninguno
* Devuelve:     Posición del primer bloque libre.
*              -1 : si ha habido algún problema.
******************************************************************************/
int reservar_bloque(){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Si hay bloques libres.
    if(SB.cantBloquesLibres == 0){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: No hay bloques libres disponibles.\n");
        return -1;       
    }
    /**********************************************************
     *  Encontramos un bloque MB que tiene un 0.
     **********************************************************/
    unsigned char bufferMB[BLOCKSIZE];      // Buffer de MB
    unsigned char bufferAux[BLOCKSIZE];     // Buffer auxiliar (comparar)
    memset(bufferAux, 255, BLOCKSIZE);      // Set buffer 1111 1111
    int posBloqueMB = SB.posPrimerBloqueMB;          // Posicion bloque MB con 0
    int encontrado = 0;       // Control de bloque encontrado
    // Recorrido de MB hasta encontrar bloque que tenga algún 0.
    while(posBloqueMB <= SB.posUltimoBloqueMB && encontrado != 1){
            bread(posBloqueMB, bufferMB);
            if(memcmp(bufferAux, bufferMB, BLOCKSIZE) > 0){
                encontrado = 1;        // Lo encontramos
            }
            else {
                posBloqueMB++;
            }
    }
    /**********************************************************
     *  Encontramos el byte MB que tiene un 0.
     **********************************************************/
    int byteEncontrado = 0;    // Control de byte encontrado
    int posbyte = 0;               // Posición byte encontrado.
    // Recorremos el bloque en busca de byte con algún 0.
    while(posbyte <= BLOCKSIZE && byteEncontrado != 1){
        // Si encontramos un byte con 0.
        int i = 255 - bufferMB[posbyte];
        if(i> 0) byteEncontrado = 1;         // Lo encontramos
        else posbyte++;
    }
    /**********************************************************
     *  Encontramos el bit MB que vale 0.
     **********************************************************/
    unsigned char mascara = 128;        // 1000 0000
    int posbit = 0;                     // Posición del bit 0.
    // Mientras no encontremos el bit con 0.
    while((bufferMB[posbyte] & mascara) != 0){
        posbit++;
        bufferMB[posbyte] <<= 1; //desplazamiento izquierda.
    }
    // Calculamos el número de bloque resultando.
    int nbloque = ((posBloqueMB - SB.posPrimerBloqueMB)*BLOCKSIZE + posbyte)*8 + posbit;
    // Escribimos en el MB que el bloque nbloque estará ocupado.
    r = escribir_bit(nbloque, 1);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Fallo en la reserva del bloque.\n");
        return -1;  
    }
    SB.cantBloquesLibres--;        // Actualizamos los bloques libres en SB.
    memset(bufferAux, 0, BLOCKSIZE);      // Set buffer 0000 0000
    r = bwrite(nbloque, &bufferAux);    // Reseteamos los valores del bloque a 0
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: No se pudo resetear valores bloque.\n");
        return -1;  
    }
    r = bwrite(posSB, &SB);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: No se pudo actualizar superbloque.\n");
        return -1;  
    }
    // Si hemos llegado aquí todo ha ido bien, devolvemos número de bloque.
    return nbloque;
}

/******************************************************************************
* Método: liberar_bloque()
* Descripción:  Libera un bloque determinado.
* Parámetros:   nbloque : Número del bloque de se desea liberar.
* Devuelve:     nbloque : Si ha funcionado correctamente.
*              -1       : Si ha habido algún error.
******************************************************************************/
int liberar_bloque(unsigned int nbloque){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Actualizamos el valor del bloque en MB
    r = escribir_bit(nbloque, 0);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: No se pudo liberar el bloque.\n");
        return -1;  
    }
    // Aumentos el número de bloques libres.
    SB.cantBloquesLibres++;
    // Actualizamos el superbloque en el disco virtual
    r = bwrite(posSB, &SB);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: No se ha podido actualizarel superbloque.\n");
        return r; 
    }
    // Devolvemos el número de bloque liberado.
    return nbloque;
}

/******************************************************************************
* Método: escribir_inodo()
* Descripción:  Escribe el contenido de una variable de tipo struct inodo en 
*               un determinado inodo del array de inodos.
* Parámetros:   ninodo  : Número del inodo donde se quiere escribir.
*               inodo   : Inodo a escribir.
* Devuelve:     0  : Si ha funcionado correctamente.
*              -1  : Si ha habido algún error.
******************************************************************************/
int escribir_inodo(unsigned int ninodo, struct inodo inodo){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Calculamos el bloque del inodo que queremos modificar.
    int posbloqueinodo = (ninodo/(BLOCKSIZE/INODOSIZE)) + SB.posPrimerBloqueAI;
    // Buffer que utilizaremos para modificar el array de inodos. 
    struct inodo inodos[BLOCKSIZE/INODOSIZE];
    // Leemos el bloque del disco
    r = bread(posbloqueinodo, &inodos);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del Array de Inodos incorrecta.\n");
        return r;    
    }
    // Calculamos la posición del inodo dentro del bloque 
    int posInodo = ninodo%(BLOCKSIZE/INODOSIZE);
    // Modificamos el inodo dentro del buffer.
    inodos[posInodo] = inodo;
    // Escribimos el buffer en el disco.
    r = bwrite(posbloqueinodo, &inodos);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Escritura del Array de Inodos incorrecta.\n");
        return r;    
    }
    // Si hemos llegado hasta aquí significa que todo ha ido bien.
    return 0;
}

/******************************************************************************
* Método: leer_inodo()
* Descripción:  Lee un determinado inodo del array de inodos para volcarlo en 
*               una variable tipo struct inodo pasada por referencia.
* Parámetros:   ninodo : Número de inodo que se quiere leer.
*               *inodo : Puntero a un inodo donde se guardarán los datos leídos
* Devuelve:     0  : Si ha funcionado correctamente.
*              -1  : Si ha habido algún error.
******************************************************************************/
int leer_inodo(unsigned int ninodo, struct inodo *inodo){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Calculamos el bloque del inodo que queremos modificar.
    int posbloqueinodo = (ninodo/(BLOCKSIZE/INODOSIZE)) + SB.posPrimerBloqueAI;
    // Buffer que utilizaremos para modificar el array de inodos. 
    struct inodo inodos[BLOCKSIZE/INODOSIZE];
    // Leemos el bloque del disco
    r = bread(posbloqueinodo, &inodos);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del Array de Inodos incorrecta.\n");
        return r;    
    }
    // Calculamos la posición del inodo dentro del bloque 
    int posInodo = ninodo%(BLOCKSIZE/INODOSIZE);
    // Devolvemos el inodo modificando el parámetro*inodo
    *inodo =  inodos[posInodo];
    // Si hemos llegado hasta aquí es que todo ha ido bien.
    return 0;
}

/******************************************************************************
* Método: reservar_inodo()
* Descripción:  Encuentra el primer inodo libre, lo reserva, devuelve su 
*               número y actualiza la lista enlazada de inodos libres.
* Parámetros:   tipo     : (directorio | fichero | libre)
*               permisos : Permisos que se le atribuyen (0-7)
* Devuelve:     Número de inodo reservado.
*               -1 : Si ha habido algún problema.
******************************************************************************/
int reservar_inodo(unsigned char tipo, unsigned char permisos){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Si hay bloques libres.
    if(SB.cantInodosLibres == 0){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: No hay inodos libres disponibles.\n");
        return -1;       
    }
    // Posición del inodo que vamos a reservar.
    int posInodoReservado = SB.posPrimerInodoLibre;
    // Declaramos una estructura inodo.
    struct inodo inodo;
    // Leermos el inodo
    leer_inodo(posInodoReservado, &inodo);
    // Hacemos apuntar al superbloque al siguiente inodo libre.
    SB.posPrimerInodoLibre = inodo.punterosDirectos[0];
    // Inicialiamos el inodo.
    inodo.tipo = tipo;
    inodo.permisos = permisos;
    inodo.nlinks = 1;
    inodo.tamEnBytesLog = 0;
    inodo.atime = time(NULL);
    inodo.mtime = time(NULL);
    inodo.ctime = time(NULL);
    inodo.numBloquesOcupados = 0;
    for(int i = 0; i < (int) (sizeof(inodo.punterosDirectos)/sizeof(int)); i++){
       inodo.punterosDirectos[i] = 0;
    }
    for(int i = 0; i < (int) (sizeof(inodo.punterosIndirectos)/sizeof(int)); i++){
       inodo.punterosIndirectos[i] = 0;
    }
    // Escribimos el inodo
    r = escribir_inodo(posInodoReservado, inodo);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Escritura del inodo incorrecta.\n");
        return r;    
    }
    // Actualizamos la cantidad inodos libres.
    SB.cantInodosLibres--;
    // Escribimos el SB en el disco.
    r = bwrite(posSB, &SB);
    // Si ha habido algún error.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Escritura en superbloque incorrecta.\n");
        return r;    
    }
    // Si hemos llegado aquí todo ha ido bien.
    return posInodoReservado;
}