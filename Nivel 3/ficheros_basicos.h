/******************************************************************************
*                          FICHEROS_BASICOS.H 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 22 DE MARZO DE 2019.
******************************************************************************/

/* HAY QUE PONER ESTO BONITO */

#include <stdio.h>  //printf(), fprintf(), stderr, stdout, stdin
#include <fcntl.h> //O_WRONLY, O_CREAT, O_TRUNC
#include <sys/stat.h> //S_IRUSR, S_IWUSR
#include <stdlib.h>  //exit(), EXIT_SUCCESS, EXIT_FAILURE, atoi()
#include <unistd.h> // SEEK_SET, read(), write(), open(), close(), lseek()
#include <errno.h>  //errno
#include <string.h> // strerror()
#include <limits.h>  // UINT_MAX
#include <time.h>   // time

#include "bloques.h"

#define  INODOSIZE 128              // Tamaño de los inodos 128 bytes.

#define  posSB  0
#define  tamSB  1

struct superbloque {
   unsigned int posPrimerBloqueMB;                       // Posición del primer bloque del mapa de bits en el SF
   unsigned int posUltimoBloqueMB;                       // Posición del último bloque del mapa de bits en el SF
   unsigned int posPrimerBloqueAI;                       // Posición del primer bloque del array de inodos en el SF
   unsigned int posUltimoBloqueAI;                       // Posición del último bloque del array de inodos en el SF
   unsigned int posPrimerBloqueDatos;                    // Posición del primer bloque de datos en el SF
   unsigned int posUltimoBloqueDatos;                    // Posición del último bloque de datos en el SF
   unsigned int posInodoRaiz;                            // Posición del inodo del directorio raíz en el AI 
   unsigned int posPrimerInodoLibre;                     // Posición del primer inodo libre en el AI
   unsigned int cantBloquesLibres;                       // Cantidad de bloques libres del SF
   unsigned int cantInodosLibres;                        // Cantidad de inodos libres del SF
   unsigned int totBloques;                              // Cantidad total de bloques del SF
   unsigned int totInodos;                               // Cantidad total de inodos del SF
   char padding[BLOCKSIZE - 12 * sizeof(unsigned int)];  // Relleno para que ocupe 1 bloque
};



struct inodo {     // comprobar que ocupa 128 bytes haciendo un sizeof(inodo)!!!
   char tipo;     // Tipo ('l':libre, 'd':directorio o 'f':fichero)
   char permisos; // Permisos (lectura y/o escritura y/o ejecución)
   /* Por cuestiones internas de alineación de estructuras, si se está utilizando
    un tamaño de palabra de 4 bytes (microprocesadores de 32 bits):
   unsigned char reservado_alineacion1 [2];
   en caso de que la palabra utilizada sea del tamaño de 8 bytes
   (microprocesadores de 64 bits): unsigned char reservado_alineacion1 [6]; */
   char reservado_alineacion1[6];
   time_t atime; // Fecha y hora del último acceso a datos: atime
   time_t mtime; // Fecha y hora de la última modificación de datos: mtime
   time_t ctime; // Fecha y hora de la última modificación del inodo: ctime

   /* comprobar el tamaño del tipo time_t para vuestra plataforma/compilador:
   printf ("sizeof time_t is: %d\n", sizeof(time_t)); */

   unsigned int nlinks;             // Cantidad de enlaces de entradas en directorio
   unsigned int tamEnBytesLog;      // Tamaño en bytes lógicos. Se actualizará al escribir si crece
   unsigned int numBloquesOcupados; // Cantidad de bloques ocupados zona de datos

   unsigned int punterosDirectos[12];  // 12 punteros a bloques directos
   unsigned int punterosIndirectos[3]; /* 3 punteros a bloques indirectos:
   1 indirecto simple, 1 indirecto doble, 1 indirecto triple */

   /* Utilizar una variable de alineación si es necesario para vuestra plataforma/compilador */
   char
       padding[INODOSIZE - 2 * sizeof(unsigned char) - 3 * sizeof(time_t) - 18 * sizeof(unsigned int) - 6 * sizeof(unsigned char)];
   // Hay que restar también lo que ocupen las variables de alineación utilizadas!!!
};

/******************************************************************************
* Método: tamMb()
* Descripción:  Calcula el número de bloques necesarios para alojar el 
*               mapa de bits.
* Parámetros:   nbloques : numero de bloques del disco.
* Devuelve:     tamMB    : número de bloques del MB.
******************************************************************************/
int tamMB(unsigned int nbloques);

/******************************************************************************
* Método: tamAI()
* Descripción:  Calcula el numero de bloques necesarios para alojar el Array 
*               de inodos. 
* Parámetros:   ninodos : Número de inodos del sistema.
* Devuelve:     tamAI   : Número de bloques para alojar el Array de inodos. 
******************************************************************************/
int tamAI(unsigned int ninodos);

/******************************************************************************
* Método: initSB()
* Descripción:  Inicializa el superbloque con los datos apropiados.
* Parámetros:   nbloques : Número de bloques del disco.
*               inodos   : Número de inodos del disco.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initSB(unsigned int nbloques, unsigned int ninodos);

/******************************************************************************
* Método: initMB()
* Descripción:  Inicializa los valores del Mapa de Bits a 0. 
* Parámetros:   Ninguno.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initMB();

/******************************************************************************
* Método: initAI()
* Descripción:  Inicializa el Array de Inodos. Al principio todos los inodos 
*               están libres y por tanto enlazados.
* Parámetros:   Ninguno.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initAI();

/******************************************************************************
* Método: escribir_bit()
* Descripción:  Escribe el valor indicado por elparámetro bit (0 ó 1) en un 
*               determinado bit del MP que representa el bloque nbloque.
* Parámetros:   unsigned int nbloque :  Número de bloque.  
*               unsigned int bit     :  Bit a escribir.
* Devuelve:     0   : Ha funcionado correctamente.
*              -1   : Ha habido algún problema.
******************************************************************************/
int escribir_bit(unsigned int nbloque, unsigned int bit);

/******************************************************************************
* Método: leer_bit()
* Descripción:  Lee un determinado bit del MB y devuelve el valor del bit 
*               leído.
* Parámetros:   nbloque  : Número de bloque que queremos leer.
* Devuelve:     El valor del bit leído.
*               -1 si ha habido algun problema.
******************************************************************************/
unsigned char leer_bit(unsigned int nbloque);

/******************************************************************************
* Método: reservar_bloque()
* Descripción:  Encuentra el primer bloque libre, consultando el MB, lo ocupa 
*               y devuelve su posición.
* Parámetros:   Ninguno
* Devuelve:     Posición del primer bloque libre.
*              -1 : si ha habido algún problema.
******************************************************************************/
int reservar_bloque();

/******************************************************************************
* Método: liberar_bloque()
* Descripción:  Libera un bloque determinado.
* Parámetros:   nbloque : Número del bloque de se desea liberar.
* Devuelve:     nbloque : Si ha funcionado correctamente.
*              -1       : Si ha habido algún error.
******************************************************************************/
int liberar_bloque(unsigned int nbloque);

/******************************************************************************
* Método: escribir_inodo()
* Descripción:  Escribe el contenido de una variable de tipo struct inodo en 
*               un determinado inodo del array de inodos.
* Parámetros:   ninodo  : Número del inodo donde se quiere escribir.
*               inodo   : Inodo a escribir.
* Devuelve:     0  : Si ha funcionado correctamente.
*              -1  : Si ha habido algún error.
******************************************************************************/
int escribir_inodo(unsigned int ninodo, struct inodo inodo);

/******************************************************************************
* Método: leer_inodo()
* Descripción:  Lee un determinado inodo del array de inodos para volcarlo en 
*               una variable tipo struct inodo pasada por referencia.
* Parámetros:   ninodo : Número de inodo que se quiere leer.
*               *inodo : Puntero a un inodo donde se guardarán los datos leídos
* Devuelve:     0  : Si ha funcionado correctamente.
*              -1  : Si ha habido algún error.
******************************************************************************/
int leer_inodo(unsigned int ninodo, struct inodo *inodo);

/******************************************************************************
* Método: reservar_inodo()
* Descripción:  Encuentra el primer inodo libre, lo reserva, devuelve su 
*               número y actualiza la lista enlazada de inodos libres.
* Parámetros:   tipo     : (directorio | fichero | libre)
*               permisos : Permisos que se le atribuyen (0-7)
* Devuelve:     Número de inodo reservado.
*               -1 : Si ha habido algún problema.
******************************************************************************/
int reservar_inodo(unsigned char tipo, unsigned char permisos);