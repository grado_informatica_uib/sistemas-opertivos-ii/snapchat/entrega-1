/******************************************************************************
*                                LEER_SF.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERACTIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÓN:
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 22 DE MARZO DE 2019.
******************************************************************************/
#include "ficheros_basicos.h"
int main (int argc, char *argv[]){
    if (argc != 2) {
      fprintf(stderr, "Error: Arg.1: Nombre fichero.\n");
      exit(EXIT_FAILURE);
    } 
    /**************************************************************************
     *                          MONTAMOS EL DISCO
     *************************************************************************/
    bmount(argv[1]);


    /**************************************************************************
     *                               PRUEBAS
     *************************************************************************/ 
    struct superbloque SB;

    if (bread(posSB, &SB) == -1) {
      perror("Error: Lectura superbloque incorrecta.\n");
      bumount();
      exit(EXIT_FAILURE);
    } 

    printf("DATOS DEL SUPERBLOQUE:\n\n");
    printf("posPrimerBloqueMB = %d\n", SB.posPrimerBloqueMB);
    printf("posUltimoBloqueMB = %d\n", SB.posUltimoBloqueMB);
    printf("posPrimerBloqueAI = %d\n", SB.posPrimerBloqueAI);
    printf("posUltimoBloqueAI = %d\n", SB.posUltimoBloqueAI);
    printf("posPrimerBloqueDatos = %d\n", SB.posPrimerBloqueDatos);
    printf("posUltimoBloqueDatos = %d\n", SB.posUltimoBloqueDatos);
    printf("posInodoRaiz = %d\n", SB.posInodoRaiz);
    printf("posPrimerInodoLibre = %d\n", SB.posPrimerInodoLibre);
    printf("cantBloquesLibres = %d\n", SB.cantBloquesLibres);
    printf("canInodosLibres = %d\n", SB.cantInodosLibres);
    printf("totBloques = %d\n", SB.totBloques);
    printf("totInodos = %d\n\n", SB.totInodos);

    printf("RESERVAMOS UN BLOQUE Y LUEGO LO LIBERAMOS\n");
    int bloque = reservar_bloque();
    if (bread(posSB, &SB) == -1) {
      perror("Error: Lectura superbloque incorrecta.\n");
      bumount();
      exit(EXIT_FAILURE);
    } 
    printf("Se ha reservado el bloque físico nº %d que era el 1º libre indicado por el MB.\n", bloque);
    printf("SB.cantBloquesLibres = ​%d \n", SB.cantBloquesLibres);
    liberar_bloque(bloque);
    if (bread(posSB, &SB) == -1) {
      perror("Error: Lectura superbloque incorrecta.\n");
      bumount();
      exit(EXIT_FAILURE);
    } 
    printf("Liberamos ese bloque y después SB.cantBloquesLibres = %d\n", SB.cantBloquesLibres);

    printf("\nMAPA DE BITS CON BLOQUES DE METADATOS OCUPADOS\n");
    leer_bit(posSB);
    printf("valor del bit correspondiente a posSB (o sea al BF no 0) = 1\n\n");
    leer_bit(SB.posPrimerBloqueMB);
    printf("valor del bit correspondiente a posPrimerBloqueMB (o sea al BF no 1) = 1\n\n");
    leer_bit(SB.posUltimoBloqueMB);
    printf("valor del bit correspondiente a posUltimoBloqueMB (o sea al BF no 13) = 1\n\n");
    leer_bit(SB.posPrimerBloqueAI);
    printf("valor del bit correspondiente a posPrimerBloqueAI (o sea al BF no 14) = 1\n\n");
    leer_bit(SB.posUltimoBloqueAI);
    printf("valor del bit correspondiente a posUltimoBloqueAI (o sea al BF no 3138) = 1\n\n");
    leer_bit(SB.posPrimerBloqueDatos);
    printf("valor del bit correspondiente a posPrimerBloqueDatos (o sea al BF no 3139) = 0\n\n");
    leer_bit(SB.posUltimoBloqueDatos);
    printf("valor del bit correspondiente a posUltimoBloqueDatos (o sea al BF no 99999) = 0\n\n");

    printf("DATOS DEL DIRECTORIO RAIZ\n");
    struct tm *ts;
    char atime[80];
    char mtime[80];
    char ctime[80];
    struct inodo inodoraiz;
    leer_inodo(SB.posInodoRaiz, &inodoraiz);
    printf("tipo: %c \n", inodoraiz.tipo);
    printf("permisos: %d \n", inodoraiz.permisos);
    ts = localtime(&inodoraiz.atime);
    strftime(atime, sizeof(atime), "%a %Y-%m-%d %H:%M:%S", ts);
    ts = localtime(&inodoraiz.mtime);
    strftime(mtime, sizeof(mtime), "%a %Y-%m-%d %H:%M:%S", ts);
    ts = localtime(&inodoraiz.ctime);
    strftime(ctime, sizeof(ctime), "%a %Y-%m-%d %H:%M:%S", ts);
    printf("atime: %s\n", atime);
    printf("mtime: %s\n", mtime);
    printf("ctime: %s\n", ctime);
    printf("nlinks: %d\n", inodoraiz.nlinks);
    printf("tamEnBytesLog: %d\n", inodoraiz.tamEnBytesLog);
    printf("numBloquesOcupados: %d\n", inodoraiz.numBloquesOcupados);

    /**************************************************************************
     *                         DESMONTAMOS DISCO
     *************************************************************************/
    if (bumount() == -1) exit(EXIT_FAILURE);
    exit(EXIT_SUCCESS);
}