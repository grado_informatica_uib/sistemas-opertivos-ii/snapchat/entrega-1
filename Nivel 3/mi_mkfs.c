#include "ficheros_basicos.h"

int main (int argc, char *argv[]){
    if (argc != 3) {
      fprintf(stderr, "Error: Arg.1: Nombre fichero. / Arg.2: Numero bloques.\n");
      exit(EXIT_FAILURE);
    } 
    /**************************************************************************
     *                          MONTAMOS EL DISCO
     *************************************************************************/
    bmount(argv[1]);

    /**************************************************************************
     *                      ASIGNAMOS TAMAÑO AL DISCO
     *************************************************************************/   
    unsigned char buffer[BLOCKSIZE];
    unsigned int nbloques, ninodos, cbloques;

    nbloques = atoi(argv[2]);
    ninodos = nbloques/4;
    memset(buffer,0,BLOCKSIZE);

    for (cbloques = 0; cbloques < nbloques; cbloques++) {
      if (bwrite(cbloques, buffer) == -1) exit(EXIT_FAILURE);
    }

    /**************************************************************************
     *                      INICIALIZAMOS ESTRUCTURAS
     *************************************************************************/
    if (initSB(nbloques, ninodos) == -1) exit(EXIT_FAILURE);
    if (initMB() == -1) exit(EXIT_FAILURE);
    if (initAI() == -1) exit(EXIT_FAILURE);

    // Reservamos un nuevo inodo
    reservar_inodo('d', 7);
    /**************************************************************************
     *                         DESMONTAMOS DISCO
     *************************************************************************/
    if (bumount() == -1) exit(EXIT_FAILURE);
    exit(EXIT_SUCCESS);
}