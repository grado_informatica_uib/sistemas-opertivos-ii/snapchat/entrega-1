/******************************************************************************
*                                  BLOQUES.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÓN: FUNCIONES BÁSICA DE ENTRADA Y SALIDA DE BLOQUES.
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 21 DE FEBRERO DE 2019.
******************************************************************************/


/******************************************************************************
*                           ARCHIVOS AUXILIARES
******************************************************************************/
#include "bloques.h"                    // Librería personalizada.

static int descriptor; 
/******************************************************************************
* Método: bmount()
* Descripción: Este método se utilizará para devolver el descriptor del 
*              fichero pasado por parámetro. 
* Parámetros:  char *camino: puntero al 'path' del fichero. 
* Devuelve:    int:          descriptor de fichero.
******************************************************************************/
int bmount(const char *camino){
    mode_t mode = 0666;                     // Establecemos permisos.
    umask(000);                             // Seteamos la máscara a 0.
    // Abrimos el fichero, y almacenamos el descriptor o código de error
    descriptor = open(camino, O_RDWR | O_CREAT, mode);    
    // Si hay un error.  
    if(descriptor == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error %d: %s\n", errno, strerror(errno));
    }
    // Devolvemos el descriptor o código -1 si ha habido un error.
    return descriptor;
}

/******************************************************************************
* Método: bunmount()
* Descripción: Cierra fichero a partir de su descriptor de fichero.
* Parámetros:  No tiene.
* Devuelve:    0: Si salió bien la operación.
*             -1: Si ha habido un error.
******************************************************************************/
int bumount(){
    // Cerramos el fichero y almacenamos valor devuelvo en a.
    int a =  close(descriptor);
    // Si ha habido algún error
    if(a == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error %d: %s\n", errno, strerror(errno));
    }
    // Devolvemos el código devuelto por close();
    return a;
}


/******************************************************************************
* Método: bwrite()
* Descripción:  Escribe en un bloque pasado por parámetro el buffer.
* Parámetros:   nbloque : Numero de bloque a escribir.
*               *buf    : Puntero a los datos a escribir.
* Devuelve:     Numero de bytes escritos o -1 si ha habido un error.
******************************************************************************/
 int bwrite(unsigned int nbloque, const void *buf){
    off_t desplazamiento = nbloque * BLOCKSIZE;
    lseek(descriptor, desplazamiento, SEEK_SET);
    size_t r = write(descriptor, buf, BLOCKSIZE);
    if(r == -1){
        fprintf(stderr, "Error %d: %s\n", errno, strerror(errno));
    }
    return r;
 }



/******************************************************************************
* Método: bread()
* Descripción:  Lee el bloque indicado por parametro y copia su contenido en 
*               el buffer.
* Parámetros:   nbloque : Numero de bloque a leer.
*               *buf    : Puntero al especio de memoria a alojar lo leido.
* Devuelve:     Numero de bytes leidos o -1 en el caso de error. 
******************************************************************************/
int bread(unsigned int nbloque, void *buf){
    off_t desplazamiento = nbloque * BLOCKSIZE;
    lseek(descriptor, desplazamiento, SEEK_SET);
    size_t r = read(descriptor, buf, BLOCKSIZE);
    if(r == -1){
        fprintf(stderr, "Error %d: %s\n", errno, strerror(errno));
    }
    return r;
}