/******************************************************************************
*                                LEER_SF.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERACTIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÓN:
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 22 DE MARZO DE 2019.
******************************************************************************/
#include "ficheros_basico.h"
int main (int argc, char *argv[]){
    if (argc != 2) {
      fprintf(stderr, "Error: Arg.1: Nombre fichero.\n");
      exit(EXIT_FAILURE);
    } 
    /**************************************************************************
     *                          MONTAMOS EL DISCO
     *************************************************************************/
    bmount(argv[1]);


    /**************************************************************************
     *                               PRUEBAS
     *************************************************************************/ 
    struct superbloque SB;

    if (bread(posSB, &SB) == -1) {
      perror("Error: Lectura superbloque incorrecta.\n");
      bumount();
      exit(EXIT_FAILURE);
    } 

    printf("DATOS DEL SUPERBLOQUE:\n");
    printf("posPrimerBloqueMB = %d\n", SB.posPrimerBloqueMB);
    printf("posUltimoBloqueMB = %d\n", SB.posUltimoBloqueMB);
    printf("posPrimerBloqueAI = %d\n", SB.posPrimerBloqueAI);
    printf("posUltimoBloqueAI = %d\n", SB.posUltimoBloqueAI);
    printf("posPrimerBloqueDatos = %d\n", SB.posPrimerBloqueDatos);
    printf("posUltimoBloqueDatos = %d\n", SB.posUltimoBloqueDatos);
    printf("posInodoRaiz = %d\n", SB.posInodoRaiz);
    printf("posPrimerInodoLibre = %d\n", SB.posPrimerInodoLibre);
    printf("cantBloquesLibres = %d\n", SB.cantBloquesLibres);
    printf("canInodosLibres = %d\n", SB.cantInodosLibres);
    printf("totBloques = %d\n", SB.totBloques);
    printf("totInodos = %d\n\n", SB.totInodos);

    printf("INODO 1. TRADUCCION DE LOS BLOQUES LOGICOS 8, 204, 30.004, 400.004 y 16.843.019\n");

    int ninodo = reservar_inodo('f',6);

    traducir_bloque_inodo(ninodo,8,1);
    traducir_bloque_inodo(ninodo,204,1);
    traducir_bloque_inodo(ninodo,30004,1);
    traducir_bloque_inodo(ninodo,400004,1);
    traducir_bloque_inodo(ninodo,16843019,1);

    printf("\nDATOS DEL INODO RESERVADO 1\n");
    struct tm *ts;
    char atime[80];
    char mtime[80];
    char ctime[80];
    struct inodo inodo;
    leer_inodo(ninodo, &inodo);
    printf("tipo: %c \n", inodo.tipo);
    printf("permisos: %d \n", inodo.permisos);
    ts = localtime(&inodo.atime);
    strftime(atime, sizeof(atime), "%a %Y-%m-%d %H:%M:%S", ts);
    ts = localtime(&inodo.mtime);
    strftime(mtime, sizeof(mtime), "%a %Y-%m-%d %H:%M:%S", ts);
    ts = localtime(&inodo.ctime);
    strftime(ctime, sizeof(ctime), "%a %Y-%m-%d %H:%M:%S", ts);
    printf("atime: %s\n", atime);
    printf("mtime: %s\n", mtime);
    printf("ctime: %s\n", ctime);
    printf("nlinks: %d\n", inodo.nlinks);
    printf("tamEnBytesLog: %d\n", inodo.tamEnBytesLog);
    printf("numBloquesOcupados: %d\n", inodo.numBloquesOcupados);

    bread(posSB, &SB);
    printf("SB.posPrimerInodoLibre: %d\n", SB.posPrimerInodoLibre);

    printf("\nLIBERAMOS EL INODO RESERVADO EN EL NIVEL ANTERIOR Y TODOS SUS BLOQUES\n");
    liberar_inodo(ninodo);


    bread(posSB, &SB);
    printf("SB.posPrimerInodoLibre: %d\n", SB.posPrimerInodoLibre);
    /**************************************************************************
     *                         DESMONTAMOS DISCO
     *************************************************************************/
    if (bumount() == -1) exit(EXIT_FAILURE);
    exit(EXIT_SUCCESS);
}