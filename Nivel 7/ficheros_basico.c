/******************************************************************************
*                            FICHEROS_BASICOS.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERACTIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÓN:
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 22 DE MARZO DE 2019.
******************************************************************************/


/******************************************************************************
*                           ARCHIVOS AUXILIARES
******************************************************************************/
#include "ficheros_basico.h"                    // Librería personalizada.

/******************************************************************************
* Método: tamMb()
* Descripción:  Calcula el número de bloques necesarios para alojar el 
*               mapa de bits.
* Parámetros:   nbloques : numero de bloques del disco.
* Devuelve:     tamMB    : número de bloques del MB.
******************************************************************************/
int tamMB(unsigned int nbloques){
    int tamMB = (nbloques/8)/BLOCKSIZE; // Calcula tamaño MB (parte entera)
    // Si necesita 1 bloque más para la parte decimal
    if((nbloques/8)%BLOCKSIZE > 0){     
        tamMB++;                        // Añadimos un bloque (parte decimal)
    }
    // Devolvemos el tamaño del Mapa de Bits
    return tamMB;
}

/******************************************************************************
* Método: tamAI()
* Descripción:  Calcula el numero de bloques necesarios para alojar el Array 
*               de inodos. 
* Parámetros:   ninodos : Número de inodos del sistema.
* Devuelve:     tamAI   : Número de bloques para alojar el Array de inodos. 
******************************************************************************/
int tamAI(unsigned int ninodos){
    int tamAI = ninodos*INODOSIZE/BLOCKSIZE;// Calcula tamaño AI (p. entera)
    // Si necesita 1 bloque más para la parte decimal
    if((ninodos*INODOSIZE)%BLOCKSIZE > 0){
        tamAI++;                            // Añadimos un bloque (p. decimal)
    }
    // Devolvemos el tamaño del Array de Inodos.
    return tamAI;
}

/******************************************************************************
* Método: initSB()
* Descripción:  Inicializa el superbloque con los datos apropiados.
* Parámetros:   nbloques : Número de bloques del disco.
*               inodos   : Número de inodos del disco.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initSB(unsigned int nbloques, unsigned int ninodos){
    // Declaración de nuestro superbloque
    struct superbloque SB;              
    // Número del primer bloque del Mapa de Bits.
    SB.posPrimerBloqueMB = posSB + tamSB;
    // Número del último bloque del Mapa de Bits.
    int var_tamMB = tamMB(nbloques)-1;
    SB.posUltimoBloqueMB = SB.posPrimerBloqueMB + var_tamMB;
    // Número del primer boqque del Array de Inodos.
    SB.posPrimerBloqueAI = SB.posUltimoBloqueMB + 1;
    // Número del último bloque del Array de Inodos.
    int var_tamAI = tamAI(ninodos)-1;
    SB.posUltimoBloqueAI = SB.posPrimerBloqueAI + var_tamAI;
    // Numero del primer bloque de datos.
    SB.posPrimerBloqueDatos = SB.posUltimoBloqueAI + 1;
    // Número del último bloque de datos.
    SB.posUltimoBloqueDatos = nbloques - 1;
    // Posición inodo raíz.
    SB.posInodoRaiz = 0;
    // Posición del primer inodo libre.
    SB.posPrimerInodoLibre = 0; // ESTO PARECE QUE SE HA DE CAMBIAR AL FINAL.
    // Número de bloques libres
    SB.cantBloquesLibres = nbloques;
    // Número de inodos libres
    SB.cantInodosLibres = ninodos;
    // Cantidad total de bloques
    SB.totBloques = nbloques;
    // Cantidad total de inodos
    SB.totInodos = ninodos;
    // Escribimos los datos en la memoria superbloque.
    int r = bwrite(posSB, &SB);
    // Comprobamos si ha habido algún error.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Escritura SuperBloque incorrecta.\n");
        return r;
    }
    return 0;
}

/******************************************************************************
* Método: initMB()
* Descripción:  Inicializa los valores del Mapa de Bits a 0. 
* Parámetros:   Ninguno.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initMB(){
    // Valor que vamos a escribir en el MB al inicializarlo.
    unsigned char initMBValue [BLOCKSIZE];
    memset(initMBValue,0,BLOCKSIZE);
    // Declaración Super Bloque.
    struct superbloque SB;

    // Leemos el superbloque y lo guardamos en SB
    int r = bread(posSB, &SB);
    // Si se ha producido un error.cd
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura Mapa de Bits incorrecta.\n");
        return r;
    }

    // Recorremos el Mapa de Bits para inicializar sus valores.
    for(int i = SB.posPrimerBloqueMB; i <= SB.posUltimoBloqueMB; i++)
    {
        // Escribimos el initMBValue en el MB.
        int r = bwrite(i,initMBValue);
        // Si se ha producido un error
        if(r == -1){
            // Imprimimos el error por la salida estándar de errores.
            fprintf(stderr, "Error: Escritura Mapa de Bits incorrecta.\n");
            return r;
        }
    }
    // Actualizamos el MB con los datos ya ocupados (SB, )
    for(int i = posSB; i <= SB.posUltimoBloqueAI; i++){
        escribir_bit(i,1);          // bloque ocupado
        SB.cantBloquesLibres--;     // Restamos uno a los bloques libres.
    }
    r = bwrite(posSB, &SB);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Escritura actualizacion Superbloque incorrecta.\n");
        return r;
    }
    // Si hemos llegado hasta aquí todo ha ido bien: Devolvemos 0;
    return 0;
}

/******************************************************************************
* Método: initAI()
* Descripción:  Inicializa el Array de Inodos. Al principio todos los inodos 
*               están libres y por tanto enlazados.
* Parámetros:   Ninguno.
* Devuelve:     0 : Ha funcionado correctamente.
*              -1 : Se ha producido un error.
******************************************************************************/
int initAI(){
    // Declaración de array de inodos par luego escribirlos.
    struct inodo inodos[BLOCKSIZE/INODOSIZE];
    // Declaración del superbloque para coger datos.
    struct superbloque SB;
    // Leemos el superbloque y lo almacenamos en SB
    int r = bread(posSB, &SB);
    // Si ha habido algún error.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura SuperBloque incorrecta.\n");
        return r;
    }
    // Contador de Inodo para hacer el enlace entre Inodos.
    int contInodos = SB.posPrimerInodoLibre + 1;
    // Recorremos los bloques que contienen el Array de Inodos.
    for(int i = SB.posPrimerBloqueAI; i <= SB.posUltimoBloqueAI;i++)
    {        
        // Recorremos cada Inodo dentro del bloque
        for(int j = 0; j < BLOCKSIZE / INODOSIZE ; j++)
        {
            // Asignamos el tipo 'libre'
           inodos[j].tipo = 'l'; // libre
           // Si no hemos llegado al último inodo
           if(contInodos < SB.totInodos){
                // Lo enlazamos con el siguiente.
			    inodos[j].punterosDirectos[0] = contInodos;
				contInodos++;
           }
           else {
                // El último elemento no se puede enlazar con nada.
				inodos[j].punterosDirectos[0] = UINT_MAX;
                j = BLOCKSIZE/INODOSIZE;
           }
        }
        // Escribimos el grupo de inodos en el Array de Inodos,
        r = bwrite(i, inodos);
        // SI ha habido algun error.
        if (r == -1) {
            // Imprimimos el error por la salida estándar de errores.
            fprintf(stderr, "Error: Escritura Inodos del bloque incorrecta.\n");
            return r;     
        }  
    }
    // Si hemos llegado hasta aquí es que ha ido todo correctamente.
    return 0;
}


/******************************************************************************
* Método: escribir_bit()
* Descripción:  Escribe el valor indicado por elparámetro bit (0 ó 1) en un 
*               determinado bit del MP que representa el bloque nbloque.
* Parámetros:   unsigned int nbloque :  Número de bloque.  
*               unsigned int bit     :  Bit a escribir.
* Devuelve:     0   : Ha funcionado correctamente.
*              -1   : Ha habido algún problema.
******************************************************************************/
int escribir_bit(unsigned int nbloque, unsigned int bit){
    if ((bit!=0) && (bit!=1)) {
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: el bit a escribir debe ser 0 ó 1.\n");
        return -1; 
    }
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    int posbyte = nbloque / 8;              // Calculamos el posbyte
    int posbit = nbloque % 8;               // Calculamos el posbit
    int nbloqueMB = posbyte / BLOCKSIZE;    // Calculamos el bloqueMB
    int nbloqueabs = nbloqueMB + SB.posPrimerBloqueMB;  // Bloque absoluto
    unsigned char bufferMB[BLOCKSIZE];      // Buffer de MB
    r = bread(nbloqueabs, &bufferMB);       // Leemos el buffer del disco.
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del bloque MB incorrecta.\n");
        return r;    
    }
    posbyte = posbyte % BLOCKSIZE; // Calculo byte implicado en el bloqueMB.
    unsigned char mascara = 128;   // Máscara para setear el valor del bit.
    mascara >>= posbit;            // Desplazamos el bit conforme posbit.
    // Si el bit es 0 (libre)
    if(bit == 0){
        bufferMB[posbyte] &= ~mascara;  // Actualizamos bufferMB[posbyte]
    }
    // Si el bit es 1 (ocupado)
    else{
        bufferMB[posbyte] |= mascara;   // Actualizamos bufferMB[posbyte]
    }
    r = bwrite(nbloqueabs, bufferMB);   // Escribimos el bloque en disco.
    // Si ha habido algún problema en la escritura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Escrita del bloque MB incorrecta.\n");
        return r;    
    }
    // Si hemos llegado hasta aquí significa que todo ha ido bien y return 0.
    return 0;
}

/******************************************************************************
* Método: leer_bit()
* Descripción:  Lee un determinado bit del MB y devuelve el valor del bit 
*               leído.
* Parámetros:   nbloque  : Número de bloque que queremos leer.
* Devuelve:     El valor del bit leído.
*               -1 si ha habido algun problema.
******************************************************************************/
unsigned char leer_bit(unsigned int nbloque){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    int posbyte = nbloque / 8;              // Calculamos el posbyte
    int posbit = nbloque % 8;               // Calculamos el posbit
    int nbloqueMB = posbyte / BLOCKSIZE;    // Calculamos el bloqueMB
    int nbloqueabs = nbloqueMB + SB.posPrimerBloqueMB;  // Bloque absoluto
    unsigned char bufferMB[BLOCKSIZE];      // Buffer de MB
    r = bread(nbloqueabs, &bufferMB);       // Leemos el buffer del disco.
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del bloque MB incorrecta.\n");
        return r;    
    }
    posbyte = posbyte % BLOCKSIZE; // Calculo byte implicado en el bloqueMB.
    unsigned char mascara = 128;   // Máscara para setear el valor del bit.
    mascara >>= posbit;            // Desplazamos el bit conforme posbit.
    mascara &= bufferMB[posbyte];  // Obtenemos en la mascara el valor del byte
    mascara >>= (7-posbit);        // Desplezamos a la derecha
    printf("[leer_bit()→ nbloque: %d, posbyte:%d, posbit:%d, nbloqueMB:%d, nbloqueabs:%d)]\n", nbloque, posbyte, posbit, nbloqueMB, nbloqueabs );
    return mascara;
}

/******************************************************************************
* Método: reservar_bloque()
* Descripción:  Encuentra el primer bloque libre, consultando el MB, lo ocupa 
*               y devuelve su posición.
* Parámetros:   Ninguno
* Devuelve:     Posición del primer bloque libre.
*              -1 : si ha habido algún problema.
******************************************************************************/
int reservar_bloque(){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Si hay bloques libres.
    if(SB.cantBloquesLibres == 0){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: No hay bloques libres disponibles.\n");
        return -1;       
    }
    /**********************************************************
     *  Encontramos un bloque MB que tiene un 0.
     **********************************************************/
    unsigned char bufferMB[BLOCKSIZE];      // Buffer de MB
    unsigned char bufferAux[BLOCKSIZE];     // Buffer auxiliar (comparar)
    memset(bufferAux, 255, BLOCKSIZE);      // Set buffer 1111 1111
    int posBloqueMB = SB.posPrimerBloqueMB;          // Posicion bloque MB con 0
    int encontrado = 0;       // Control de bloque encontrado
    // Recorrido de MB hasta encontrar bloque que tenga algún 0.
    while(posBloqueMB <= SB.posUltimoBloqueMB && encontrado != 1){
            bread(posBloqueMB, bufferMB);
            if(memcmp(bufferAux, bufferMB, BLOCKSIZE) > 0){
                encontrado = 1;        // Lo encontramos
            }
            else {
                posBloqueMB++;
            }
    }
    /**********************************************************
     *  Encontramos el byte MB que tiene un 0.
     **********************************************************/
    int byteEncontrado = 0;    // Control de byte encontrado
    int posbyte = 0;               // Posición byte encontrado.
    // Recorremos el bloque en busca de byte con algún 0.
    while(posbyte <= BLOCKSIZE && byteEncontrado != 1){
        // Si encontramos un byte con 0.
        int i = 255 - bufferMB[posbyte];
        if(i> 0) byteEncontrado = 1;         // Lo encontramos
        else posbyte++;
    }
    /**********************************************************
     *  Encontramos el bit MB que vale 0.
     **********************************************************/
    unsigned char mascara = 128;        // 1000 0000
    int posbit = 0;                     // Posición del bit 0.
    // Mientras no encontremos el bit con 0.
    while((bufferMB[posbyte] & mascara) != 0){
        posbit++;
        bufferMB[posbyte] <<= 1; //desplazamiento izquierda.
    }
    // Calculamos el número de bloque resultando.
    int nbloque = ((posBloqueMB - SB.posPrimerBloqueMB)*BLOCKSIZE + posbyte)*8 + posbit;
    // Escribimos en el MB que el bloque nbloque estará ocupado.
    r = escribir_bit(nbloque, 1);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Fallo en la reserva del bloque.\n");
        return -1;  
    }
    SB.cantBloquesLibres--;        // Actualizamos los bloques libres en SB.
    memset(bufferAux, 0, BLOCKSIZE);      // Set buffer 0000 0000
    r = bwrite(nbloque, &bufferAux);    // Reseteamos los valores del bloque a 0
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: No se pudo resetear valores bloque.\n");
        return -1;  
    }
    r = bwrite(posSB, &SB);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: No se pudo actualizar superbloque.\n");
        return -1;  
    }
    // Si hemos llegado aquí todo ha ido bien, devolvemos número de bloque.
    return nbloque;
}

/******************************************************************************
* Método: liberar_bloque()
* Descripción:  Libera un bloque determinado.
* Parámetros:   nbloque : Número del bloque de se desea liberar.
* Devuelve:     nbloque : Si ha funcionado correctamente.
*              -1       : Si ha habido algún error.
******************************************************************************/
int liberar_bloque(unsigned int nbloque){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Actualizamos el valor del bloque en MB
    r = escribir_bit(nbloque, 0);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: No se pudo liberar el bloque.\n");
        return -1;  
    }
    // Aumentos el número de bloques libres.
    SB.cantBloquesLibres++;
    // Actualizamos el superbloque en el disco virtual
    r = bwrite(posSB, &SB);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: No se ha podido actualizarel superbloque.\n");
        return r; 
    }
    // Devolvemos el número de bloque liberado.
    return nbloque;
}

/******************************************************************************
* Método: escribir_inodo()
* Descripción:  Escribe el contenido de una variable de tipo struct inodo en 
*               un determinado inodo del array de inodos.
* Parámetros:   ninodo  : Número del inodo donde se quiere escribir.
*               inodo   : Inodo a escribir.
* Devuelve:     0  : Si ha funcionado correctamente.
*              -1  : Si ha habido algún error.
******************************************************************************/
int escribir_inodo(unsigned int ninodo, struct inodo inodo){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Calculamos el bloque del inodo que queremos modificar.
    int posbloqueinodo = (ninodo/(BLOCKSIZE/INODOSIZE)) + SB.posPrimerBloqueAI;
    // Buffer que utilizaremos para modificar el array de inodos. 
    struct inodo inodos[BLOCKSIZE/INODOSIZE];
    // Leemos el bloque del disco
    r = bread(posbloqueinodo, &inodos);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del Array de Inodos incorrecta.\n");
        return r;    
    }
    // Calculamos la posición del inodo dentro del bloque 
    int posInodo = ninodo%(BLOCKSIZE/INODOSIZE);
    // Modificamos el inodo dentro del buffer.
    inodos[posInodo] = inodo;
    // Escribimos el buffer en el disco.
    r = bwrite(posbloqueinodo, &inodos);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Escritura del Array de Inodos incorrecta.\n");
        return r;    
    }
    // Si hemos llegado hasta aquí significa que todo ha ido bien.
    return 0;
}

/******************************************************************************
* Método: leer_inodo()
* Descripción:  Lee un determinado inodo del array de inodos para volcarlo en 
*               una variable tipo struct inodo pasada por referencia.
* Parámetros:   ninodo : Número de inodo que se quiere leer.
*               *inodo : Puntero a un inodo donde se guardarán los datos leídos
* Devuelve:     0  : Si ha funcionado correctamente.
*              -1  : Si ha habido algún error.
******************************************************************************/
int leer_inodo(unsigned int ninodo, struct inodo *inodo){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Calculamos el bloque del inodo que queremos modificar.
    int posbloqueinodo = (ninodo/(BLOCKSIZE/INODOSIZE)) + SB.posPrimerBloqueAI;
    // Buffer que utilizaremos para modificar el array de inodos. 
    struct inodo inodos[BLOCKSIZE/INODOSIZE];
    // Leemos el bloque del disco
    r = bread(posbloqueinodo, &inodos);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del Array de Inodos incorrecta.\n");
        return r;    
    }
    // Calculamos la posición del inodo dentro del bloque 
    int posInodo = ninodo%(BLOCKSIZE/INODOSIZE);
    // Devolvemos el inodo modificando el parámetro*inodo
    *inodo =  inodos[posInodo];
    // Si hemos llegado hasta aquí es que todo ha ido bien.
    return 0;
}

/******************************************************************************
* Método: reservar_inodo()
* Descripción:  Encuentra el primer inodo libre, lo reserva, devuelve su 
*               número y actualiza la lista enlazada de inodos libres.
* Parámetros:   tipo     : (directorio | fichero | libre)
*               permisos : Permisos que se le atribuyen (0-7)
* Devuelve:     Número de inodo reservado.
*               -1 : Si ha habido algún problema.
******************************************************************************/
int reservar_inodo(unsigned char tipo, unsigned char permisos){
    struct superbloque SB;         // Declaramos el superbloque.
    int r = bread(posSB, &SB);     // Leemos el superbloque desde el disco
    // Si ha habido algún problema en la lectura.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura del superbloque incorrecta.\n");
        return r;    
    }
    // Si hay bloques libres.
    if(SB.cantInodosLibres == 0){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: No hay inodos libres disponibles.\n");
        return -1;       
    }
    // Posición del inodo que vamos a reservar.
    int posInodoReservado = SB.posPrimerInodoLibre;
    // Declaramos una estructura inodo.
    struct inodo inodo;
    // Leermos el inodo
    leer_inodo(posInodoReservado, &inodo);
    // Hacemos apuntar al superbloque al siguiente inodo libre.
    SB.posPrimerInodoLibre = inodo.punterosDirectos[0];
    // Inicialiamos el inodo.
    inodo.tipo = tipo;
    inodo.permisos = permisos;
    inodo.nlinks = 1;
    inodo.tamEnBytesLog = 0;
    inodo.atime = time(NULL);
    inodo.mtime = time(NULL);
    inodo.ctime = time(NULL);
    inodo.numBloquesOcupados = 0;
    for(int i = 0; i < (int) (sizeof(inodo.punterosDirectos)/sizeof(int)); i++){
       inodo.punterosDirectos[i] = 0;
    }
    for(int i = 0; i < (int) (sizeof(inodo.punterosIndirectos)/sizeof(int)); i++){
       inodo.punterosIndirectos[i] = 0;
    }
    // Escribimos el inodo
    r = escribir_inodo(posInodoReservado, inodo);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Escritura del inodo incorrecta.\n");
        return r;    
    }
    // Actualizamos la cantidad inodos libres.
    SB.cantInodosLibres--;
    // Escribimos el SB en el disco.
    r = bwrite(posSB, &SB);
    // Si ha habido algún error.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Escritura en superbloque incorrecta.\n");
        return r;    
    }
    // Si hemos llegado aquí todo ha ido bien.
    return posInodoReservado;
}

/******************************************************************************
* Método: obtener_nrangoBL()
* Descripción: Obtiene el rando de punteros en el que se sitúa el bloque lógico 
*              que buscamos y la dirección almaenada en el puntero 
*              correspondiente del inodo.
* Parámetros:  inodo    : inodo en el cual buscaremos en el bloque.
*              nblogico : número de bloque lógico.
*              *ptr     : puntero hacia atributo inodo correspondiente.
* Devuelve:    0    : Si pertenece a bloque directo.
*              1    : Si pertenece a bloque indirecto 1
*              2    : Si pertenece a bloque indirecto 2
*              3    : Si pertenece a bloque indirecto 3
*             -1    : Si ha habido algún error.
******************************************************************************/
int obtener_nrangoBL(struct inodo inodo, unsigned int nblogico, unsigned int  *ptr){
    // Comprobamos si el bloque lógico está fuera de rango
    if(nblogico < 0 || nblogico >= INDIRECTOS2){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: número de bloque lógico fuera de rango.\n");
        return -1; 
    }
    // Si pertenece a los punteros directos
    if(nblogico < DIRECTOS){
        *ptr=inodo.punterosDirectos[nblogico];    
        return 0;
    }
    // Si pertenece a los punteros indirectos 1
    if(nblogico < INDIRECTOS0){
        *ptr=inodo.punterosIndirectos[0];     
        return 1;
    }
    // Si pertenecen a los punteros indirectos 2
    if(nblogico < INDIRECTOS1){
        *ptr=inodo.punterosIndirectos[1];     
        return 2;
    }
    // Si pertenecen a los punteros indirectos 3
    if(nblogico < INDIRECTOS2){
        *ptr=inodo.punterosIndirectos[2];     
        return 3;
    }
    // Si por lo que se ha habido un error.
    fprintf(stderr, "Error: error inesperado en obtener_nrangoBL().\n");
    return -1; 
}

/******************************************************************************
* Método: obtener_indice()
* Descripción: A partir de un numero de bloque lógico y un nivel de punteros, 
*              obtiene el el índice que almacena el bloque físico de los datos. 
* Parámetros:  nblogico       : número de bloque lógico.
*              nivel_punteros : Determina qué nivel hemos solicitado.
* Devuelve:    ïndice solicitado
*              -1 si ha habido algún error. 
******************************************************************************/
int obtener_indice(int nblogico, int nivel_punteros){
    // Comprobamos si el bloque lógico está fuera de rango
    if(nblogico < 0 || nblogico >= INDIRECTOS2){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: número de bloque lógico fuera de rango.\n");
        return -1; 
    }
    int indice;
    // Si pertenece a los punteros directos
    if(nblogico < DIRECTOS){
        indice = nblogico;    
        return indice;  
    }
    else {
        // Si pertenece a los punteros indirectos0
        if(nblogico < INDIRECTOS0){
            indice = nblogico - DIRECTOS;
            return indice;
        }
        else{
            // Si pertenece a los punterior indirectos1
            if(nblogico < INDIRECTOS1){
                // Nivel 2
                if(nivel_punteros == 2){
                    indice = (nblogico - INDIRECTOS0) / NPUNTEROS;
                    return indice;
                }
                // Nivel 1
                if(nivel_punteros == 1){
                    indice = (nblogico - INDIRECTOS0) % NPUNTEROS;
                    return indice;
                }
            }
            else {
                // Si pertenece a los punteros indirectos2
                if(nblogico < INDIRECTOS2){
                    // Nivel 3
                    if(nivel_punteros == 3){
                        indice = (nblogico - INDIRECTOS1)/(NPUNTEROS*NPUNTEROS);
                        return indice;
                    }
                    // Nivel 2
                    if(nivel_punteros == 2){
                        indice = ((nblogico-INDIRECTOS1)%(NPUNTEROS*NPUNTEROS))/NPUNTEROS;
                        return indice;
                    }
                    // Nivel 1
                    if(nivel_punteros == 1){
                        indice = ((nblogico-INDIRECTOS1)%(NPUNTEROS*NPUNTEROS))%NPUNTEROS;
                        return indice;
                    }
                }
            }
        }
    }
    // Si llegamos hasta aquí ha habido un error
    return -1;
}

/******************************************************************************
* Método: traducir_bloque_inodo()
* Descripción:  Se encarga de obtener el número de bloque físico correspondiente 
*               a un bloque lógico determinado del inodo indicado.
* Parámetros:   ninodo    : Número de inodo que queremos traducir.
*               nblogico  : Número de bloque lógico.
*               reservvar : 0 (consulta) | 1 (escritura)
* Devuelve:     Número de bloque físico.
******************************************************************************/
int traducir_bloque_inodo(unsigned int ninodo, unsigned int nblogico, char reservar){
    struct inodo inodo;     // Estructura auxiliar inodo
    unsigned int ptr, ptr_ant, salvar_inodo, nRangoBL, nivel_punteros, indice;
    int buffer[NPUNTEROS];
    // Leemos el inodo correspondiente y lo guardamos en inodo.
    int r = leer_inodo (ninodo, &inodo);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Lectura de inodo incorrecta.\n");
        return r;    
    }
    // Inicializamos variables.
    ptr = 0;
    ptr_ant = 0;
    salvar_inodo = 0;
    indice = 0;
    nRangoBL = obtener_nrangoBL(inodo, nblogico, &ptr);
    if(nRangoBL == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error: Error al obtener el rangoBL.\n");
        return r;    
    }
    nivel_punteros = nRangoBL;
    // Nos pasamos por todos los niveles posibles.
    for(int i = nivel_punteros; i > 0; i--){
        if(ptr == 0){
            if(reservar == 0){
                // Imprimimos el error por la salida estándar de errores.
                //fprintf(stderr, "Error: lectura bloque inexistente.\n");
                return -1; 
            }
            else {
                salvar_inodo = 1;
                ptr = reservar_bloque();
                if(ptr == -1){
                    // Imprimimos el error por la salida estándar de errores.
                    fprintf(stderr, "Error: No se ha podido reservar el bloque..\n");
                    return -1;    
                }
                inodo.numBloquesOcupados++;
                inodo.ctime = time(NULL);
                if(nivel_punteros == nRangoBL){
                    inodo.punterosIndirectos[nRangoBL-1] = ptr;
                    printf("[traducir_bloque_inodo()→ inodo.punterosIndirectos[%d] = %d (reservado BF %d para punteros_nivel%d)]\n", nRangoBL-1, ptr, ptr, nivel_punteros);
                }
                else {
                    buffer[indice] = ptr; // (imprimirlo)
                    printf("[traducir_bloque_inodo()→ punteros_nivel%d [%d] = %d (reservado BF %d para punteros_nivel%d)]\n",nivel_punteros+1, indice, ptr, ptr, nivel_punteros );
                    r = bwrite(ptr_ant, buffer);
                    if(r == -1){
                         // Imprimimos el error por la salida estándar de errores.
                        fprintf(stderr, "Error: No se ha podido escrbir el bloque.\n");
                        return -1; 
                    }
                }
            }
        }
        r = bread(ptr, buffer);
        if(r == -1){
            // Imprimimos el error por la salida estándar de errores.
            fprintf(stderr, "Error: No se ha podido leer el bloque.\n");
            return -1; 
        }
        indice = obtener_indice(nblogico, nivel_punteros);
        if(indice == -1){
            // Imprimimos el error por la salida estándar de errores.
            fprintf(stderr, "Error: Ha habido algún problema al obetener el indice.\n");
            return -1; 
        }
        ptr_ant = ptr; //guardamos el puntero
        ptr = buffer[indice]; // y lo desplazamos al siguiente nivel
        nivel_punteros--;
    }
    
    if(ptr == 0){
        if(reservar == 0){
            // Imprimimos el error por la salida estándar de errores.
            //fprintf(stderr, "Error: lectura bloque inexistente.\n");
            return -1; 
        }
        else {
            salvar_inodo = 1;
            ptr = reservar_bloque();
            if(ptr == -1){
                // Imprimimos el error por la salida estándar de errores.
                fprintf(stderr, "Error: No se ha podido reservar el bloque..\n");
                return -1;    
            }
            inodo.numBloquesOcupados++;
            inodo.ctime = time(NULL);
            if(nRangoBL == 0){
                inodo.punterosDirectos[nblogico] = ptr;
                printf("[traducir_bloque_inodo()→ inodo.punterosDirectos[%d] = %d (reservado BF %d para BL %d)]\n", nblogico, ptr, ptr, nblogico);
            }
            else {
                buffer[indice] = ptr;
                r = bwrite(ptr_ant, buffer);
                if(r == -1){
                    // Imprimimos el error por la salida estándar de errores.
                    fprintf(stderr, "Error: No se ha podido escrbir el bloque.\n");
                    return -1; 
                }
               printf("[traducir_bloque_inodo()→ punteros_nivel%d [%d] = %d (reservado BF %d para BL %d)]\n", nivel_punteros+1, indice, ptr , ptr, nblogico);
            }
        }
    }
    if(salvar_inodo == 1){
        r = escribir_inodo(ninodo, inodo);
        if(r == -1){
            // Imprimimos el error por la salida estándar de errores.
            fprintf(stderr, "Error: No se ha podido escribir el inodo.\n");
            return -1; 
        }
    }
    // Devolvemos el índice.
    return ptr;
}

/******************************************************************************
* Método: liberar_inodo()
* Descripción:  Libera el inodo especificado por parámetro.
* Parámetros:   ninodo : Número de inodo a liberar.
* Devuelve:     Número de inodo liberado.
*               -1 si ha habido algún error.
******************************************************************************/
int liberar_inodo(unsigned int ninodo){
    struct inodo inodo;                                 // Estructura Inodo
    int liberados = liberar_bloques_inodo(ninodo, 0);   // Liberamos inodo.
    int r = leer_inodo(ninodo, &inodo);                 // Leemos inodo.
    // Si ha habido algún error.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error (liberar_inodo()): No se ha podido leer el inodo.\n");
        return -1; 
    }
    inodo.numBloquesOcupados =- liberados;    // Actualizamos num bloques ocupados.
    inodo.tipo = 'l';                         // Tipo libre.
    struct superbloque SB;                    // Estructura superbloque.
    r = bread(posSB, &SB);                        // Leemos superbloque
    // Si ha habido algún error.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error (liberar_inodo()): No se ha podido leer el Superbloque.\n");
        return -1; 
    }
    inodo.punterosDirectos[0] = SB.posPrimerInodoLibre; //Inodo apunta al último libre.  
    SB.posPrimerInodoLibre = ninodo;                    //SB apunta al inodo.
    SB.cantInodosLibres++;                              //Aumentamos num inodos libres.
    r = bwrite(posSB, &SB);                             // Escribimos SB en disco
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error (liberar_inodo()): No se ha podido escribir el Superbloque.\n");
        return -1; 
    }
    r = escribir_inodo(ninodo, inodo);
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error (liberar_inodo()): No se ha podido escribir el inodo.\n");
        return -1; 
    }
    // Devolvemos el número de inodo liberado.
    return ninodo;
}

/******************************************************************************
* Método: liberar_bloques_inodo()
* Descripción:  Libera todos los bloques ocupados a partir del bloque lógico 
*               indicado por el argumento nblogico (inclusive)
* Parámetros:   ninodo   : Número de inodo afectado.
*               nblogico : Número de bloque lógico al partir del cual se 
*                          comenzará a liberar.
* Devuelve:     Cantidad de bloques liberados.
******************************************************************************/
int liberar_bloques_inodo(unsigned int ninodo, unsigned int nblogico){
    struct inodo inodo;                             // Estructura inodo.
    int r = leer_inodo(ninodo, &inodo);             // Leemos inodo.
    if(r == -1){
        // Imprimimos el error por la salida estándar de errores.
        fprintf(stderr, "Error (liberar_bloques_inodo()): No se ha podido leer el inodo.\n");
        return -1; 
    }
    int nblogico_ultimo;                            // Ultimo num bloque lógico.
    int nbloquesLiberados = 0;                      // Num bloques liberados.
    int nRangoBL;                                   // Rango de Bloque lógico
    int nivel_punteros;                             // Nivel de punteros indirectos
    unsigned int bloques_punteros[3][NPUNTEROS];    // Array auxiliar bloques punteros
    unsigned char bufferAux[BLOCKSIZE];             // Buffer auxiliar blocksize
    int indice;                                     // Indice 
    int ptr_nivel[3];                               // Puntero nivel
    int indices[3];                                 // indices
    int nblog;                                      // num bloque logico 
    int salvar_inodo;                               // salvar inodo.
    unsigned int ptr = 0;                           // Puntero
    // Si el fichero no tiene bloques asignados, está vacío y ya hemos terminado.
    if(inodo.tamEnBytesLog == 0){
        return 0; // fichero vacío.
    }
    memset(bufferAux,0,BLOCKSIZE);                  // Asignamos valor a bufferAux
    // Obtener últino número de bloque lógico ocupado.
    if((inodo.tamEnBytesLog % BLOCKSIZE) == 0){
        nblogico_ultimo = inodo.tamEnBytesLog / BLOCKSIZE -1;}
    else {
        nblogico_ultimo = inodo.tamEnBytesLog / BLOCKSIZE;}
    //nblogico_ultimo = 16843019;
    printf("[liberar_bloques_inodo()→ primerBL: %d ultimoBL %d]\n", nblogico, nblogico_ultimo);
    // Recorremos los bloques lógicos para liberarlos.
    for(nblog = nblogico; nblog <= nblogico_ultimo; nblog++){
        nRangoBL = obtener_nrangoBL(inodo, nblog, &ptr);
        if(nRangoBL < 0){
            fprintf(stderr, "Error (liberar_bloques_inodo()): No se ha podido obtener el rangoBL.\n");
            return -1;
        }
        nivel_punteros = nRangoBL;
        while(ptr > 0 && nivel_punteros > 0){        
            r = bread(ptr, bloques_punteros[nivel_punteros-1]);
            if(r == -1){
                // Imprimimos el error por la salida estándar de errores.
                fprintf(stderr, "Error (liberar_bloques_inodo()): No se ha podido leer blocksize.\n");
                return -1; 
            }
            indice = obtener_indice(nblog, nivel_punteros);
            if(r == -1){
                // Imprimimos el error por la salida estándar de errores.
                fprintf(stderr, "Error (liberar_bloques_inodo()): No se ha podido obtener indice.\n");
                return -1; 
            }
            ptr_nivel[nivel_punteros-1] = ptr;
            indices[nivel_punteros-1] = indice;
            ptr = bloques_punteros[nivel_punteros-1][indice];
            nivel_punteros--;    
        }
        if(ptr > 0){
            int bloque_liberado = liberar_bloque(ptr);
            if(bloque_liberado == -1){
                // Imprimimos el error por la salida estándar de errores.
                fprintf(stderr, "Error (liberar_bloques_inodo()): No se ha podido liberar el bloque.\n");
                return -1; 
            }
            nbloquesLiberados++;
            printf("[liberar_bloques_inodo()→ liberado BF %d  de datos correspondiente al BL %d]\n", bloque_liberado,nblog);
            if(nRangoBL == 0){
                inodo.punterosDirectos[nblog] = 0;
                salvar_inodo = 1;
            }
            else {
                while(nivel_punteros < nRangoBL){
                    indice = indices[nivel_punteros];
                    bloques_punteros[nivel_punteros][indice] = 0;
                    ptr = ptr_nivel[nivel_punteros];
                    if(memcmp(bloques_punteros[nivel_punteros],bufferAux,BLOCKSIZE)==0){
                        r = liberar_bloque(ptr);
                        if(r == -1){
                            // Imprimimos el error por la salida estándar de errores.
                            fprintf(stderr, "Error (liberar_bloques_inodo()): No se ha podido liberar el bloque.\n");
                            return -1; 
                        }
                        nbloquesLiberados++;
                        printf("[liberar_bloques_inodo()→ liberado BF %d  de punteros de nivel %d correspondiente al BL %d]\n", ptr, nivel_punteros+1, nblog);
                        nivel_punteros++;
                        if(nivel_punteros == nRangoBL){
                            inodo.punterosIndirectos[nRangoBL-1] = 0;
                            salvar_inodo = 1;
                        }
                    }
                    else{
                        r = bwrite(ptr,bloques_punteros[nivel_punteros]);
                        if(r == -1){
                            // Imprimimos el error por la salida estándar de errores.
                            fprintf(stderr, "Error (liberar_bloques_inodo()): No se ha podido escribir bloques_punteros.\n");
                            return -1; 
                        }
                        nivel_punteros = nRangoBL;
                    }
                }
            }
        }

    }
    if(salvar_inodo == 1){
        escribir_inodo(ninodo, inodo);
    }
    // Devolvemos número de bloques liberados.
    printf("[liberar_bloques_inodo()→ total bloques liberados: %d]\n", nbloquesLiberados);
    return nbloquesLiberados;
}