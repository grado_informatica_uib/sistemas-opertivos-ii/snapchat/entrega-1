/******************************************************************************
*                               DIRECTORIOS.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÓN: FUNCIONES BÁSICA DE ENTRADA Y SALIDA DE BLOQUES.
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 14 DE MAYO DE 2019.
******************************************************************************/


/******************************************************************************
*                           ARCHIVOS AUXILIARES
******************************************************************************/
#include "directorios.h"
#include <string.h>

/******************************************************************************
* Método: extraer_camino()
* Descripción:  Disecciona un camino para saber si una ruta es un directorio 
*               o un fichero.
* Parámetros:   camino  :   Camino completo recibido.
*               inicial :   Puntero donde se guardará el inicial.
*               final   :   Puntero donde se guardará el final.
*               tipo    :   Puntero donde se guardará el tipo.
* Devuelve:    -1: Si ha habido algún problema.
*               1: Si se ha ejecutado correctamente.
******************************************************************************/
int extraer_camino(const char *camino, char *inicial, char *final, char *tipo){
    const char ch = '/';
    int estado = 1;
    // Si el camino comianza por '/'
    if(camino[0] == ch){
        // Tratamos el inicial.
        int i;                      // Indice de camino.
        int j = 0;                  // Indice de inicial.
        for(i=1;camino[i] != ch || camino[i] = 0 ;i++){
            inicial[j] = comino[i]; // Asignamos camino a inicial por caracteres
            j++;                    // Aumentamos el indice de inicial.
        }
        inicial[j] = 0;             // Final de String en inicial.
        // Averiguamos si es directorio o fichero.
        if(camino[i] == ch){
            strcpy(final, camino);      // Copiamos el camino en final.
            final++;                    // Nos saltamos el primer caracter.    
            final = strchr(final, ch);  // Avanzamos al segundo '/' y actualizamos final
            strcpy(tipo, "d");          // Informamos que se trata de un directorio
        }
        else{
            strcpy(tipo, "f");          // Informamos de que se trata de un directorio.
        }
    }
    // Si el camino no comienza por '/'
    else{
        // Estado error
        estado = -1;
        // Imprimimos por la salida estandar de errores, el error.
        fprintf(stderr, " Error: [extraer_camino()] --> camino inválido.\n");
    }
    // Devolvemos el estado de la ejecución de la función.
    return estado;
}


/******************************************************************************
* Método: buscar_entrada()
* Descripción: Devuelve el número de inodo del camino parcial. 
* Parámetros:  reservas : (1:crea) | (0:no crea)
* Devuelve:   -1 : Si ha habido algún error.
*              0 : Si todo ha ido bien.
******************************************************************************/
int buscar_entrada(const char *camino_parcial, unsigned int *p_inodo_dir, 
unsigned int *p_inodo, unsigned int *p_entrada, char resexit failurervar, unsigned char permisos){
    char inicial[MAX_TAMANO]; // LIMITE TAMAÑO TEMPORAL
    char final[MAX_TAMANO];   // LIMITE TAMAÑO TEMPORAL
    memset(inicial, 0, MAX_TAMANO);
    memset(final, 0, MAX_TAMANO);
    char tipo;
    struct entrada entradas[BLOCKSIZE / sizeof(struct entrada)];
    int numentradas = 0;
    if(strcmp(camino_parcial, "/") == 0){
        p_inodo = 0;
        p_entrada = 0;
        return 0;
    }

    if(extraer_camino(camino_parcial, inicio, final, &tipo) == -1){
        fprintf(stderr, "[buscar_entrada]--> Error al extraer camino.\n");
        return -1;
    }

    struct inodo inodo_dir;
    if(leer_inodo(p_inodo_dir, &inodo_dir) == -1){
        fprintf(stderr, "[buscar_entrada]--> Error de lectura del inodo");
        return -1;
    }

    // COMPROBAMOS LOS PERMISOS DEL INODO
    if((inodo_dir.permisos & 4) != 4){
        fprintf(stderr, "[buscar_entrada]--> Error en los permisos del inodo");
        return -1;
    }

    memset(entradas, 0, sizeof(entrada));

    numentradas = inodo_dir.tamEnBytesLog/sizeof(struct entradas);

    int nentrada = 0;
    int offset = 0;
    int index = 0;

    if(numentradas > 0){
        mi_read_f(p_inodo_dir, entradas, offset, BLOCKSIZE);
        while((nentrada < numentradas) && (strcmp(inicial, entradas[index].nombre) != 0){
            nentrada++;
            index++;
            offset += sizeof(struct entrada);
            if(index == BLOCKSIZE / sizeof(struct entrada)){
                index = 0;
                mi_read_f(p_inodo_dir, entradas, offset, BLOCKSIZE):
            }
        }
    }

    if(nentrada == numentradas){
    switch(reservar){
        case 0:
            fprintf(stderr, "[buscar_entrada]--> Error: No existe entrada consulta.\n");
            return -1;
        case 1:
            if(inodo_dir.tipo = 'f'){
                fprintf(stderr, "[buscar_entrada]--> Error: No se puede crear una entrada en un fichero.\n");
                return -1;
            }
            if((inodo_dir.permisos & 2) != 2){
                fprintf(stderr, "[buscar_entrada]--> Error: Fallo en permiso escritura.\n");
                return -1;
            } else {
                strcpy(entradas[index].nombre, inicial);
                if(tipo == 'd'){
                    if(strcmp(final, "/") != 0){
                        int ninodo = reservar_inodo(tipo, permisos);
                        entradas[index].ninodo = ninodo;
                    } else {
                        fprintf(stderr, "[buscar_entrada]--> Error: No existe directorio intermedio.\n");
                        return -1;
                    }
                } else {
                    int ninodo = reservar_inodo('f', permisos);
                    entradas[index].ninodo = ninodo
                }
                if(mi_write_f(p_inodo_dir, &entradas[index], offset, sizeof(struct entrada)) == -1){
                    if(entradas[index].ninodo != -1){
                        liberar_inodo(entradas[index].ninodo);
                    }
                    fprintf(stderr, "[buscar_entrada]--> Error: Exit failure.\n");
                    return -1;
                }
            }
        }
    } 

    if(strcmp(final, "\0")==0){
        if((nentrada < numentradas) && (reservar = 1)){
            fprintf(stderr, "[buscar_entrada]--> Error: Entrada ya existe.\n");
            return -1;
        }

        p_inodo = entradas[index].ninodo;
        p_entrada = nentrada;
        return 0;
    } else {
        p_inodo_dir = entradas[index].ninodo;
        return buscar_entrada(final, p_inodo_dir, p_inodo, p_entrada, reservar, permisos);
    }




    return 0;
}