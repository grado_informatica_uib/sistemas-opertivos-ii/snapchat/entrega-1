/******************************************************************************
*                               FICHEROS.H 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÓN:
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 30 DE MARZO DE 2019.
******************************************************************************/
#include "ficheros_basico.h"
#include <time.h>

struct STAT {     // comprobar que ocupa 128 bytes haciendo un sizeof(inodo)!!!
   char tipo;     // Tipo ('l':libre, 'd':directorio o 'f':fichero)
   char permisos; // Permisos (lectura y/o escritura y/o ejecución)
   time_t atime; // Fecha y hora del último acceso a datos: atime
   time_t mtime; // Fecha y hora de la última modificación de datos: mtime
   time_t ctime; // Fecha y hora de la última modificación del inodo: ctime

   /* comprobar el tamaño del tipo time_t para vuestra plataforma/compilador:
   printf ("sizeof time_t is: %d\n", sizeof(time_t)); */

   unsigned int nlinks;             // Cantidad de enlaces de entradas en directorio
   unsigned int tamEnBytesLog;      // Tamaño en bytes lógicos. Se actualizará al escribir si crece
   unsigned int numBloquesOcupados; // Cantidad de bloques ocupados zona de datos
};

/******************************************************************************
* Método: mi_write_f()
* Descripción:  Escribe el contenido de un buffer de memoria, ​buf_original​, 
*               en un fichero/directorio
* Parámetros:   ninodo        : Identificador de fichero/directorio (inodo)
*               *buf_original : Buffer donde se referencia el contenido
*               offset        : Posición inicial de escritura en inodo, bytes 
*                               lógicos.
*               nbytes        : Número de bytes que hay que escribir.
* Devuelve:     Cantidad de bytes escritos.
*               -1 si ha habido algún problema.
******************************************************************************/
int mi_write_f(unsigned int ninodo, const void *buf_original, unsigned int offset, unsigned int nbytes); 

/******************************************************************************
* Método: mi_read_f()
* Descripción:  Lee información de un fichero/directorio correspondiente al 
*               número de inodo pasado como argumento y lo almacena en el buffer
*               de memoria, buf_original.
* Parámetros:   ninodo        : Número de inodo correspondiente.
*               *buf_original : Buffer donde se almacenarán los datos.
*               offset        : Posición de lectura inicial.
*               nbytes        : Número de bytes que se deben leer.
* Devuelve:     Número de bytes leídos.
*              -1 si ha habido algún problema.
******************************************************************************/
int mi_read_f(unsigned int ninodo, void *but_original, unsigned int offset, unsigned int nbytes);

/******************************************************************************
* Método: mi_stat_f()
* Descripción:  Devuelve la metainformacióm de un fichero/directorio 
*               correspondiente al número de inodo pasado como argumento.
* Parámetros:   ninodo  : Número de inodo
*               *p_stat : Puntero a struct STAT
* Devuelve:      0 : Ha funcionado bien
*               -1 : Ha habido algún error.
******************************************************************************/
int mi_stat_f(unsigned int ninodo, struct STAT *p_stat);

/******************************************************************************
* Método:  mi_chmod_f()
* Descripción:  Cambia los permisos de un fichero/directorio correspondiente al 
*               número de inodo pasado como argumento según indique el argumento.
* Parámetros:   ninodo   : Número de inodo
*               permisos : Permisos
* Devuelve:      0 : Ha funcionado bien
*               -1 : Ha habido algún error.
******************************************************************************/
int mi_chmod_f(unsigned int ninodo, unsigned char permisos);

/******************************************************************************
* Método: mi_truncar_f()
* Descripción:  Trunca un fichero/directorio a los bytes indicados, liberando 
*               los bloques necesarios.
* Parámetros:   ninodo : Número de inodo a truncar.
*               nbytes : Número de bytes a truncar.
* Devuelve:     Número de bloques liberados.
*              -1 : Ha ocurrido algún error.
******************************************************************************/
int mi_truncar_f(unsigned int ninodo, unsigned int nbytes);