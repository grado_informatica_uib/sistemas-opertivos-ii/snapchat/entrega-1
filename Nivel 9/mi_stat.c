/******************************************************************************
*                                MI_STAT.C 
* PROYECTO: ENTREGA 1 - SISTEMAS OPERATIVOS II - GRADO ING. INFORMATICA - UIB
* DESCRIPCIÓN: FUNCIONES BÁSICA DE ENTRADA Y SALIDA DE BLOQUES.
* AUTORES: JUAN CARLOS BOO CRUGEIRAS
*          HÉCTOR MARIO MEDINA CABANELAS
* FECHA: 14 DE MAYO DE 2019.
******************************************************************************/
#include "directorios.h"

int main(int argc, char *argv[]){
    // Comprobamos que el numero de argumentos es el correcto
    if (argc != 3) {
      fprintf(stderr, "Sintaxis: ./mi_stat <disco> </ruta>.\n");
      exit(EXIT_FAILURE);
    } 

    // Montamos eldisco
    if (bmount(argv[1]) == -1) {
        fprintf(stderr, "Error: error de apertura de fichero.\n");
        exit(-1);
    }
    struct STAT p_stat;
    
    mi_stat(argv[2], &p_stat);

     char buffer[1000];
    memset(buffer, 0, 1000);
      printf("tipo = %c\n",  p_stat.tipo);
    printf("permisos = %d\n",  p_stat.permisos);

    struct tm *tm; //ver info: struct tm
    char tmp[100];
    tm = localtime(&p_stat.atime);
      sprintf(tmp,"atime: %d-%02d-%02d %02d:%02d:%02d\t",tm->tm_year+1900,
                    tm->tm_mon+1,tm->tm_mday,tm->tm_hour,tm->tm_min,tm->tm_sec);
      strcat(buffer,tmp);
      strcat(buffer, "\n");
    tm = localtime(&p_stat.ctime);

sprintf(tmp,"mtime. %d-%02d-%02d %02d:%02d:%02d\t",tm->tm_year+1900,
                    tm->tm_mon+1,tm->tm_mday,tm->tm_hour,tm->tm_min,tm->tm_sec);
      strcat(buffer,tmp);
      strcat(buffer, "\n");
    tm = localtime(&p_stat.mtime);

sprintf(tmp,"ctime: %d-%02d-%02d %02d:%02d:%02d\t",tm->tm_year+1900,
                    tm->tm_mon+1,tm->tm_mday,tm->tm_hour,tm->tm_min,tm->tm_sec);
      strcat(buffer,tmp);
      strcat(buffer, "\n");



      printf("%s", buffer);

        printf("nlinks = %d\n", p_stat.nlinks);

    printf("tamEnBytesLog = %d\n", p_stat.tamEnBytesLog);
    printf("numBloquesOcupados = %d\n", p_stat.numBloquesOcupados);


    if (bumount() == -1) {
        fprintf(stderr, "Error: error al cerrar el fichero.\n");
        exit(EXIT_FAILURE);
    }
    return 0;
}