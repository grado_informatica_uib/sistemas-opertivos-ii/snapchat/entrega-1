# **SISTEMAS DE FICHEROS**

## **1. INTRODUCCIÓN.**

Este repositorio pretende alojar el proyecto de **Sistema de Ficheros**  de la asignatura de **Sistemas Operativos II** impartida en la **Universidad de Islas Baleares**. Se trata de el desarrollo de un sistemas de ficheros, dentro de un disco virtual.


<img style="display:block; margin: 0 auto" src="doc/images/ficheros.PNG">

Más información sobre objetivos del proyecto en [documentación introductoria](doc/Práctica de Sistemas Operativos II.pdf).

## **2. AUTORES.**

Los autores son dos alumnos de la asignatura:

- Héctor Mario Medina Cabanelas (hmedcab@gmail.com / @hector.medina)
- Juan Carlos Boo Crugeiras (carlosboocrugeiras@gmail.com / @carlosboo )

## **3. ESTRUCTURA.**

La práctica se divide en Niveles, cuyo desarrollo se realiza de forma escalonada de modo que un nivel superior contiene los conocimientos, funciones y utilidades de sus niveles inferiores.

## **4. NIVELES.**

### **4.1. NIVEL 1.**

Vamos a programar en bloques.c las funciones básicas para E/S de bloques y luego mi_mkfs.c será el programa que formateará nuestro sistema de ficheros y hará uso de tales funciones para montar/desmontar el dispositivo virtual y leer/escribir por bloques.

- [x] `int bmount(const char *camino);` Monta el disco y devuelve su descriptor.
- [x] `int bumount();` Cierra el disco.
- [x] `int bwrite(unsigned int nbloque, const void *buf);` Escribe los datos del buffer en un bloque pasado por parámetro.
- [x] `int bread(unsigned int nbloque, void *buf);` Lee el bloque indicado por parámetro y copia su contenido en el buffer.

[Más información.](doc/Nivel 1.pdf)

### **4.2. Nivel 2.**

Comenzamos a desarrollar en ficheros_basico.c las funciones de tratamiento básico del sistema de ficheros y también definiremos la estructura del superbloque y la estructura de un inodo. Después actualizaremos mi_mkfs.c para llamar a las funciones de inicialización de cada zona de metadatos, que hemos desarrollado en ficheros_basico.c.

- [x] `int tamMB(unsigned int nbloques);` Calcula el número de bloques necesarios para alojar el mapa de bits.
- [x] `int tamAI(unsigned int ninodos);` Calcula el número de bloques necesarios para alojar el Array de Inodos.
- [x] `int initSB(unsigned int nbloques, unsigned int ninodos);` Inicializa el superbloque con los datos apropiados.
- [x] `int initMB();` Inicializa los valores del Mapa de Bits.
- [x] `int initAI();` Inicializa los valores del Array de Inodos.

[Más información.](doc/Nivel 2.pdf)

### **4.3. Nivel 3.**

Continuemos con la definición de funciones básicas de gestión de ficheros (en ficheros_basico.c, y declaradas en su cabecera ficheros_basico.h), y actualizando mi_mkfs.c para formatear nuestro sistema de ficheros.

- [X] `int escribir_bit(unsigned int nbloque, unsigned int bit);` Escribe el valor indicado por el parámetro bit (0 o 1) en un determinado bit del MB que representa el bloque nbloque.
- [X] `unsigned char leer_bit(unsigned int nbloque);` Lee un determinado bit del MB y devuelve el valor del bit leído.
- [X] `int reservar_bloque();` Encuentra el primer bloque libre, consultando el MB, lo ocupa y devuelve su posición.
- [X] `int liberar_bloque(unsigned int nbloque);` Libera un bloque determinado.
- [X] `int escribir_inodo(unsigned int ninodo, struct inodo inodo);`Escribe el contenido de una variable de tipo struct inodo en un determinado inodo del array de inodos.
- [X] `int leer_inodo(unsigned int ninodo, struct inodo *inodo);` Lee un determinado inodo del array de inodos para volcarlo en una variable tipo struct inodo pasada por referencia.
- [X] `int reservar_inodo(unsigned char tipo, unsigned char permisos);`  Encuentra el primer inodo libre, lo reserva, devuelve su número y actualiza la lista enlazada de inodos libres.

[Más información.](doc/Nivel 3.pdf)

### **4.4. Nivel 4.**

En este nivel se introducirán utilizadades para poder trabajar correctamente con los inodos.

- [x] `obtener_nrangoBL(inodo: inodo, nblogico:unsigned ent, *ptr:unsigned ent);` Obtiene el rando de punteros en el que se sitúa el bloque lógico que buscamos y la dirección almaenada en el puntero correspondiente del inodo.
- [x] `obtener_indice(nblogico: ent, nivel_punteros:ent);` A partir de un numero de bloque lógico y un nivel de punteros, obtiene el el índice que almacena el bloque físico de los datos.
- [x] `int traducir_bloque_inodo(unsigned int ninodo, unsigned int nblogico, char reservar);` Se encarga de obtener el número de bloque físico correspondiente a un bloque lógico determinado del inodo indicado.

[Más información.](doc/Nivel 4.pdf)

### **4.5. Nivel 5.**

En el nivel 3 ya realizamos las funciones leer_inodo(), escribir_inodo() y reservar_inodo(), ahora vamos a realizar la que nos faltaba para completa el grupo: liberar_inodo(), la cual requiere de una función auxiliar para liberar todos los bloques que cualgan del inodo, liberar_bloques_inodo(). Con ello completaremos el conjunto de funciones de la capa ficheros_basico.c.

- [x] `int liberar_inodo(unsigned int ninodo);` Libera el inodo especificado por parámetro.
- [x] `int liberar_bloques_inodo(unsigned int ninodo, unsigned int nblogico);` Libera todos los bloques ocupados a partir del bloque lógico indicado por el argumento nblogico (inclusive).

[Más información.](doc/Nivel 5.pdf)

### **4.6. Nivel 6.**

Construiremos el programa ficheros.c

- [x] `int mi_write_f(unsigned int ninodo, const void *buf_original, unsigned int offset, unsigned int nbytes);` Escribe el contenido de un buffer de memoria, ​buf_original​, en un fichero/directorio
- [x] `int mi_read_f(unsigned int ninodo, void *but_original, unsigned int offset, unsigned int nbytes);` Lee información de un fichero/directorio correspondiente al número de inodo pasado como argumento y lo almacena en el buffer de memoria, buf_original.
- [x] `int mi_stat_f(unsigned int ninodo, struct STAT *p_stat);` Devuelve la metainformacióm de un fichero/directorio correspondiente al número de inodo pasado como argumento.
- [x] `int mi_chmod_f(unsigned int ninodo, unsigned char permisos);` Cambia los permisos de un fichero/directorio correspondiente al número de inodo pasado como argumento según indique el argumento.
- [x] `int mi_truncar_f(unsigned int ninodo, unsigned int nbytes);`Trunca un fichero/directorio a los bytes indicados, liberando los bloques necesarios.

[Más información.](doc/Nivel 6.pdf)

### **4.7. Nivel 7.**

En este nivel desarrollaremos una serie de programas para comprobar el correcto funcionamiento de las 3 primeras capas de nuestra biblioteca. Como tal no forman parte del sistema de ficheros.

- [x] `escribir.c` Escribirá texto en uno o varios inodos haciendo uso de reservar_inodo ('f', 6) para obtener un no de inodo, que mostraremos por pantalla y además utilizaremos como parámetro para mi_write_f()​.
- [x] `leer.c` Le pasaremos por línea de comandos un no de inodo obtenido con el programa anterior (además del nombre del dispositivo). Su funcionamiento tiene que ser similar a la función cat de linux, explorando ​TODO​ el fichero
- [x] `permitir.c` Cambia los permisos de un inodo.
- [x] `truncar.c` Libera un inodo a partir de un número de byte dado por parámetro.

[Más información.](doc/Nivel 7.pdf)

### **4.8. Entrega 1**

La entrega 1 se corresponde con la primera evaluación de la práctica y básicamente sirve para comprobar que el Sistema de Ficheros funciona correctamente a bajo nivel. Simplemente se han proporcionado los ficheros solicitados por el profesor desde el Nivel 7 y consiste en hacer correr los siguientes scripts de validación.

- [x] `script1e1.sh`: Pasado correctamente
- [x] `script2e1.sh`: Pasado correctamente
- [x] `script3e1.sh`: Pasado correctamente
- [x] `script3e1.sh`: Pasado correctamente

### **4.9. Nivel 8**

A partir de este momento, los directorios y los ficheros serán conocidos por su nombre (en vez de por el número de su inodo). El nombre incluye el camino de directorios seguido para llegar a él. Para facilitar el trabajo, haremos que el nombre de los directorios termine con el carácter de separación '/' (el de de los ficheros no terminará con ningún carácter en particular).

- [ ] `int extraer_camino(const char *camino, char *inicial, char *final, char *tipo);`:
- [ ] `int buscar_entrada(const char *camino_parcial, unsigned int *p_inodo_dir, unsigned int *p_inodo, unsigned int *p_entrada, char reservar, unsigned char permisos);`:ç

[Más información.](doc/Nivel 8.pdf)

### **4.10. Nivel 9**

En este nivel crearemos algunas funciones de la capa de directorios y los programas  correspondientes (comandos) que permiten la ejecución de tales funcionalidades desde consola. Todas las funciones llamarán a buscar_entrada() para obtener el nº de inodo. Varias de ellas van ligadas a la función correspondiente de la capa de ficheros, a la cual le pasan ese nº de inodo.

[Más información.](doc/Nivel 9.pdf)